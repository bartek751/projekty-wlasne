var canvas = document.querySelector('canvas');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext('2d');

c.fillRect(100, 100, 100, 100);
c.fillRect(200, 400, 100, 100);
c.fillRect(300, 300, 100, 100);

console.log(canvas);

c.fillStyle = "blue";
c.fillRect(500, 300, 100, 100);

// line
c.beginPath()
c.moveTo(50,300);
c.lineTo(300,400);
c.stroke();

// arc / circle
c.beginPath();
c.arc(200, 300, 30,  0, Math.PI * 2, false);
c.strokeStyle = "blue";
c.stroke();

// Math.random() * max;

// grafika
var img = document.getElementById("trawa");
var x,y;
c.drawImage(img, 10, 10);
for(var i=0; i<=10; i++)
{
	x=Math.round(Math.random() * 1700);
	y=Math.round(Math.random() * 900);
	c.drawImage(img, x , y);
}
