<html>
	<head>
		<meta charset="utf-8">
		<title>Edytor V4</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<style>
			body{
				margin: 0px;
			}
			#przyciski{
				width:20%;
				height:10%;
				background-color:#002d6e;
				float: left;
				color: #91c900;
			}
			#przybornik{
				width:80%;
				height:10%;
				background-color:#1d4368;
				float: left;
				color: #91c900;
			}
			#przyciskiPrzybornika{
				float: left;
			}
			#stanUsuwaniaObiektowDiv{
				float:left;
			}
			#edycja{
				width:100%;
				height:90%;
				background-color:#173653;
				float: left;
			}
		</style>
	</head>
	<body>
		<div id="przyciski">
			<input type="button" value="Wypełnij mapę grafiką domyślną" onclick="wypelnienieDomyslne()"><br>
			<?php
			$szerMapy = $_POST["szerokoscMapy"];
			$wysMapy = $_POST["wysokoscMapy"];
			$wielkoscPola = $_POST["wielkoscPola"];
			$iloscTekstur = $_POST["iloscTekstur"];

			echo "<input type='hidden' value='".$szerMapy."' name='szerMapy' id='szerMapy' disabled'>";
			echo "<input type='hidden' value='".$wysMapy."' name='wysMapy' id='wysMapy' disabled>";
			echo "<input type='hidden' value='".$wielkoscPola."' name='wielkoscPola' id='wielkoscPola' disabled>";
			echo "<input type='hidden' value='".$iloscTekstur."' name='iloscTekstur' id='iloscTekstur' disabled>";
			?>
			Mapa/Level:<input type="number" id="mapaLevel" style="width:50px;"><input type="button" value="wczytaj" onclick="wczytajPoziom()">
			<input type="button" value="eskport" onclick="eksportMapy()">
			<input type="button" value="umieszczanie bloków" id="trybEdycji" onclick="trybEdycji()">
			<input type="button" value="zablokuj tekstury" id="trybTekstur" onclick="trybTekstury(id)">
			<input type="button" value="zapisz tekstury" onclick="zapisPalety()">
			<input type="button" value="wczytaj tekstury" onclick="wczytaniePalety()">
		</div>
		<div id="przybornik">
			<?php
			$maxRubryk=0;
			$tabela="tabela";
			
			$tabela="<table border 2px solid black>";
			$maxRubryk=10; 
			$tabela.="<tr>";
			for($i=0; $i<=$maxRubryk-1;$i++)
			{
				$tabela.="<td id='tek".$i."' onclick='dodajTeksture(this.id)'><center>+</center></td>";
			}
			$tabela.="</tr>";
			$tabela.="</table>";
			echo $tabela;
			echo "<div id='PrzyciskiPrzybornika'>Strony przybornika:";
			echo "<input type='button' value='-' onclick='przybDol()'>";
			echo "<input type='button' value='+' onclick='przybGora()'>";
			echo "<br>";
			echo "Usuwanie obiektów: <input type='button' value='X' onclick='usuwacz()'></div>";
			echo "<div id='stanUsuwaniaObiektowDiv'>Usuwanie obiektów nie aktywne</div>";
			if($wielkoscPola==25)
			{
				for($i=0; $i<=$maxRubryk-1;$i++)
				{
					echo "<script>document.getElementById('tek".$i."').style.width = '25px';</script>";
					echo "<script>document.getElementById('tek".$i."').style.height = '25px';</script>";
				}
			}
			else if($wielkoscPola==50)
			{	
				for($i=0; $i<=$maxRubryk-1;$i++)
				{
					echo "<script>document.getElementById('tek".$i."').style.width = '50px';</script>";
					echo "<script>document.getElementById('tek".$i."').style.height = '50px';</script>";
				}
			}
			?>
			
		</div>
		<div id="edycja">
		<?php
			$szerMapy;
			$wysMapy;
			$tabelaMapy="<table border 2px solid black>";
			for($i=0;$i<=$wysMapy-1;$i++)
				{
					$tabelaMapy.="<tr>";
					for($j=0; $j<=$szerMapy-1;$j++)
					{
						$tabelaMapy.="<td id='tabMapy".$i."_".$j."' onclick='zmienTeksture(this.id)'><center>O</center></td>";
					}
					$tabelaMapy.="</tr>";
				}
				$tabelaMapy.="</table>";
			echo $tabelaMapy;
			if($wielkoscPola==25)
			{
				for($i=0;$i<=$wysMapy-1;$i++)
				{
					for($j=0; $j<=$szerMapy-1;$j++)
					{
						echo "<script>document.getElementById('tabMapy".$i."_".$j."').style.width = '25px';</script>";
						echo "<script>document.getElementById('tabMapy".$i."_".$j."').style.height = '25px';</script>";
					}
				}
			}
			else if($wielkoscPola==50)
			{
				for($i=0;$i<=$wysMapy-1;$i++)
				{
					for($j=0; $j<=$szerMapy-1;$j++)
					{
						echo "<script>document.getElementById('tabMapy".$i."_".$j."').style.width = '50px';</script>";
						echo "<script>document.getElementById('tabMapy".$i."_".$j."').style.height = '50px';</script>";
					}
				}
			}
		?>
		</div>
	</body>
	<script>
		var mapaLevel=0;
		var szerMapy = document.getElementById("szerMapy").value;
		var wysMapy = document.getElementById("wysMapy").value;
		var wyborTekstury="";
		var wyborTeksturyNazwa="";
		var bufforId="";
		var bufforDrugi;
		var idTeksturyDodanej="";
		var trybOknaTextury="dodaj";
		var texturaDomyslnaId="";
		var trybEdycjiMapy="bloki";
		var iloscTekstur=document.getElementById("iloscTekstur").value;
		var czyUsowanieObiektow=0;
		var nazwaPliku="";
		var nazwaEksportowanejTablicy="";
		
		// przybornik
		var stronaPrzybornika=0;
		var k=0;
		
		// tablice
		var tTekstury = new Array(iloscTekstur);
		var tMapa = new Array(parseInt(szerMapy));
		
		for(var i=0;i<=iloscTekstur-1;i++)
		{
			tTekstury[i]=new przybornikZestaw();
		}
		
		for(var i=0; i<tMapa.length;i++)
		{
			tMapa[i]=new Array(parseInt(wysMapy));
			for(var j=0;j<tMapa[i].length;j++)
			{
				tMapa[i][j]=new poleMapy();
			}
		}
		
		function dane()
		{
			console.log("=====================================");
			for(var i=0; i<iloscTekstur;i++)
			{
				console.log("tTekstury["+i+"].id: "+tTekstury[i].id);
				console.log("tTekstury["+i+"].nazwa: "+tTekstury[i].nazwa);
				console.log("tTekstury["+i+"].adres: "+tTekstury[i].adres);
				console.log("- - - - - - - - -");
			}
			console.log("================END==================");
		}
		
		function przybornikZestaw()
		{
			this.id="";
			this.nazwa="";
			this.adres="";
		}
		
		function poleMapy()
		{
			this.blok="";
			this.obiekt="";
		}
		
		function przybGora()
		{
			console.log("Przybornik strona +");
			if(iloscTekstur/10>stronaPrzybornika+1)
			{
				stronaPrzybornika++;
				rysujPrzybornik();
			}
		}
		
		function przybDol()
		{
			console.log("Przybornik strona -");
			if(stronaPrzybornika>0)
			{
				stronaPrzybornika--;
				rysujPrzybornik();
			}
		}
		
		function wczytaniePalety()
		{
			console.log("Wczytanie palety");
			mapaLevel=parseInt(document.getElementById("mapaLevel").value);
			if(mapaLevel>=0)
			{
				console.log("level wiekszy od 0");
				$.post("wczytaniePalety.php",
					{
						level: mapaLevel,
					},
					function(daneB, status)
					{
						//console.log(daneB);
						//console.log(status);
						if(daneB!="")
						{
							uzupelnijTabTekstur(daneB);
						}
					}
				);
			}
			else
			{
				alert("Nie podano mapy/levelu do wczytania.");
			}
		}
		
		function zapisPalety()
		{
			console.log("Zapis palety");
			mapaLevel=parseInt(document.getElementById("mapaLevel").value);
			if(mapaLevel>=0)
			{
				console.log("level wiekszy od 0");
				for(var i=0;i<iloscTekstur;i++)
				{
					if(tTekstury[i].id!="" && tTekstury[i].id!="undefinied")
					{
						$.post("zapisPalety.php",
						{
							id: tTekstury[i].id,
							nazwa: tTekstury[i].nazwa,
							adres: tTekstury[i].adres,
							level: mapaLevel,
						},
						function(daneB, status)
						{
							console.log(daneB);
							console.log(status);
						}
						);
					}
				}
			}
			else
			{
				alert("Nie podano mapy/levelu do wczytania.");
			}
		}
		
		function rysujPrzybornik()
		{
			k=0;
			var cos=1;
			for(var i=stronaPrzybornika*10; i<=parseInt(stronaPrzybornika*10+9);i++)
			{
				if(tTekstury[parseInt(stronaPrzybornika*10+k)].adres!="")
				{
					bufforId=tTekstury[parseInt(stronaPrzybornika*10+k)].adres.split("'");
					document.getElementById("tek"+k).innerHTML="<img src="+bufforId[1]+" id='"+tTekstury[parseInt(stronaPrzybornika*10+k)].nazwa+"'>";
				}
				else
				{
					document.getElementById("tek"+k).innerHTML="<center>+</center>";
				}
				k++;
				if(stronaPrzybornika*10+k>=iloscTekstur)
				{
					break;
				}
				cos++;
			}
			if(cos<10)
			{
				console.log("brakło coska, cosiek = "+cos);
				for(var i=cos;i<10;i++)
				{
					console.log("wywołanie nr: "+i);
					document.getElementById("tek"+i).innerHTML="<center>-</center>";
				}
			}
		}
		
		function dodajTeksture(id)
		{
			console.log("==================");
			console.log("dodajTeksture(), id: "+id);
			if(trybOknaTextury=="dodaj")
			{
				console.log("Dodanie tekstury");
				tekstura=prompt("Podaj nazwę grafiki z rozszerzeniem", "grafika.png");
				if (tekstura == null || tekstura == "") // pusty input
				{
					tekstura = "Odrzucono dodanie/zmianę tekstury.";
					console.log(tekstura);
				} 
				else // wypełniony input
				{
					console.log(tekstura);
					teksturaNazwa = tekstura.split(".");
					document.getElementById(id).innerHTML="<img src='grafiki/"+tekstura+"' id='"+teksturaNazwa[0]+"'>";
					bufforId=id.slice(3,4);
					wyborTekstury="url('";
					wyborTekstury+=document.getElementById(teksturaNazwa[0]).src;
					wyborTekstury+="')";
					console.log("buforId: "+bufforId);
					console.log(parseInt(bufforId));
					tTekstury[stronaPrzybornika*10+parseInt(bufforId)].id=id+"_"+stronaPrzybornika;
					tTekstury[stronaPrzybornika*10+parseInt(bufforId)].nazwa=teksturaNazwa[0];
					tTekstury[stronaPrzybornika*10+parseInt(bufforId)].adres=wyborTekstury;
					wyborTekstury="";
				}
			}
			else if(trybOknaTextury=="zablok")
			{
				console.log("wybieranie textury: "+id);
				bufforId=id.slice(3,4);
				console.log("Przybornik id: "+bufforId);
				if(tTekstury[stronaPrzybornika*10+parseInt(bufforId)].nazwa!="")
				{
					wyborTeksturyNazwa=tTekstury[stronaPrzybornika*10+parseInt(bufforId)].nazwa;
					wyborTekstury=tTekstury[stronaPrzybornika*10+parseInt(bufforId)].adres;
				}
				console.log(wyborTeksturyNazwa);
				console.log(wyborTekstury);
			}
		}
		
		function trybEdycji()
		{
			console.log("tryb edycji");
			if(trybEdycjiMapy=="bloki")
			{
				document.getElementById("trybEdycji").value="umieszczanie itemów";
				trybEdycjiMapy="przedmioty";
			}
			else if(trybEdycjiMapy=="przedmioty")
			{
				document.getElementById("trybEdycji").value="umieszczanie bloków";
				trybEdycjiMapy="bloki";
			}
		}
		
		function trybTekstury(id)
		{
			console.log("Zaznaczenie tekstury");
			if(trybOknaTextury=="dodaj")
			{
				trybOknaTextury="zablok";
				document.getElementById(id).value="Edytuj textury";
			}
			else if(trybOknaTextury=="zablok")
			{
				trybOknaTextury="dodaj";
				document.getElementById(id).value="Zablokuj textury";
			}
		}
		
		function zmienTeksture(id)
		{
			console.log("zmiana tekstury");
			if(wyborTekstury!="" && wyborTekstury!="url('undefined')" && czyUsowanieObiektow==0)
			{
				document.getElementById(id).style.backgroundImage=wyborTekstury;
				document.getElementById(id).innerHTML="";
				bufforId=id.split("_");
				bufforDrugi=bufforId[0].slice(7,9);
				if(trybEdycjiMapy=="bloki")
				{
					console.log(wyborTekstury);
					console.log(wyborTeksturyNazwa);
					tMapa[bufforId[1]][bufforDrugi].blok=wyborTeksturyNazwa;
					mapaLevel=parseInt(document.getElementById("mapaLevel").value);
					if(mapaLevel>=0)
					{
						$.post("edytujZaw.php",
						{
							level: mapaLevel,
							blok: tMapa[bufforId[1]][bufforDrugi].blok,
							obiekt: tMapa[bufforId[1]][bufforDrugi].obiekt,
							x: bufforId[1],
							y: bufforDrugi,
						},
						function(dane, status)
						 {
							 console.log(dane);
							 console.log(status);
						 }
						);
					}
					else
					{
						alert("Nie podano mapy/levelu do wczytania.");
					}
				}
				else if(trybEdycjiMapy=="przedmioty")
				{
					console.log("itemy");
					console.log(wyborTekstury);
					console.log(wyborTeksturyNazwa);
					tMapa[bufforId[1]][bufforDrugi].obiekt=wyborTeksturyNazwa;
					mapaLevel=parseInt(document.getElementById("mapaLevel").value);
					$.post("edytujZaw.php",
						{
							level: mapaLevel,
							blok: tMapa[bufforId[1]][bufforDrugi].blok,
							obiekt: tMapa[bufforId[1]][bufforDrugi].obiekt,
							x: bufforId[1],
							y: bufforDrugi,
						},
						function(dane, status)
						 {
							 console.log(dane);
							 console.log(status);
						 }
						);
				}
			}
			else if(czyUsowanieObiektow==1)
			{
				console.log("usuwanie obiektow");
				bufforId=id.split("_");
				bufforDrugi=bufforId[0].slice(7,9);
				tMapa[bufforId[1]][bufforDrugi].obiekt="";
				mapaLevel=parseInt(document.getElementById("mapaLevel").value);
				$.post("edytujZaw.php",
					{
						level: mapaLevel,
						blok: tMapa[bufforId[1]][bufforDrugi].blok,
						obiekt: tMapa[bufforId[1]][bufforDrugi].obiekt,
						x: bufforId[1],
						y: bufforDrugi,
					},
					function(dane, status)
					 {
						 console.log(dane);
						 console.log(status);
					 }
					);
				for(var i = 0; i<iloscTekstur; i++)
				{
					if(tMapa[bufforId[1]][bufforDrugi].blok == tTekstury[i].nazwa)
					{
						document.getElementById(id).style.backgroundImage=tTekstury[i].adres;
					}
					if(i>=100)
					{
						console.log("Petla zapetlona");
						break;
					}
				}
			}
		}
		
		function rysujTabMapy()
		{
			console.log("Rysuj mapę na podstawie wczytanej tablicy.");		
			for(var i=0;i<tMapa.length;i++)
			{
				for(var j=0;j<tMapa[i].length;j++)
				{
					for(var k=0;k<iloscTekstur;k++)
					{
						if(tMapa[i][j].blok==tTekstury[k].nazwa)
						{
							document.getElementById("tabMapy"+j+"_"+i).style.backgroundImage=tTekstury[k].adres;
							document.getElementById("tabMapy"+j+"_"+i).innerHTML="";
						}
						if(tMapa[i][j].obiekt==tTekstury[k].nazwa && tTekstury[k].id!="")
						{
							document.getElementById("tabMapy"+j+"_"+i).style.backgroundImage=tTekstury[k].adres;
							console.log("obiekt");
						}
					}
				}
			}
		}//-
		
		function wypelnienieDomyslne()
		{
			console.log("wypełnienie domyślne");
			mapaLevel=parseInt(document.getElementById("mapaLevel").value);
			if(tTekstury[0].adres!="")
			{
				wyborTekstury=tTekstury[0].adres;
			}
			for(var i=0; i<=wysMapy-1;i++)
			{
				for(var j=0;j<=szerMapy-1;j++)
				{
					document.getElementById("tabMapy"+i+"_"+j).style.backgroundImage=wyborTekstury;
					document.getElementById("tabMapy"+i+"_"+j).innerHTML="";
				}
			}
			wyborTekstury="";
			for(var i=0; i<tMapa.length;i++)
			{
				for(var j=0;j<tMapa[i].length;j++)
				{
					tMapa[i][j].blok=tTekstury[0].nazwa;
					if(mapaLevel>=0)
					{
						$.post("edytujZaw.php",
						{
							level: mapaLevel,
							blok: tMapa[i][j].blok,
							obiekt: tMapa[i][j].obiekt,
							x: i,
							y: j,
						},
						function(dane, status)
							 {
								 console.log(dane);
								 console.log(status);
							 }
						);
					}
				}
			}
		}
		
		function wczytajPoziom()
		{
			console.log("Wczytaj poziom");
			mapaLevel=parseInt(document.getElementById("mapaLevel").value);
			if(mapaLevel>=0)
			{
				console.log("level wiekszy od 0");
				$.post("wczytajTab.php",
				  {
					  wys: wysMapy,
					  szer: szerMapy,
					  level: mapaLevel,
				  },
				  function(daneB, status)
				  {
					  if(daneB!="")
					  {
						 uzupelnijTabMapy(daneB); 
					  }
				  }
				 );
			}
			else
			{
				alert("Nie podano mapy/levelu do wczytania.");
			}
		}
		
		function uzupelnijTabTekstur(daneB)
		{
			console.log("Uzupełnij tablicę tekstur.");
			var wyniki = daneB.split("\n");
			var wynikiBuff;
			var poProstuBufor;
			var k=0;
			for(var i=0;i<wyniki.length-1;i++)
			{
				wynikiBuff=wyniki[k].split(",");
				tTekstury[i].id=wynikiBuff[0];
				tTekstury[i].nazwa=wynikiBuff[1];
				tTekstury[i].adres=wynikiBuff[2];
				k++;
			}
			sortujTabText();
			rysujPrzybornik();
		}
		
		function sortujTabText()
		{
			var poProstuBufor;
			var buforLiczbyA;
			var buforLiczbyB;
			var posortowana=false;
			for(;posortowana!=true;)// sortowanie stron
			{
				posortowana=true;
				for(var i=0;i<iloscTekstur-1;i++)
				{
					if(tTekstury[i+1].id!="" && tTekstury[i].id!="")
					{	
						buforLiczbyA=tTekstury[i].id.slice(5,6);
						buforLiczbyB=tTekstury[i+1].id.slice(5,6);
						if(buforLiczbyA>buforLiczbyB)
						{
							bufforId = tTekstury[i].id;
							bufforDrugi = tTekstury[i].nazwa;
							poProstuBufor = tTekstury[i].adres;
							
							tTekstury[i].id = tTekstury[i+1].id;
							tTekstury[i].nazwa = tTekstury[i+1].nazwa;
							tTekstury[i].adres = tTekstury[i+1].adres;
							
							tTekstury[i+1].id = bufforId;
							tTekstury[i+1].nazwa = bufforDrugi;
							tTekstury[i+1].adres = poProstuBufor;
							posortowana=false;
						}
					}
				}
			}
			posortowana=false;
			for(;posortowana!=true;)// sortowanie elementow strony
			{
				posortowana=true;
				for(var i=0;i<iloscTekstur-1;i++)
				{
					if(tTekstury[i+1].id!="" && tTekstury[i].id!="")
					{	
						buforLiczbyA=tTekstury[i].id.slice(5,6);
						buforLiczbyB=tTekstury[i+1].id.slice(5,6);
						if(buforLiczbyA==buforLiczbyB)
						{
							buforLiczbyA=tTekstury[i].id.slice(3,4);
							buforLiczbyB=tTekstury[i+1].id.slice(3,4);
							if(buforLiczbyA>buforLiczbyB)
							{
								bufforId = tTekstury[i].id;
								bufforDrugi = tTekstury[i].nazwa;
								poProstuBufor = tTekstury[i].adres;
								
								tTekstury[i].id = tTekstury[i+1].id;
								tTekstury[i].nazwa = tTekstury[i+1].nazwa;
								tTekstury[i].adres = tTekstury[i+1].adres;
								
								tTekstury[i+1].id = bufforId;
								tTekstury[i+1].nazwa = bufforDrugi;
								tTekstury[i+1].adres = poProstuBufor;
								posortowana=false;
							}
						}
					}
				}
			}
		}
		
		function uzupelnijTabMapy(daneB)
		{
			console.log("Uzupełnij tablicę mapy");
			var wyniki = daneB.split("\n");
			var wynikiBuff;
			var k=0;
			for(var i=0; i<tMapa.length;i++)
			{
				for(var j=0; j<tMapa[i].length;j++)
				{
					if(wyniki[k]!="" && wyniki[k]!="undefined")
					{
						//console.log("wyniki["+k+"] :"+wyniki[k]);
						wynikiBuff=wyniki[k].split(",");
						//console.log("wynikiBuff[0]: "+wynikiBuff[0]);
						//console.log("wynikiBuff[1]: "+wynikiBuff[1]);
						//console.log("wynikiBuff[2]: "+wynikiBuff[2]);
						tMapa[wynikiBuff[0]][wynikiBuff[1]].blok=wynikiBuff[2];
						tMapa[wynikiBuff[0]][wynikiBuff[1]].obiekt=wynikiBuff[3];
						k++;
					}
				}
			}
			rysujTabMapy();
		}
		
		function usuwacz()
		{
			console.log("usuwanie obiektów");
			if(czyUsowanieObiektow==0)
			{
				czyUsowanieObiektow=1;
				document.getElementById("stanUsuwaniaObiektowDiv").innerHTML="Usuwanie obiektów aktywne";
			}
			else if(czyUsowanieObiektow==1)
			{
				czyUsowanieObiektow=0;
				document.getElementById("stanUsuwaniaObiektowDiv").innerHTML="Usuwanie obiektów nie aktywne";
			}
		}
		
		function eksportMapy()
		{
			console.log("eksportowanie mapy do pliku");
			nazwaPliku=prompt("Podaj nazwę pliku", "mapa");
			nazwaEksportowanejTablicy=prompt("Podaj nazwe tablicy mapy","tMapa");
				if (nazwaPliku == null || nazwaPliku == "") // pusty input
				{
					tekstura = "Odrzucono eksport mapy do pliku.";
					console.log(tekstura);
				}
				else if (nazwaEksportowanejTablicy == null || nazwaEksportowanejTablicy == "") // pusty input
				{
					tekstura = "Odrzucono eksport mapy do pliku.";
					console.log(tekstura);
				}
				else
				{
					mapaLevel=parseInt(document.getElementById("mapaLevel").value);
					//console.log("number is finite: "+Number.isFinite(mapaLevel));
					if(mapaLevel == null || mapaLevel == "" || Number.isFinite(mapaLevel) == false)
					{
						alert("Pole Mapa/Level jest puste. Nie można ustalić dla jakiej mapy przeprowadzić eksport.");
					}
					else
					{
						console.log("Nazwa pliku: "+nazwaPliku);
						console.log("Mapa/Level: "+mapaLevel);
						$.post("EksportMap.php",
						{
							level: mapaLevel,
							nazwa_pliku: nazwaPliku,
							nazwa_tablicy: nazwaEksportowanejTablicy,
						},
						function(daneB, status)
						{
							console.log("Dane: "+daneB);
							console.log("Status: "+status);
						});
					}
				}
		}
	</script>
</html>