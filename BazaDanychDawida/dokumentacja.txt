===================
   znane b��dy:
===================
- po usunieciu kat nie mo�na doda� nast�pnej?
- przy puszczeniu przycisku shift w polu pojawia si� odpowienik ma�ej tej samej litery
- po zmianie okna mo�liwy powr�t na pole kt�rego nie ma w tabeli przycisk�w
- brak obs�ugi klawiatury numerycznej?

===================
Elementy programu:
===================
a) blok g��wny
	- do��czenie bibliotek allegro 5 oraz c++
	- swtorzenie struktur danych, zmiennych globalnych oraz ich inicjalizacja

b) funkcja dane()
	- po wci�ni�ciu przycisku " ~ " w konsoli pojawiaj� si� informacje o stanie wyszczeg�lnionych zmiennych

c) funkcja daneKatB()
	- po wci�ni�ciu przycisku " Insert " w konsoli pojawia si� informacja o stanie pierwszych 5 kategorii
	  oraz ich pierwszych 3 item�w

d) funkcja sprawdzPoleTekstowe()
	- dokonuje zmiany zawarto�ci zmiennej tekstowej kt�ra zosta�a do niej przes�ana, stosowane do obs�ugi
	  p�l do wprowadzania danych tekstowych oraz liczbowych

e) funkcja przyciski()
	- okre�la co ma si� sta� po wci�ni�ciu okre�lonych przycisk�w w zale�no�ci od tego w jakim miejscu programu
	  si� znajdujemy oraz co mamy zaznaczone

f) funkcja klawiatura()
	- domy�lna obs�uga klawiszy klawiatury, po wci�ni�ciu obs�ugiwanego przycisku zmienia jego zmienn� z 0 na 1
	  a po jego puszczeniu z 1 na 0, ustawia stan zmiennej " czyZmienionoPoleWyboru " z 1 na 0

g) funkcja mysz()
	- domy�lna obs�uga myszy, ustawia na bierz�co koordynaty kursora, sprawdza stan LPM oraz ustala stan
	  zmiennej " czyKliknieto " z 1 na 0

h) funkcja rysujGrafPrzycTKat()
	- rysuje przyciski " podglad ", " edycja " oraz " kosz " w tabeli z przegl�dem kategorii

i) funkcja rysujGrafPrzycItemow()
	- rysuje przyciski " edycja " oraz " kosz " w tabeli z przegl�dem item�w

j) funkcja blad()
	- ustala co ma si� pojawi� w oknie komunikatu z b��dem

k) funkcja wczytajDaneZPliku()
	- wczytuje dane z pliku i zapisuje je w strukturze danych, zakomenowane cz�ci mo�na u�y� przy debugowaniu
	- wczytywanie ko�czone jest linijk� " koniec_pliku: "

l) funkcja zapiszDaneDoPliku()
	- zapisuje zawarto�� struktury danych w pliku bazodanowym w formacie kt�ry jest mo�liwy do odczytu
	- zapis ko�czony jest linijk� " koniec_pliku: "

m) funkcja sortujTablice()
	- sortuje strukture danych po dokonaniu na niej operacji modyfikacji oraz usuni�cia

n) funkcja main()
	- inicjalizuje pliki tekstowe,
	- inicjalizuje biblioteki allegro oraz grafiki, w przypadku b��du dodaje wpis do pliku " Error.txt "
	- zawiera p�tl� g��wn� programu kt�ra rysuje zawarto�� ekranu w zale�no�ci gdzie si� znajdujemy
	- na ko�cu p�tli jest segment odpowiedzialny za od�wierzanie ekranu oraz okre�lenie co ma by� zaznaczone
	  po zmianie okna
	- po zako�czeniu programu zwalnia pami�� i zaka�cza program