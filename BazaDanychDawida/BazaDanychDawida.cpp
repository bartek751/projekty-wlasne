﻿#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <string>
#include <fstream>

using namespace std;

// stale przechowujace limity tablic
const int iloscObslugiwanychKat = 50;
const int iloscObslugiwanychItemow = 150;

struct itemy {
    string nazwa = "";
    int ilosc = 0;
    string stan = "";
};

struct kat {
    string nazwa = "";
    int iloscWczytanychItemow = 0;
    itemy Itemy[iloscObslugiwanychItemow];
};

// zmienne obrazowe
int obrazX = 0;
int obrazY = 0;
float polowaObrazX = 0;
float polowaObrazY = 0;

// zmienne techniczne
fstream LogError;
int OknoTestoweZmiennaA = 0;
int OknoTestoweZmiennaB = 0;
int OknoTestoweZmiennaC = 0;
string OknoTestoweZmiennaTekst = "";

char OknoTestoweZmiennaZnak;

// tablica przechowujaca dane
kat tKat[iloscObslugiwanychKat];

// zmienne tabeli kategorii i itemow
string tKatPrzyc[3][5];
int tKatPrzycX = 0;
int tKatPrzycY = 0;
int iloscWczytanychKat = 0;
int stronaTabeliKat = 0;

int iloscKatNaStronie = 0;
string tItemPrzyc[2][5];
int tItemPrzycX = 0;
int tItemPrzycY = 0;
int iloscWczytanychItemow = 0;

int stronaTabeliItemow = 0;
int iloscItemowNaStronie = 0;

// zmienne operacyjne wczytywania tablic z pliku
int iloscKresek = 0;
int etapWczytywania = 0;
int buffKatPlikPocz = 0;
int buffPozWPliku = 0;
int buffDlugosciLiniWczytanej = 0;

char znak;
string liniaPliku = "";
string buffId = "";
string buforZawartosciPliku = "";
string buforLiniiPliku = "";

string wyborMenuG = "menu glowne";
string poprzednieOkno = "menu glowne";
string zaznaczenie = "Baza";
fstream Baza;

// zmienne klawiszowe
bool czyEnter = 0;
bool czyStrzalkaGora = 0;
bool czyStrzalkaDol = 0;
bool czyStrzalkaLewo = 0;
bool czyStrzalkaPrawo = 0;

bool czyW = 0;
bool czyS = 0;
bool czyD = 0;
bool czyA = 0;
bool czyK = 0;

bool czyQ = 0;
bool czyE = 0;
bool czyR = 0;
bool czyT = 0;
bool czyY = 0;

bool czyU = 0;
bool czyI = 0;
bool czyO = 0;
bool czyP = 0;
bool czyF = 0;

bool czyG = 0;
bool czyH = 0;
bool czyJ = 0;
bool czyL = 0;
bool czyZ = 0;

bool czyX = 0;
bool czyC = 0;
bool czyV = 0;
bool czyB = 0;
bool czyN = 0;

bool czyM = 0;
bool czySpacja = 0;
bool czyESC = 0;
bool czy1 = 0;
bool czy2 = 0;

bool czy3 = 0;
bool czy4 = 0;
bool czy5 = 0;
bool czy6 = 0;
bool czy7 = 0;

bool czy8 = 0;
bool czy9 = 0;
bool czy0 = 0;
bool czyBackspace = 0;
bool czyShift = 0;

bool czyWindowsStart = 0;
bool czyTylda = 0;
bool czyDelete = 0;
bool czyInsert = 0;
bool czyPodloga = 0;

// zmienne operacyjne
bool czyZmienionoPoleWyboru = 0; // czy przycisk wcisniety
bool czyZmianaMenu = 0;
bool czyOknoFull = 1;
bool czyWczytywacPlik = 0;
bool czyWczytanoPlik = 0;

bool czyZaladowacKategorie = 1;
bool czyUdaloSieZaladowacGrafiki = 0;
bool czyTworzycPrzyciskiTKat = 1;
bool czyWartoscPolaTextPusta = 0;
bool czyZatwierdzonoNazweKat = 0;

bool czyUsuwamyKat = 0;
bool czyPrzypisacNowyTekstPola = 0;
bool czyKoniecWczytywania = 0;
bool czyZatwierdzonoNazweItemu = 0;
bool czyUsuwamyItem = 0;

int BackspaceCykl = 5; // uzywane do ciaglego usuwania przyciskiem backspace 
int nrWczytywanejKat = 0;
int nrWczytywanegoItemu = 0;
int nrWybranejKat = 0;
string wartoscPolaText = "";

string buforKomunikatu = "";
string wartoscPolaTextNazwa = "";
string wartoscPolaTextStan = "";
string wartoscPolaTextIlosc = "";

// zmienne myszy
int myszX = 0;
int myszY = 0;
bool czyLewyMyszyOn = 0;
bool czyKliknieto = 0;

void dane()
{
    cout << "============================================" << endl;
    cout << "obrazX: " << obrazX << endl;
    cout << "obrazY: " << obrazY << endl;
    cout << "zaznaczenie: " << zaznaczenie << endl;
    cout << "wyborMenuG: " << wyborMenuG << endl;
    cout << "Popradnie okno: " << poprzednieOkno << endl;
    cout << "iloscWczytanychKat: " << iloscWczytanychKat << endl;
    cout << "tKatPrzycX: " << tKatPrzycX << endl;
    cout << "tKatPrzycY: " << tKatPrzycY << endl;
    cout << "tItemPrzycX: " << tItemPrzycX << endl;
    cout << "tItemPrzycY: " << tItemPrzycY << endl;
    cout << "iloscKatNaStronie: " << iloscKatNaStronie << endl;
    cout << "iloscItemowNaStronie: " << iloscItemowNaStronie << endl;
    cout << "stronaTableliItemow: " << stronaTabeliItemow << endl;
    cout << "stronaTabeliKat: " << stronaTabeliKat << endl;
    cout << "nrWczytywanejKat: " << nrWczytywanejKat << endl;
    cout << "Mysz X: " << myszX << endl;
    cout << "Mysz Y: " << myszY << endl;
    cout << "============================================" << endl;
}

void daneKatB()
{
    cout << "============================================" << endl;
    cout << "Tablica stukturalna kat i itemow: " << endl;
    for (int i = 0; i <= 5; i++)
    {
        cout << "- - - - -" << endl;
        cout << "tKat[" << i << "].nazwa: " << tKat[i].nazwa << endl;
        cout << "tKat[" << i << "].iloscItemow: " << tKat[i].iloscWczytanychItemow << endl;
        cout << "tKat[" << i << "].Itemy[0].nazwa: " << tKat[i].Itemy[0].nazwa << endl;
        cout << "tKat[" << i << "].Itemy[0].ilosc: " << tKat[i].Itemy[0].ilosc << endl;
        cout << "tKat[" << i << "].Itemy[0].stan: " << tKat[i].Itemy[0].stan << endl;
        cout << "- - - - -" << endl;
        cout << "tKat[" << i << "].Itemy[1].nazwa: " << tKat[i].Itemy[1].nazwa << endl;
        cout << "tKat[" << i << "].Itemy[1].ilosc: " << tKat[i].Itemy[1].ilosc << endl;
        cout << "tKat[" << i << "].Itemy[1].stan: " << tKat[i].Itemy[1].stan << endl;
        cout << "- - - - -" << endl;
        cout << "tKat[" << i << "].Itemy[2].nazwa: " << tKat[i].Itemy[2].nazwa << endl;
        cout << "tKat[" << i << "].Itemy[2].ilosc: " << tKat[i].Itemy[2].ilosc << endl;
        cout << "tKat[" << i << "].Itemy[2].stan: " << tKat[i].Itemy[2].stan << endl;
        if (i+1 >= iloscObslugiwanychKat)
        {
            cout << "Awaryjne zamkniecie petli" << endl;
            break;
        }
    }
    cout << "Tablica srtukturalna - koniec" << endl;
    cout << "============================================" << endl;
}

void sprawdzPoleTesktowe(string & poleText, bool czyObslugaLiter, bool czyObslugaCyfr)
{
    if (czyObslugaLiter == 1)
    {
        if (czyW == 1)
        {
            if (czyShift == 1)
            {
                poleText += "W";
            }
            else
            {
                poleText += "w";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyS == 1)
        {
            if (czyShift == 1)
            {
                poleText += "S";
            }
            else
            {
                poleText += "s";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyA == 1)
        {
            if (czyShift == 1)
            {
                poleText += "A";
            }
            else
            {
                poleText += "a";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyD == 1)
        {
            if (czyShift == 1)
            {
                poleText += "D";
            }
            else
            {
                poleText += "d";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyQ == 1)
        {
            if (czyShift == 1)
            {
                poleText += "Q";
            }
            else
            {
                poleText += "q";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyE == 1)
        {
            if (czyShift == 1)
            {
                poleText += "E";
            }
            else
            {
                poleText += "e";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyR == 1)
        {
            if (czyShift == 1)
            {
                poleText += "R";
            }
            else
            {
                poleText += "r";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyT == 1)
        {
            if (czyShift == 1)
            {
                poleText += "T";
            }
            else
            {
                poleText += "t";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyY == 1)
        {
            if (czyShift == 1)
            {
                poleText += "Y";
            }
            else
            {
                poleText += "y";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyU == 1)
        {
            if (czyShift == 1)
            {
                poleText += "U";
            }
            else
            {
                poleText += "u";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyI == 1)
        {
            if (czyShift == 1)
            {
                poleText += "I";
            }
            else
            {
                poleText += "i";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyO == 1)
        {
            if (czyShift == 1)
            {
                poleText += "O";
            }
            else
            {
                poleText += "o";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyP == 1)
        {
            if (czyShift == 1)
            {
                poleText += "P";
            }
            else
            {
                poleText += "p";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyF == 1)
        {
            if (czyShift == 1)
            {
                poleText += "F";
            }
            else
            {
                poleText += "f";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyG == 1)
        {
            if (czyShift == 1)
            {
                poleText += "G";
            }
            else
            {
                poleText += "g";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyH == 1)
        {
            if (czyShift == 1)
            {
                poleText += "H";
            }
            else
            {
                poleText += "h";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyJ == 1)
        {
            if (czyShift == 1)
            {
                poleText += "J";
            }
            else
            {
                poleText += "j";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyK == 1)
        {
            if (czyShift == 1)
            {
                poleText += "K";
            }
            else
            {
                poleText += "k";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyL == 1)
        {
            if (czyShift == 1)
            {
                poleText += "L";
            }
            else
            {
                poleText += "l";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyZ == 1)
        {
            if (czyShift == 1)
            {
                poleText += "Z";
            }
            else
            {
                poleText += "z";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyX == 1)
        {
            if (czyShift == 1)
            {
                poleText += "X";
            }
            else
            {
                poleText += "x";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyC == 1)
        {
            if (czyShift == 1)
            {
                poleText += "C";
            }
            else
            {
                poleText += "c";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyV == 1)
        {
            if (czyShift == 1)
            {
                poleText += "V";
            }
            else
            {
                poleText += "v";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyB == 1)
        {
            if (czyShift == 1)
            {
                poleText += "B";
            }
            else
            {
                poleText += "b";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyN == 1)
        {
            if (czyShift == 1)
            {
                poleText += "N";
            }
            else
            {
                poleText += "n";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyM == 1)
        {
            if (czyShift == 1)
            {
                poleText += "M";
            }
            else
            {
                poleText += "m";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyPodloga == 1)
        {
            if (czyShift == 1)
            {
                poleText += "_";
            }
            else
            {
                poleText += "-";
            }
            czyZmienionoPoleWyboru = 1;
        }
    }
    if (czyObslugaCyfr == 1)
    {
        if (czy0 == 1)
        {
            poleText += "0";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy1 == 1)
        {
            poleText += "1";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy2 == 1)
        {
            poleText += "2";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy3 == 1)
        {
            poleText += "3";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy4 == 1)
        {
            poleText += "4";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy5 == 1)
        {
            poleText += "5";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy6 == 1)
        {
            poleText += "6";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy7 == 1)
        {
            poleText += "7";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy8 == 1)
        {
            poleText += "8";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czy9 == 1)
        {
            poleText += "9";
            czyZmienionoPoleWyboru = 1;
        }
    }
    if (czyBackspace == 1)
    {
        BackspaceCykl++;
        if (poleText.size() > 0)
        {
            if (BackspaceCykl >= 6)
            {
                poleText.erase(poleText.size() - 1, 1);
                BackspaceCykl = 0;
            }
        }
    }
    else if (czySpacja == 1)
    {
        poleText += " ";
        czyZmienionoPoleWyboru = 1;
    }
}

// funkcje przycisków w zależności od menu w którym się znajdujemy
void przyciski(ALLEGRO_DISPLAY* okno)
{
    //cout << "przyciski" << endl;
    // obsluga wyjscia z fullscreen start
    if (czyWindowsStart == 1 && czyZmienionoPoleWyboru == 0)
    {
        if (czyOknoFull == 1)
        {
            al_set_display_flag(okno, ALLEGRO_FULLSCREEN_WINDOW, false);
            obrazX = al_get_display_width(okno);
            obrazY = al_get_display_height(okno);
            polowaObrazX = obrazX / 2;
            polowaObrazY = obrazY / 2;
            czyOknoFull = 0;
        }
        else if (czyOknoFull == 0)
        {
            al_set_display_flag(okno, ALLEGRO_FULLSCREEN_WINDOW, true);
            obrazX = al_get_display_width(okno);
            obrazY = al_get_display_height(okno);
            polowaObrazX = obrazX / 2;
            polowaObrazY = obrazY / 2;
            czyOknoFull = 1;
        }
        czyZmienionoPoleWyboru = 1;
    }
    // obsluga wyjscia z fullscreen end

    // wywolanie funkcji ze stanem zmiennych start
    if (czyTylda == 1 && czyZmienionoPoleWyboru == 0)
    {
        dane();
        czyZmienionoPoleWyboru = 1;
    }
    // wywolanie funkcji ze stanem zmiennych stop

    // wywolanie funkcji ze stanem tablicy katB start
    if (czyInsert == 1 && czyZmienionoPoleWyboru == 0)
    {
        daneKatB();
        czyZmienionoPoleWyboru = 1;
    }
    // wywolanie funkcji ze stanem tablicy katB start

    if (wyborMenuG == "menu glowne")
    {
        if (czyStrzalkaDol == 1 && czyZmienionoPoleWyboru == 0) // zmiana przycisku strzalkami w dol
        {
            cout << "czyStrzalkaDol = 1" << endl;
            if (zaznaczenie == "Baza")
            {
                zaznaczenie = "Informacje";
            }
            else if (zaznaczenie == "Informacje")
            {
                zaznaczenie = "Exit";
            }
            else if (zaznaczenie == "Exit")
            {
                zaznaczenie = "Baza";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaGora == 1 && czyZmienionoPoleWyboru == 0) // zmiana przycisku strzalkami w gore
        {
            cout << "czyStrzalkaGora = 1" << endl;
            if (zaznaczenie == "Baza")
            {
                zaznaczenie = "Exit";
            }
            else if (zaznaczenie == "Informacje")
            {
                zaznaczenie = "Baza";
            }
            else if (zaznaczenie == "Exit")
            {
                zaznaczenie = "Informacje";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyEnter == 1 && czyZmienionoPoleWyboru == 0) // zatwierdzenie wyboru przycisku enterem
        {
            cout << "czyEnter = 1" << endl;
            if (zaznaczenie == "Baza")
            {
                wyborMenuG = "Baza";
                poprzednieOkno = "menu glowne";
                czyZmienionoPoleWyboru = 1;
                czyZaladowacKategorie = 1;
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "Informacje")
            {
                wyborMenuG = "Informacje";
                czyZmienionoPoleWyboru = 1;
            }
            else if (zaznaczenie == "Exit")
            {
                wyborMenuG = "chyba Wyjscie";
                poprzednieOkno = "menu glowne";
                czyZmienionoPoleWyboru = 1;
                czyZmianaMenu = 1;
            }
        }
        else if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "czyESC = 1" << endl;
            wyborMenuG = "chyba Wyjscie";
            poprzednieOkno = "menu glowne";
            czyZmienionoPoleWyboru = 1;
            czyZmianaMenu = 1;
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto==0)
        {
            cout << "klik" << endl;
            if (myszX > polowaObrazX - 150 && myszX < polowaObrazX + 150)
            {
                if (myszY > polowaObrazY - 95 && myszY < polowaObrazY - 45)
                {
                    if (zaznaczenie != "Baza")
                    {
                        zaznaczenie = "Baza";
                    }
                    else if(zaznaczenie == "Baza")
                    {
                        wyborMenuG = "Baza";
                        poprzednieOkno = "menu glowne";
                        czyZaladowacKategorie = 1;
                        czyZmianaMenu = 1;
                    }
                }
                else if (myszY > polowaObrazY - 25 && myszY < polowaObrazY + 25)
                {
                    if (zaznaczenie != "Informacje")
                    {
                        zaznaczenie = "Informacje";
                    }
                    else if (zaznaczenie == "Informacje")
                    {
                        wyborMenuG = "Informacje";
                        poprzednieOkno = "menu glowne";
                    }
                }
                else if (myszY > polowaObrazY + 45 && myszY < polowaObrazY + 95)
                {
                    if (zaznaczenie != "Exit")
                    {
                        zaznaczenie = "Exit";
                    }
                    else if (zaznaczenie == "Exit")
                    {
                        wyborMenuG = "chyba Wyjscie";
                        poprzednieOkno = "menu glowne";
                        czyZmianaMenu = 1;
                    }
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "chyba Wyjscie")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "czyESC = 1" << endl;
            wyborMenuG = "menu glowne";
            poprzednieOkno = "chyba Wyjscie";
            czyZmienionoPoleWyboru = 1;
            czyZmianaMenu = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "czyStrzalkaLewo = 1" << endl;
            if (zaznaczenie == "Tak")
            {
                zaznaczenie = "Nie";
            }
            else if (zaznaczenie == "Nie")
            {
                zaznaczenie = "Tak";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "czyStrzalkaPrawo = 1" << endl;
            if (zaznaczenie == "Tak")
            {
                zaznaczenie = "Nie";
            }
            else if (zaznaczenie == "Nie")
            {
                zaznaczenie = "Tak";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "czyEnter = 1" << endl;
            if (zaznaczenie == "Tak")
            {
                wyborMenuG = "wylanczenie";
            }
            else if (zaznaczenie == "Nie")
            {
                wyborMenuG = "menu glowne";
                poprzednieOkno = "chyba Wyjscie";
                czyZmianaMenu = 1;
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 150 && myszX< polowaObrazX - 100 && myszY>polowaObrazY + 5 && myszY < polowaObrazY + 55)
            {
                if (zaznaczenie == "Tak")
                {
                    wyborMenuG = "wylanczenie";
                }
                else if (zaznaczenie == "Nie")
                {
                    zaznaczenie = "Tak";
                }
            }
            if (myszX > polowaObrazX + 100 && myszX < polowaObrazX + 150 && myszY > polowaObrazY + 5 && myszY < polowaObrazY + 55)
            {
                if (zaznaczenie == "Nie")
                {
                    wyborMenuG = "menu glowne";
                    poprzednieOkno = "chyba Wyjscie";
                    czyZmianaMenu = 1;
                }
                else if (zaznaczenie == "Tak")
                {
                    zaznaczenie = "Nie";
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "Informacje")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            wyborMenuG = "menu glowne";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czySpacja == 1 && czyZmienionoPoleWyboru == 0)
        {
            wyborMenuG = "Blad";
            poprzednieOkno = "Informacje";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyW == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "W" << endl;
        }
        else if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            wyborMenuG = "menu glowne";
            poprzednieOkno = "Informacje";
            czyZmianaMenu = 1;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyX==1 && czyZmienionoPoleWyboru==0)
        {
            cout << "X" << endl;
            wyborMenuG = "OknoTestowe";
            czyZmienionoPoleWyboru = 1;
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 50 && myszX< polowaObrazX + 50 && myszY>polowaObrazY + 190 && myszY < polowaObrazY + 230)
            {
                wyborMenuG = "menu glowne";
                poprzednieOkno = "Informacje";
                czyZmianaMenu = 1;
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "Blad")
    {
        if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            wyborMenuG = poprzednieOkno;
            if(poprzednieOkno=="Baza")
            {
                czyZaladowacKategorie = 1;
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 25 && myszX< polowaObrazX + 25 && myszY>polowaObrazY + 5 && myszY < polowaObrazY + 45)
            {
                wyborMenuG = poprzednieOkno;
                czyZmianaMenu = 1;
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "Baza")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            wyborMenuG = "menu glowne";
            poprzednieOkno = "Baza";
            czyZmianaMenu = 1;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka prawo" << endl;
            if (zaznaczenie == "Powrot")
            {
                zaznaczenie = "Nowa kategoria";
            }
            else if (zaznaczenie == "Nowa kategoria")
            {
                zaznaczenie = "Powrot";
            }
            else if (zaznaczenie == "KatStronaDown" || zaznaczenie == "KatStronaUp")
            {
                if (tKatPrzycY > iloscKatNaStronie-1 && tKatPrzycY > 0)
                {
                    tKatPrzycY--;
                }
                if (tKatPrzycY > iloscKatNaStronie - 1 && iloscKatNaStronie > 0)
                {
                    tKatPrzycY = iloscKatNaStronie - 1;
                }
                zaznaczenie = tKatPrzyc[tKatPrzycX][tKatPrzycY];
            }
            else if (zaznaczenie != "Powrot" && zaznaczenie != "Nowa kategoria" && zaznaczenie != "KatStronaDown" && zaznaczenie != "KatStronaUp")
            {
                if (tKatPrzycX < 2)
                {
                    tKatPrzycX++;
                    zaznaczenie = tKatPrzyc[tKatPrzycX][tKatPrzycY];
                }
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka lewo" << endl;
            if (zaznaczenie == "Powrot")
            {
                zaznaczenie = "Nowa kategoria";
            }
            else if (zaznaczenie == "Nowa kategoria")
            {
                zaznaczenie = "KatStronaDown";
            }
            else if (zaznaczenie != "Powrot" && zaznaczenie != "Nowa kategoria" && zaznaczenie!="KatStronaDown" && zaznaczenie != "KatStronaUp")
            {
                if (tKatPrzycX >0)
                {
                    tKatPrzycX--;
                    zaznaczenie = tKatPrzyc[tKatPrzycX][tKatPrzycY];
                }
                else
                {
                    zaznaczenie = "KatStronaDown";
                }
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaGora == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka gora" << endl;
            if (zaznaczenie == "Powrot" || zaznaczenie == "Nowa kategoria")
            {
                if (tKatPrzycY > iloscKatNaStronie-1 && tKatPrzycY > 0)
                {
                    tKatPrzycY--;
                }
                if (tKatPrzycY > iloscKatNaStronie - 1 && iloscKatNaStronie > 0)
                {
                    tKatPrzycY = iloscKatNaStronie - 1;
                }
                zaznaczenie = tKatPrzyc[tKatPrzycX][tKatPrzycY];
      
            }
            else if (zaznaczenie == "KatStronaDown")
            {
                zaznaczenie = "KatStronaUp";
            }
            else if (zaznaczenie != "Powrot" && zaznaczenie != "Nowa kategoria" && zaznaczenie!="KatStronaDown" && zaznaczenie!="KatStronaUp")
            {
                if (tKatPrzycY > 0)
                {
                    tKatPrzycY--;
                }
                if (tKatPrzycY > iloscKatNaStronie - 1 && iloscKatNaStronie > 0)
                {
                    tKatPrzycY = iloscKatNaStronie - 1;
                }
                zaznaczenie = tKatPrzyc[tKatPrzycX][tKatPrzycY];
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaDol == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka dol" << endl;
            if (zaznaczenie != "Powrot" && zaznaczenie != "Nowa kategoria" && zaznaczenie != "KatStronaDown" && zaznaczenie != "KatStronaUp")
            {
                if (tKatPrzycY < iloscKatNaStronie - 1)
                {
                    tKatPrzycY++;
                    zaznaczenie = tKatPrzyc[tKatPrzycX][tKatPrzycY];
                }
                else
                {
                    if (tKatPrzycY < iloscKatNaStronie)
                    {
                        tKatPrzycY++;
                    }
                    zaznaczenie = "Powrot";
                }
            }
            else if (zaznaczenie == "KatStronaDown")
            {
                zaznaczenie = "Nowa kategoria";
            }
            else if (zaznaczenie == "KatStronaUp")
            {
                zaznaczenie = "KatStronaDown";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "czyEnter = 1" << endl;
            if (zaznaczenie == "Powrot")
            {
                wyborMenuG = "menu glowne";
                poprzednieOkno = "Baza";
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "Nowa kategoria")
            {
                wyborMenuG = "NowaKat";
                poprzednieOkno = "Baza";
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "KatStronaUp")
            {
                cout << "KatStronaUp" << endl;
                if (stronaTabeliKat > 0)
                {
                    stronaTabeliKat--;
                    cout << "Strona tabeli kat --" << endl;
                }
            }
            else if (zaznaczenie == "KatStronaDown")
            {
                cout << "KatStronaDown" << endl;
                if ((iloscWczytanychKat/5)>stronaTabeliKat)
                {
                    stronaTabeliKat++;
                    cout << "Strona tabeli kat ++" << endl;
                }
            }
            else if (zaznaczenie == "oko")
            {
                cout << "oko" << endl;
                wyborMenuG = "kat_X_przeglad";
                poprzednieOkno = "Baza";
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "edycja")
            {
                cout << "edycja" << endl;
                poprzednieOkno = "Baza";
                wyborMenuG = "EdycjaKat";
                czyPrzypisacNowyTekstPola = 1;
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "kosz")
            {
                cout << "kosz" << endl;
                poprzednieOkno = "Baza";
                wyborMenuG = "chybaUsunKat";
                czyZmianaMenu = 1;
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX + 73 && myszX < polowaObrazX + 123) // oko
            {
                if (myszY > polowaObrazY - 132 && myszY < polowaObrazY - 82)
                {
                    if (iloscKatNaStronie - 1 >= 0)
                    {
                        if (zaznaczenie != "oko" || tKatPrzycY != 0)
                        {
                            tKatPrzycY = 0;
                            zaznaczenie = "oko";
                        }
                        else if (zaznaczenie == "oko" && tKatPrzycY == 0)
                        {
                            wyborMenuG = "kat_X_przeglad";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 80 && myszY < polowaObrazY - 30)
                {
                    if (iloscKatNaStronie - 1 >= 1)
                    {
                        if (zaznaczenie != "oko" || tKatPrzycY != 1)
                        {
                            tKatPrzycY = 1;
                            zaznaczenie = "oko";
                        }
                        else if (zaznaczenie == "oko" && tKatPrzycY == 1)
                        {
                            wyborMenuG = "kat_X_przeglad";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 28 && myszY < polowaObrazY + 22)
                {
                    if (iloscKatNaStronie - 1 >= 2)
                    {
                        if (zaznaczenie != "oko" || tKatPrzycY != 2)
                        {
                            tKatPrzycY = 2;
                            zaznaczenie = "oko";
                        }
                        else if (zaznaczenie == "oko" && tKatPrzycY == 2)
                        {
                            wyborMenuG = "kat_X_przeglad";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 24 && myszY < polowaObrazY + 74)
                {
                    if (iloscKatNaStronie - 1 >= 3)
                    {
                        if (zaznaczenie != "oko" || tKatPrzycY != 3)
                        {
                            tKatPrzycY = 3;
                            zaznaczenie = "oko";
                        }
                        else if (zaznaczenie == "oko" && tKatPrzycY == 3)
                        {
                            wyborMenuG = "kat_X_przeglad";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 76 && myszY < polowaObrazY + 128)
                {
                    if (iloscKatNaStronie - 1 >= 4)
                    {
                        if (zaznaczenie != "oko" || tKatPrzycY != 4)
                        {
                            tKatPrzycY = 4;
                            zaznaczenie = "oko";
                        }
                        else if (zaznaczenie == "oko" && tKatPrzycY == 4)
                        {
                            wyborMenuG = "kat_X_przeglad";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
            }
            if (myszX > polowaObrazX + 125 && myszX < polowaObrazX + 175) // edycja
            {
                if (myszY > polowaObrazY - 132 && myszY < polowaObrazY - 82)
                {
                    if (iloscKatNaStronie - 1 >= 0)
                    {
                        if (zaznaczenie != "edycja" || tKatPrzycY != 0)
                        {
                            tKatPrzycY = 0;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tKatPrzycY == 0)
                        {
                            wyborMenuG = "EdycjaKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 80 && myszY < polowaObrazY - 30)
                {
                    if (iloscKatNaStronie - 1 >= 1)
                    {
                        if (zaznaczenie != "edycja" || tKatPrzycY != 1)
                        {
                            tKatPrzycY = 1;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tKatPrzycY == 1)
                        {
                            wyborMenuG = "EdycjaKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 28 && myszY < polowaObrazY + 22)
                {
                    if (iloscKatNaStronie - 1 >= 2)
                    {
                        if (zaznaczenie != "edycja" || tKatPrzycY != 2)
                        {
                            tKatPrzycY = 2;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tKatPrzycY == 2)
                        {
                            wyborMenuG = "EdycjaKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 24 && myszY < polowaObrazY + 74)
                {
                    if (iloscKatNaStronie - 1 >= 3)
                    {
                        if (zaznaczenie != "edycja" || tKatPrzycY != 3)
                        {
                            tKatPrzycY = 3;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tKatPrzycY == 3)
                        {
                            wyborMenuG = "EdycjaKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 76 && myszY < polowaObrazY + 128)
                {
                    if (iloscKatNaStronie - 1 >= 4)
                    {
                        if (zaznaczenie != "edycja" || tKatPrzycY != 4)
                        {
                            tKatPrzycY = 4;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tKatPrzycY == 4)
                        {
                            wyborMenuG = "EdycjaKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
            }
            if (myszX > polowaObrazX + 177 && myszX < polowaObrazX + 227) // kosz
            {
                if (myszY > polowaObrazY - 132 && myszY < polowaObrazY - 82)
                {
                    if (iloscKatNaStronie - 1 >= 0)
                    {
                        if (zaznaczenie != "kosz" || tKatPrzycY != 0)
                        {
                            tKatPrzycY = 0;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tKatPrzycY == 0)
                        {
                            wyborMenuG = "chybaUsunKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 80 && myszY < polowaObrazY - 30)
                {
                    if (iloscKatNaStronie - 1 >= 1)
                    {
                        if (zaznaczenie != "kosz" || tKatPrzycY != 1)
                        {
                            tKatPrzycY = 1;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tKatPrzycY == 1)
                        {
                            wyborMenuG = "chybaUsunKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 28 && myszY < polowaObrazY + 22)
                {
                    if (iloscKatNaStronie - 1 >= 2)
                    {
                        if (zaznaczenie != "kosz" || tKatPrzycY != 2)
                        {
                            tKatPrzycY = 2;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tKatPrzycY == 2)
                        {
                            wyborMenuG = "chybaUsunKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 24 && myszY < polowaObrazY + 74)
                {
                    if (iloscKatNaStronie - 1 >= 3)
                    {
                        if (zaznaczenie != "kosz" || tKatPrzycY != 3)
                        {
                            tKatPrzycY = 3;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tKatPrzycY == 3)
                        {
                            wyborMenuG = "chybaUsunKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 76 && myszY < polowaObrazY + 128)
                {
                    if (iloscKatNaStronie - 1 >= 4)
                    {
                        if (zaznaczenie != "kosz" || tKatPrzycY != 4)
                        {
                            tKatPrzycY = 4;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tKatPrzycY == 4)
                        {
                            wyborMenuG = "chybaUsunKat";
                            poprzednieOkno = "Baza";
                            czyZmianaMenu = 1;
                        }
                    }
                }
            }
            if (myszX > polowaObrazX - 150 && myszX< polowaObrazX + 10 && myszY>polowaObrazY + 144 && myszY < polowaObrazY + 194)// nowa kategoria
            {
                if (zaznaczenie != "Nowa kategoria")
                {
                    zaznaczenie = "Nowa kategoria";
                }
                else if (zaznaczenie == "Nowa kategoria")
                {
                    wyborMenuG = "NowaKat";
                    poprzednieOkno = "Baza";
                    czyZmianaMenu = 1;
                }
            }
            if (myszX > polowaObrazX + 50 && myszX< polowaObrazX + 140 && myszY>polowaObrazY + 144 && myszY < polowaObrazY + 194)// powrot
            {
                if (zaznaczenie != "Powrot")
                {
                    zaznaczenie = "Powrot";
                }
                else if (zaznaczenie == "Powrot")
                {
                    wyborMenuG = "menu glowne";
                    poprzednieOkno = "Baza";
                    czyZmianaMenu = 1;
                }
            }
            if (myszX > polowaObrazX - 258 && myszX< polowaObrazX - 229 && myszY>polowaObrazY - 184 && myszY < polowaObrazY - 132)// strzalka gora
            {
                if (zaznaczenie != "KatStronaUp")
                {
                    zaznaczenie = "KatStronaUp";
                }
                else if (zaznaczenie == "KatStronaUp")
                {
                    if (stronaTabeliKat > 0)
                    {
                        stronaTabeliKat--;
                    }
                }
            }
            if (myszX > polowaObrazX - 258 && myszX< polowaObrazX - 229 && myszY>polowaObrazY + 76 && myszY < polowaObrazY + 126)// strzalka dol
            {
                if (zaznaczenie != "KatStronaDown")
                {
                    zaznaczenie = "KatStronaDown";
                }
                else if (zaznaczenie == "KatStronaDown")
                {
                    if ((iloscWczytanychKat / 5) > stronaTabeliKat)
                    {
                        stronaTabeliKat++;
                    }
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "NowaKat")
    {
        if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Enter" << endl;
            if (zaznaczenie == "Anuluj")
            {
                wyborMenuG = "Baza";
                poprzednieOkno = "NowaKat";
                wartoscPolaText = "";
                czyZmianaMenu = 1;
            }
            if (zaznaczenie == "Zatwierdz")
            {
                cout << "Przycisk zatwierdz" << endl;
                if (wartoscPolaText != "")
                {
                    cout << "Zatwierdzenie nazwy kategorii jako: " <<wartoscPolaText<< endl;
                    czyWartoscPolaTextPusta = 0;
                }
                else
                {
                    cout << "Blad: pole nazwy kategorii jest puste." << endl; // generacja okna bledu
                    czyWartoscPolaTextPusta = 1;
                }
                czyZatwierdzonoNazweKat = 1;
            }
            if (zaznaczenie == "PoleText")
            {
                cout << "Pole tekstowe" << endl;
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka prawo" << endl;
            if (zaznaczenie == "Anuluj")
            {
                zaznaczenie = "Zatwierdz";
            }
            else if (zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "Anuluj";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka lewo" << endl;
            if (zaznaczenie == "Anuluj")
            {
                zaznaczenie = "Zatwierdz";
            }
            else if (zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "Anuluj";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaGora == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka gora" << endl;
            if (zaznaczenie == "Anuluj" || zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "PoleText";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaDol == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka dol" << endl;
            if (zaznaczenie == "PoleText")
            {
                zaznaczenie = "Zatwierdz";
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (zaznaczenie == "PoleText" && czyZmienionoPoleWyboru == 0)
        {
            sprawdzPoleTesktowe(wartoscPolaText, 1, 1);
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 75 && myszX< polowaObrazX + 175 && myszY>polowaObrazY - 55 && myszY < polowaObrazY - 5)
            {
                if (zaznaczenie != "PoleText")
                {
                    zaznaczenie = "PoleText";
                }
            }
            if (myszX > polowaObrazX - 175 && myszX< polowaObrazX - 100 && myszY>polowaObrazY + 35 && myszY < polowaObrazY + 85)
            {
                if (zaznaczenie != "Anuluj")
                {
                    zaznaczenie = "Anuluj";
                }
                else if (zaznaczenie == "Anuluj")
                {
                    wyborMenuG = "Baza";
                    poprzednieOkno = "NowaKat";
                    wartoscPolaText = "";
                    czyZmianaMenu = 1;
                }
            }
            if (myszX > polowaObrazX + 65 && myszX< polowaObrazX + 175 && myszY>polowaObrazY + 35 && myszY < polowaObrazY + 85)
            {
                if (zaznaczenie != "Zatwierdz")
                {
                    zaznaczenie = "Zatwierdz";
                }
                else if (zaznaczenie == "Zatwierdz")
                {
                    if (wartoscPolaText != "")
                    {
                        cout << "Zatwierdzenie nazwy kategorii jako: " << wartoscPolaText << endl;
                        czyWartoscPolaTextPusta = 0;
                    }
                    else
                    {
                        cout << "Blad: pole nazwy kategorii jest puste." << endl; // generacja okna bledu
                        czyWartoscPolaTextPusta = 1;
                    }
                    czyZatwierdzonoNazweKat = 1;
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "OknoTestowe")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {  
            wyborMenuG = "Informacje";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyW == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "W" << endl;
            OknoTestoweZmiennaA = 1;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyO == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "O" << endl;
            OknoTestoweZmiennaC = 1;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyP == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "P" << endl;
            OknoTestoweZmiennaTekst = "P";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyI == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "I" << endl;
            OknoTestoweZmiennaTekst = "I";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyT == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "T" << endl;
            OknoTestoweZmiennaTekst = "T";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyY == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Y" << endl;
            OknoTestoweZmiennaTekst = "Y";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyN == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "N" << endl;
            OknoTestoweZmiennaTekst = "N";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyM == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "M" << endl;
            OknoTestoweZmiennaTekst = "M";
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyB == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "B" << endl;
            OknoTestoweZmiennaTekst = "B";
            czyZmienionoPoleWyboru = 1;
        }
    }
    else if (wyborMenuG == "chybaUsunKat")
    {
        if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Enter" << endl;
            if (zaznaczenie == "Tak")
            {
                cout << "Tak" << endl;
                czyUsuwamyKat = 1;
            }
            else if (zaznaczenie == "Nie")
            {
                wyborMenuG = "Baza";
                poprzednieOkno = "chybaUsunKat";
                czyZmianaMenu = 1;
                cout << "Nie" << endl;
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka lewo" << endl;
            if (zaznaczenie == "Tak")
            {
                zaznaczenie = "Nie";
            }
            else if (zaznaczenie == "Nie")
            {
                zaznaczenie = "Tak";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka prawo" << endl;
            if (zaznaczenie == "Tak")
            {
                zaznaczenie = "Nie";
            }
            else if (zaznaczenie == "Nie")
            {
                zaznaczenie = "Tak";
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 150 && myszX< polowaObrazX - 100 && myszY>polowaObrazY + 5 && myszY < polowaObrazY + 55)
            {
                if (zaznaczenie != "Tak")
                {
                    zaznaczenie = "Tak";
                }
                else if (zaznaczenie == "Tak")
                {
                    czyUsuwamyKat = 1;
                }
            }
            if (myszX > polowaObrazX + 100 && myszX< polowaObrazX + 150 && myszY>polowaObrazY + 5 && myszY < polowaObrazY + 55)
            {
                if (zaznaczenie != "Nie")
                {
                    zaznaczenie = "Nie";
                }
                else if (zaznaczenie == "Nie")
                {
                    wyborMenuG = "Baza";
                    poprzednieOkno = "chybaUsunKat";
                    czyZmianaMenu = 1;
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "EdycjaKat")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "ESC" << endl;
            wyborMenuG = "Informacje";
            poprzednieOkno = "EdycjaKat";
            czyZmianaMenu = 1;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Enter" << endl;
            if (zaznaczenie == "Anuluj")
            {
                wyborMenuG = "Baza";
                poprzednieOkno = "EdycjaKat";
                czyZmianaMenu = 1;
            }
            if (zaznaczenie == "Zatwierdz")
            {
                cout << "Przycisk zatwierdz" << endl;
                if (wartoscPolaText != "")
                {
                    cout << "Zatwierdzenie nazwy kategorii jako: " << wartoscPolaText << endl;
                    czyWartoscPolaTextPusta = 0;
                }
                else
                {
                    cout << "Blad: pole nazwy kategorii jest puste." << endl; // generacja okna bledu
                    czyWartoscPolaTextPusta = 1;
                }
                czyZatwierdzonoNazweKat = 1;
            }
            if (zaznaczenie == "PoleText")
            {
                cout << "Pole tekstowe" << endl;
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka prawo" << endl;
            if (zaznaczenie == "Anuluj")
            {
                zaznaczenie = "Zatwierdz";
            }
            else if (zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "Anuluj";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka lewo" << endl;
            if (zaznaczenie == "Anuluj")
            {
                zaznaczenie = "Zatwierdz";
            }
            else if (zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "Anuluj";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaGora == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka gora" << endl;
            if (zaznaczenie == "Anuluj" || zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "PoleText";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaDol == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka dol" << endl;
            if (zaznaczenie == "PoleText")
            {
                zaznaczenie = "Zatwierdz";
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (zaznaczenie == "PoleText" && czyZmienionoPoleWyboru == 0)
        {
            sprawdzPoleTesktowe(wartoscPolaText, 1, 1);
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 75 && myszX< polowaObrazX + 175 && myszY>polowaObrazY - 55 && myszY < polowaObrazY - 5)
            {
                if (zaznaczenie != "PoleText")
                {
                    zaznaczenie = "PoleText";
                }
            }
            if (myszX > polowaObrazX - 175 && myszX< polowaObrazX - 100 && myszY>polowaObrazY + 35 && myszY < polowaObrazY + 85)
            {
                if (zaznaczenie != "Anuluj")
                {
                    zaznaczenie = "Anuluj";
                }
                else if (zaznaczenie == "Anuluj")
                {
                    wyborMenuG = "Baza";
                    poprzednieOkno = "NowaKat";
                    wartoscPolaText = "";
                    czyZmianaMenu = 1;
                }
            }
            if (myszX > polowaObrazX + 65 && myszX< polowaObrazX + 175 && myszY>polowaObrazY + 35 && myszY < polowaObrazY + 85)
            {
                if (zaznaczenie != "Zatwierdz")
                {
                    zaznaczenie = "Zatwierdz";
                }
                else if (zaznaczenie == "Zatwierdz")
                {
                    if (wartoscPolaText != "")
                    {
                        cout << "Zatwierdzenie nazwy kategorii jako: " << wartoscPolaText << endl;
                        czyWartoscPolaTextPusta = 0;
                    }
                    else
                    {
                        cout << "Blad: pole nazwy kategorii jest puste." << endl; // generacja okna bledu
                        czyWartoscPolaTextPusta = 1;
                    }
                    czyZatwierdzonoNazweKat = 1;
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "kat_X_przeglad")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "ESC" << endl;
            wyborMenuG = "Baza";
            poprzednieOkno = "kat_X_przeglad";
            czyZmianaMenu = 1;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyI == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "I" << endl;
            cout << "nrWybranejKat: " << nrWybranejKat << endl;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            if (zaznaczenie == "Nowy przedmiot")
            {
                cout << "Nowy item" << endl;
                wyborMenuG = "kat_X_nowyItem";
                poprzednieOkno = "kat_X_przeglad";
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "Powrot")
            {
                cout << "Powrot" << endl;
                wyborMenuG = "Baza";
                poprzednieOkno = "kat_X_przeglad";
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "ItemStronaUp")
            {
                cout << "itemy strona up" << endl;
                if (stronaTabeliItemow > 0)
                {
                    stronaTabeliItemow--;
                    cout << "stronaTabeliItemow--" << endl;
                }
            }
            else if (zaznaczenie == "ItemStronaDown")
            {
                cout << "itemy strona down" << endl;
                if ((tKat[nrWybranejKat].iloscWczytanychItemow / 5) > stronaTabeliItemow)
                {
                    stronaTabeliItemow++;
                    cout << "stronaTabeliItemow++" << endl;
                }
            }
            else if (zaznaczenie == "edycja")
            {
                cout << "edycja" << endl;
                wyborMenuG = "kat_X_EdytujItem";
                poprzednieOkno = "kat_X_przeglad";
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "kosz")
            {
                cout << "usun" << endl;
                wyborMenuG = "kat_X_usunItem";
                poprzednieOkno = "kat_X_przeglad";
                czyZmianaMenu = 1;
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka prawo" << endl;
            if (zaznaczenie == "Powrot")
            {
                zaznaczenie = "Nowy przedmiot";
            }
            else if (zaznaczenie == "Nowy przedmiot")
            {
                zaznaczenie = "Powrot";
            }
            else if (zaznaczenie == "ItemStronaDown" || zaznaczenie == "ItemStronaUp")
            {
                if (tItemPrzycY > iloscItemowNaStronie - 1 && tItemPrzycY > 0)
                {
                    tItemPrzycY--;
                }
                if (tItemPrzycY > iloscItemowNaStronie - 1 && iloscItemowNaStronie > 0)
                {
                    tItemPrzycY = iloscItemowNaStronie - 1;
                }
                zaznaczenie = tItemPrzyc[tItemPrzycX][tItemPrzycY];
            }
            else if (zaznaczenie != "Powrot" && zaznaczenie != "Nowy przedmiot" && zaznaczenie != "ItemStronaDown" && zaznaczenie != "ItemStronaUp")
            {
                if (tItemPrzycX < 2)
                {
                    tItemPrzycX++;
                    zaznaczenie = tItemPrzyc[tItemPrzycX][tItemPrzycY];
                }
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka lewo" << endl;
            if (zaznaczenie == "Powrot")
            {
                zaznaczenie = "Nowy przedmiot";
            }
            else if (zaznaczenie == "Nowy przedmiot")
            {
                zaznaczenie = "ItemStronaDown";
            }
            else if (zaznaczenie != "Powrot" && zaznaczenie != "Nowy przedmiot" && zaznaczenie != "ItemStronaDown" && zaznaczenie != "ItemStronaUp")
            {
                if (tItemPrzycX > 0)
                {
                    tItemPrzycX--;
                    zaznaczenie = tItemPrzyc[tItemPrzycX][tItemPrzycY];
                }
                else
                {
                    zaznaczenie = "ItemStronaDown";
                }
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaGora == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka gora" << endl;
            if (zaznaczenie == "Powrot" || zaznaczenie == "Nowy przedmiot")
            {
                if (tItemPrzycY > iloscItemowNaStronie - 1 && tItemPrzycY > 0)
                {
                    tItemPrzycY--;
                }
                if (tItemPrzycY > iloscItemowNaStronie - 1 && iloscItemowNaStronie > 0)
                {
                    tItemPrzycY = iloscItemowNaStronie - 1;
                }
                zaznaczenie = tItemPrzyc[tItemPrzycX][tItemPrzycY];

            }
            else if (zaznaczenie == "ItemStronaDown")
            {
                zaznaczenie = "ItemStronaUp";
            }
            else if (zaznaczenie != "Powrot" && zaznaczenie != "Nowy przedmiot" && zaznaczenie != "ItemStronaDown" && zaznaczenie != "ItemStronaUp")
            {
                if (tItemPrzycY > 0)
                {
                    tItemPrzycY--;
                }
                if (tItemPrzycY > iloscItemowNaStronie - 1 && iloscItemowNaStronie > 0)
                {
                    tItemPrzycY = iloscItemowNaStronie - 1;
                }
                zaznaczenie = tItemPrzyc[tItemPrzycX][tItemPrzycY];
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaDol == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka dol" << endl;
            if (zaznaczenie != "Powrot" && zaznaczenie != "Nowy przedmiot" && zaznaczenie != "ItemStronaDown" && zaznaczenie != "ItemStronaUp")
            {
                if (tItemPrzycY < iloscItemowNaStronie - 1)
                {
                    tItemPrzycY++;
                    zaznaczenie = tItemPrzyc[tItemPrzycX][tItemPrzycY];
                }
                else
                {
                    if (tItemPrzycY < iloscItemowNaStronie)
                    {
                        tItemPrzycY++;
                    }
                    zaznaczenie = "Powrot";
                }
            }
            else if (zaznaczenie == "ItemStronaDown")
            {
                zaznaczenie = "Nowy przedmiot";
            }
            else if (zaznaczenie == "ItemStronaUp")
            {
                zaznaczenie = "ItemStronaDown";
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX + 191 && myszX < polowaObrazX + 241) // edycja
            {
                if (myszY > polowaObrazY - 132 && myszY < polowaObrazY - 82)
                {
                    if (iloscItemowNaStronie - 1 >= 0)
                    {
                        if (zaznaczenie != "edycja" || tItemPrzycY != 0)
                        {
                            tItemPrzycY = 0;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tItemPrzycY == 0)
                        {
                            wyborMenuG = "kat_X_EdytujItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 80 && myszY < polowaObrazY - 30)
                {
                    if (iloscItemowNaStronie - 1 >= 1)
                    {
                        if (zaznaczenie != "edycja" || tItemPrzycY != 1)
                        {
                            tItemPrzycY = 1;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tItemPrzycY == 1)
                        {
                            wyborMenuG = "kat_X_EdytujItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 28 && myszY < polowaObrazY + 22)
                {
                    if (iloscItemowNaStronie - 1 >= 2)
                    {
                        if (zaznaczenie != "edycja" || tItemPrzycY != 2)
                        {
                            tItemPrzycY = 2;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tItemPrzycY == 2)
                        {
                            wyborMenuG = "kat_X_EdytujItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 24 && myszY < polowaObrazY + 74)
                {
                    if (iloscItemowNaStronie - 1 >= 3)
                    {
                        if (zaznaczenie != "edycja" || tItemPrzycY != 3)
                        {
                            tItemPrzycY = 3;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tItemPrzycY == 3)
                        {
                            wyborMenuG = "kat_X_EdytujItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 76 && myszY < polowaObrazY + 128)
                {
                    if (iloscItemowNaStronie - 1 >= 4)
                    {
                        if (zaznaczenie != "edycja" || tItemPrzycY != 4)
                        {
                            tItemPrzycY = 4;
                            zaznaczenie = "edycja";
                        }
                        else if (zaznaczenie == "edycja" && tItemPrzycY == 4)
                        {
                            wyborMenuG = "kat_X_EdytujItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
            }
            if (myszX > polowaObrazX + 243 && myszX < polowaObrazX + 293) // kosz
            {
                if (myszY > polowaObrazY - 132 && myszY < polowaObrazY - 82)
                {
                    if (iloscItemowNaStronie - 1 >= 0)
                    {
                        if (zaznaczenie != "kosz" || tItemPrzycY != 0)
                        {
                            tItemPrzycY = 0;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tItemPrzycY == 0)
                        {
                            wyborMenuG = "kat_X_usunItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 80 && myszY < polowaObrazY - 30)
                {
                    if (iloscItemowNaStronie - 1 >= 1)
                    {
                        if (zaznaczenie != "kosz" || tItemPrzycY != 1)
                        {
                            tItemPrzycY = 1;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tItemPrzycY == 1)
                        {
                            wyborMenuG = "kat_X_usunItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY - 28 && myszY < polowaObrazY + 22)
                {
                    if (iloscItemowNaStronie - 1 >= 2)
                    {
                        if (zaznaczenie != "kosz" || tItemPrzycY != 2)
                        {
                            tItemPrzycY = 2;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tItemPrzycY == 2)
                        {
                            wyborMenuG = "kat_X_usunItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 24 && myszY < polowaObrazY + 74)
                {
                    if (iloscItemowNaStronie - 1 >= 3)
                    {
                        if (zaznaczenie != "kosz" || tItemPrzycY != 3)
                        {
                            tItemPrzycY = 3;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tItemPrzycY == 3)
                        {
                            wyborMenuG = "kat_X_usunItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
                if (myszY > polowaObrazY + 76 && myszY < polowaObrazY + 128)
                {
                    if (iloscItemowNaStronie - 1 >= 4)
                    {
                        if (zaznaczenie != "kosz" || tItemPrzycY != 4)
                        {
                            tItemPrzycY = 4;
                            zaznaczenie = "kosz";
                        }
                        else if (zaznaczenie == "kosz" && tItemPrzycY == 4)
                        {
                            wyborMenuG = "kat_X_usunItem";
                            poprzednieOkno = "kat_X_przeglad";
                            czyZmianaMenu = 1;
                        }
                    }
                }
            }
            if (myszX > polowaObrazX - 150 && myszX< polowaObrazX + 10 && myszY>polowaObrazY + 144 && myszY < polowaObrazY + 194)// nowy item
            {
                if (zaznaczenie != "Nowy przedmiot")
                {
                    zaznaczenie = "Nowy przedmiot";
                }
                else if (zaznaczenie == "Nowy przedmiot")
                {
                    wyborMenuG = "kat_X_nowyItem";
                    poprzednieOkno = "kat_X_przeglad";
                    czyZmianaMenu = 1;
                }
            }
            if (myszX > polowaObrazX + 50 && myszX< polowaObrazX + 140 && myszY>polowaObrazY + 144 && myszY < polowaObrazY + 194)// powrot
            {
                if (zaznaczenie != "Powrot")
                {
                    zaznaczenie = "Powrot";
                }
                else if (zaznaczenie == "Powrot")
                {
                    wyborMenuG = "Baza";
                    poprzednieOkno = "kat_X_przeglad";
                    czyZmianaMenu = 1;
                }
            }
            if (myszX > polowaObrazX - 323 && myszX< polowaObrazX - 295 && myszY>polowaObrazY - 184 && myszY < polowaObrazY - 132)// strzalka gora
            {
                if (zaznaczenie != "ItemStronaUp")
                {
                    zaznaczenie = "ItemStronaUp";
                }
                else if (zaznaczenie == "ItemStronaUp")
                {
                    if (stronaTabeliItemow > 0)
                    {
                        stronaTabeliItemow--;
                    }
                }
            }
            if (myszX > polowaObrazX - 323 && myszX< polowaObrazX - 295 && myszY>polowaObrazY + 76 && myszY < polowaObrazY + 126)// strzalka dol
            {
                if (zaznaczenie != "ItemStronaDown")
                {
                    zaznaczenie = "ItemStronaDown";
                }
                else if (zaznaczenie == "ItemStronaDown")
                {
                    if ((tKat[nrWybranejKat].iloscWczytanychItemow / 5) > stronaTabeliItemow)
                    {
                        stronaTabeliItemow++;
                    }
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "kat_X_nowyItem")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "ESC" << endl;
            wyborMenuG = "kat_X_przeglad";
            poprzednieOkno = "kat_X_nowyItem";
            czyZmianaMenu = 1;
            czyZmienionoPoleWyboru = 1;
        }
        if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Enter" << endl;
            if (zaznaczenie == "Anuluj")
            {
                wyborMenuG = "kat_X_przeglad";
                poprzednieOkno = "kat_X_nowyItem";
                wartoscPolaTextNazwa = "";
                wartoscPolaTextIlosc = "";
                wartoscPolaTextStan = "";
                czyZmianaMenu = 1;
            }
            if (zaznaczenie == "Zatwierdz")
            {
                cout << "Przycisk zatwierdz" << endl;
                if (wartoscPolaTextNazwa != "" && wartoscPolaTextIlosc!="" && wartoscPolaTextStan!="")
                {
                    cout << "Zatwierdzenie nazwy itemu jako: " << wartoscPolaTextNazwa << endl;
                    cout << "Zatwierdzenie ilosci itemu jako: " << wartoscPolaTextIlosc << endl;
                    cout << "Zatwierdzenie stanu itemu jako: " << wartoscPolaTextStan << endl;
                    czyWartoscPolaTextPusta = 0;
                }
                else
                {
                    cout << "Blad: pole nazwy, ilosci lub stanu itemu jest puste." << endl; // generacja okna bledu
                    czyWartoscPolaTextPusta = 1;
                }
                czyZatwierdzonoNazweItemu = 1;
            }
            if (zaznaczenie == "PoleText")
            {
                cout << "Pole tekstowe" << endl;
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka prawo" << endl;
            if (zaznaczenie == "Anuluj")
            {
                zaznaczenie = "Zatwierdz";
            }
            else if (zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "Anuluj";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka lewo" << endl;
            if (zaznaczenie == "Anuluj")
            {
                zaznaczenie = "Zatwierdz";
            }
            else if (zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "Anuluj";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaGora == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka gora" << endl;
            if (zaznaczenie == "Anuluj" || zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "PoleTextStan";
            }
            else if (zaznaczenie == "PoleTextStan")
            {
                zaznaczenie = "PoleTextIlosc";
            }
            else if (zaznaczenie == "PoleTextIlosc")
            {
                zaznaczenie = "PoleTextNazwa";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaDol == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka dol" << endl;
            if (zaznaczenie == "PoleTextNazwa")
            {
                zaznaczenie = "PoleTextIlosc";
            }
            else if (zaznaczenie == "PoleTextIlosc")
            {
                zaznaczenie = "PoleTextStan";
            }
            else if (zaznaczenie == "PoleTextStan")
            {
                zaznaczenie = "Zatwierdz";
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (zaznaczenie == "PoleTextNazwa" && czyZmienionoPoleWyboru == 0)
        {
            sprawdzPoleTesktowe(wartoscPolaTextNazwa, 1, 1);
        }
        else if (zaznaczenie == "PoleTextIlosc" && czyZmienionoPoleWyboru == 0)
        {
            sprawdzPoleTesktowe(wartoscPolaTextIlosc, 0, 1);
        }
        else if (zaznaczenie == "PoleTextStan" && czyZmienionoPoleWyboru == 0)
        {
            sprawdzPoleTesktowe(wartoscPolaTextStan,1,0);
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 75 && myszX< polowaObrazX + 175 && myszY>polowaObrazY - 118 && myszY < polowaObrazY - 68)
            {
                if (zaznaczenie != "PoleTextNazwa")
                {
                    zaznaczenie = "PoleTextNazwa";
                }
            }
            if (myszX > polowaObrazX - 75 && myszX< polowaObrazX + 175 && myszY>polowaObrazY - 43 && myszY < polowaObrazY + 7)
            {
                if (zaznaczenie != "PoleTextIlosc")
                {
                    zaznaczenie = "PoleTextIlosc";
                }
            }
            if (myszX > polowaObrazX - 75 && myszX< polowaObrazX + 175 && myszY>polowaObrazY + 32 && myszY < polowaObrazY + 82)
            {
                if (zaznaczenie != "PoleTextStan")
                {
                    zaznaczenie = "PoleTextStan";
                }
            }
            if (myszX > polowaObrazX - 175 && myszX< polowaObrazX - 100 && myszY>polowaObrazY + 112 && myszY < polowaObrazY + 162)
            {
                if (zaznaczenie != "Anuluj")
                {
                    zaznaczenie = "Anuluj";
                }
                else if (zaznaczenie == "Anuluj")
                {
                    wyborMenuG = "kat_X_przeglad";
                    poprzednieOkno = "kat_X_nowyItem";
                    wartoscPolaTextNazwa = "";
                    wartoscPolaTextIlosc = "";
                    wartoscPolaTextStan = "";
                    czyZmianaMenu = 1;
                }
            }
            if (myszX > polowaObrazX + 50 && myszX< polowaObrazX + 175 && myszY>polowaObrazY + 112 && myszY < polowaObrazY + 162)
            {
                if (zaznaczenie != "Zatwierdz")
                {
                    zaznaczenie = "Zatwierdz";
                }
                else if (zaznaczenie == "Zatwierdz")
                {
                    if (wartoscPolaTextNazwa != "" && wartoscPolaTextIlosc != "" && wartoscPolaTextStan != "")
                    {
                        cout << "Zatwierdzenie nazwy itemu jako: " << wartoscPolaTextNazwa << endl;
                        cout << "Zatwierdzenie ilosci itemu jako: " << wartoscPolaTextIlosc << endl;
                        cout << "Zatwierdzenie stanu itemu jako: " << wartoscPolaTextStan << endl;
                        czyWartoscPolaTextPusta = 0;
                    }
                    else
                    {
                        cout << "Blad: pole nazwy, ilosci lub stanu itemu jest puste." << endl; // generacja okna bledu
                        czyWartoscPolaTextPusta = 1;
                    }
                    czyZatwierdzonoNazweItemu = 1;
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "kat_X_usunItem")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "ESC" << endl;
            wyborMenuG = "kat_X_przeglad";
            poprzednieOkno = "kat_X_nowyItem";
            czyZmianaMenu = 1;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Enter" << endl;
            if (zaznaczenie == "Nie")
            {
                wyborMenuG = "kat_X_przeglad";
                poprzednieOkno = "kat_X_usunItem";
                czyZmianaMenu = 1;
            }
            else if (zaznaczenie == "Tak")
            {
                czyUsuwamyItem = 1;
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Prawo" << endl;
            if (zaznaczenie == "Tak")
            {
                zaznaczenie = "Nie";
            }
            else if (zaznaczenie == "Nie")
            {
                zaznaczenie = "Tak";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Lewo" << endl;
            if (zaznaczenie == "Tak")
            {
                zaznaczenie = "Nie";
            }
            else if (zaznaczenie == "Nie")
            {
                zaznaczenie = "Tak";
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 150 && myszX< polowaObrazX - 100 && myszY>polowaObrazY - 5 && myszY < polowaObrazY + 45)
            {
                if (zaznaczenie != "Tak")
                {
                    zaznaczenie = "Tak";
                }
                else if (zaznaczenie == "Tak")
                {
                    czyUsuwamyItem = 1;
                }
            }
            if (myszX > polowaObrazX + 100 && myszX< polowaObrazX + 150 && myszY>polowaObrazY - 5 && myszY < polowaObrazY + 45)
            {
                if (zaznaczenie != "Nie")
                {
                    zaznaczenie = "Nie";
                }
                else if (zaznaczenie == "Nie")
                {
                    wyborMenuG = "kat_X_przeglad";
                    poprzednieOkno = "kat_X_usunItem";
                    czyZmianaMenu = 1;
                }
            }
            czyKliknieto = 1;
        }
    }
    else if (wyborMenuG == "kat_X_EdytujItem")
    {
        if (czyESC == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "ESC" << endl;
            wyborMenuG = "kat_X_przeglad";
            poprzednieOkno = "kat_X_EdytujItem";
            czyZmianaMenu = 1;
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyEnter == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Enter" << endl;
            if (zaznaczenie == "Anuluj")
            {
                wyborMenuG = "kat_X_przeglad";
                poprzednieOkno = "kat_X_EdytujItem";
                wartoscPolaText = "";
                czyZmianaMenu = 1;
            }
            if (zaznaczenie == "Zatwierdz")
            {
                cout << "Przycisk zatwierdz" << endl;
                if (wartoscPolaTextNazwa != "" && wartoscPolaTextIlosc != "" && wartoscPolaTextStan != "")
                {
                    cout << "Zatwierdzenie nazwy itemu jako: " << wartoscPolaTextNazwa << endl;
                    cout << "Zatwierdzenie ilosci itemu jako: " << wartoscPolaTextIlosc << endl;
                    cout << "Zatwierdzenie stanu itemu jako: " << wartoscPolaTextStan << endl;
                    czyWartoscPolaTextPusta = 0;
                }
                else
                {
                    cout << "Blad: pole nazwy, ilosci lub stanu itemu jest puste." << endl; // generacja okna bledu
                    czyWartoscPolaTextPusta = 1;
                }
                czyZatwierdzonoNazweItemu = 1;
            }
            if (zaznaczenie == "PoleText")
            {
                cout << "Pole tekstowe" << endl;
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaPrawo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka prawo" << endl;
            if (zaznaczenie == "Anuluj")
            {
                zaznaczenie = "Zatwierdz";
            }
            else if (zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "Anuluj";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaLewo == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka lewo" << endl;
            if (zaznaczenie == "Anuluj")
            {
                zaznaczenie = "Zatwierdz";
            }
            else if (zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "Anuluj";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaGora == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka gora" << endl;
            if (zaznaczenie == "Anuluj" || zaznaczenie == "Zatwierdz")
            {
                zaznaczenie = "PoleTextStan";
            }
            else if (zaznaczenie == "PoleTextStan")
            {
                zaznaczenie = "PoleTextIlosc";
            }
            else if (zaznaczenie == "PoleTextIlosc")
            {
                zaznaczenie = "PoleTextNazwa";
            }
            czyZmienionoPoleWyboru = 1;
        }
        else if (czyStrzalkaDol == 1 && czyZmienionoPoleWyboru == 0)
        {
            cout << "Strzalka dol" << endl;
            if (zaznaczenie == "PoleTextNazwa")
            {
                zaznaczenie = "PoleTextIlosc";
            }
            else if (zaznaczenie == "PoleTextIlosc")
            {
                zaznaczenie = "PoleTextStan";
            }
            else if (zaznaczenie == "PoleTextStan")
            {
                zaznaczenie = "Zatwierdz";
            }
            czyZmienionoPoleWyboru = 1;
        }
        if (zaznaczenie == "PoleTextNazwa" && czyZmienionoPoleWyboru == 0)
        {
            sprawdzPoleTesktowe(wartoscPolaTextNazwa, 1, 1);
        }
        else if (zaznaczenie == "PoleTextIlosc" && czyZmienionoPoleWyboru == 0)
        {
            sprawdzPoleTesktowe(wartoscPolaTextIlosc, 0, 1);
        }
        else if (zaznaczenie == "PoleTextStan" && czyZmienionoPoleWyboru == 0)
        {
            sprawdzPoleTesktowe(wartoscPolaTextStan, 1, 0);
        }
        if (czyLewyMyszyOn == 1 && czyKliknieto == 0) // mysz
        {
            if (myszX > polowaObrazX - 75 && myszX< polowaObrazX + 175 && myszY>polowaObrazY - 118 && myszY < polowaObrazY - 68)
            {
                if (zaznaczenie != "PoleTextNazwa")
                {
                    zaznaczenie = "PoleTextNazwa";
                }
            }
            if (myszX > polowaObrazX - 75 && myszX< polowaObrazX + 175 && myszY>polowaObrazY - 43 && myszY < polowaObrazY + 7)
            {
                if (zaznaczenie != "PoleTextIlosc")
                {
                    zaznaczenie = "PoleTextIlosc";
                }
            }
            if (myszX > polowaObrazX - 75 && myszX< polowaObrazX + 175 && myszY>polowaObrazY + 32 && myszY < polowaObrazY + 82)
            {
                if (zaznaczenie != "PoleTextStan")
                {
                    zaznaczenie = "PoleTextStan";
                }
            }
            if (myszX > polowaObrazX - 175 && myszX< polowaObrazX - 100 && myszY>polowaObrazY + 112 && myszY < polowaObrazY + 162)
            {
                if (zaznaczenie != "Anuluj")
                {
                    zaznaczenie = "Anuluj";
                }
                else if (zaznaczenie == "Anuluj")
                {
                    wyborMenuG = "kat_X_przeglad";
                    poprzednieOkno = "kat_X_nowyItem";
                    wartoscPolaTextNazwa = "";
                    wartoscPolaTextIlosc = "";
                    wartoscPolaTextStan = "";
                    czyZmianaMenu = 1;
                }
            }
            if (myszX > polowaObrazX + 50 && myszX< polowaObrazX + 175 && myszY>polowaObrazY + 112 && myszY < polowaObrazY + 162)
            {
                if (zaznaczenie != "Zatwierdz")
                {
                    zaznaczenie = "Zatwierdz";
                }
                else if (zaznaczenie == "Zatwierdz")
                {
                    if (wartoscPolaTextNazwa != "" && wartoscPolaTextIlosc != "" && wartoscPolaTextStan != "")
                    {
                        cout << "Zatwierdzenie nazwy itemu jako: " << wartoscPolaTextNazwa << endl;
                        cout << "Zatwierdzenie ilosci itemu jako: " << wartoscPolaTextIlosc << endl;
                        cout << "Zatwierdzenie stanu itemu jako: " << wartoscPolaTextStan << endl;
                        czyWartoscPolaTextPusta = 0;
                    }
                    else
                    {
                        cout << "Blad: pole nazwy, ilosci lub stanu itemu jest puste." << endl; // generacja okna bledu
                        czyWartoscPolaTextPusta = 1;
                    }
                    czyZatwierdzonoNazweItemu = 1;
                }
            }
            czyKliknieto = 1;
        }
    }
}

void klawiatura(ALLEGRO_EVENT event)
{
    if (event.type == ALLEGRO_EVENT_KEY_UP)
    {
        switch (event.keyboard.keycode)
        {
        case ALLEGRO_KEY_ESCAPE:
            czyESC = 0;
            break;
        case 67: // enter
            czyEnter = 0;
            break;
        case 84: // stralka gora
            czyStrzalkaGora = 0;
            break;
        case 83: // strzalka prawo
            czyStrzalkaPrawo = 0;
            break;
        case 85: // strzalka dol
            czyStrzalkaDol = 0;
            break;
        case 82: // strzalka lewo
            czyStrzalkaLewo = 0;
            break;
        case 23: // W
            czyW = 0;
            break;
        case 19: // S
            czyS = 0;
            break;
        case 1: // A
            czyA = 0;
            break;
        case 4: // D
            czyD = 0;
            break;
        case 75: // spacja
            czySpacja = 0;
            break;
        case 77: // Delete
            czyDelete = 0;
            break;
        case 11: // K
            czyK = 0;
            break;
        case 221: // przycisk startu
            czyWindowsStart = 0;
            break;
        case 60: // tylda
            czyTylda = 0;
            break;
        case 76: // Insert
            czyInsert = 0;
            break;
        case 17: // Q
            czyQ = 0;
            break;
        case 5: // E
            czyE = 0;
            break;
        case 18: // R
            czyR = 0;
            break;
        case 20: // T
            czyT = 0;
            break;
        case 25: // Y
            czyY = 0;
            break;
        case 21: // U
            czyU = 0;
            break;
        case 9: // I
            czyI = 0;
            break;
        case 15: // O
            czyO = 0;
            break;
        case 16: // P
            czyP = 0;
            break;
        case 6: // F
            czyF = 0;
            break;
        case 7: // G
            czyG = 0;
            break;
        case 8: // H
            czyH = 0;
            break;
        case 10: // J
            czyJ = 0;
            break;
        case 12: //L
            czyL = 0;
            break;
        case 26: //Z
            czyZ = 0;
            break;
        case 24: // X
            czyX = 0;
            break;
        case 3: // C
            czyC = 0;
            break;
        case 22: // V
            czyV = 0;
            break;
        case 2: // B
            czyB = 0;
            break;
        case 14: // N
            czyN = 0;
            break;
        case 13: // M
            czyM = 0;
            break;
        case 28: // 1
            czy1 = 0;
            break;
        case 29: // 2
            czy2 = 0;
            break;
        case 30: // 3
            czy3 = 0;
            break;
        case 31: // 4
            czy4 = 0;
            break;
        case 32: // 5
            czy5 = 0;
            break;
        case 33: // 6
            czy6 = 0;
            break;
        case 34: // 7
            czy7 = 0;
            break;
        case 35: // 8
            czy8 = 0;
            break;
        case 36: // 9
            czy9 = 0;
            break;
        case 27: // 0
            czy0 = 0;
            break;
        case 37: // num 0
            czy0 = 0;
            break;
        case 38: // num 1
            czy1 = 0;
            break;
        case 39: // num 2
            czy2 = 0;
            break;
        case 40: // num 3
            czy3 = 0;
            break;
        case 41: // num 4
            czy4 = 0;
            break;
        case 42: // num 5
            czy5 = 0;
            break;
        case 43: // num 6
            czy6 = 0;
            break;
        case 44: // num 7
            czy7 = 0;
            break;
        case 45: // num 8
            czy8 = 0;
            break;
        case 46: // num 9
            czy9 = 0;
            break;
        case 63: // Backspace
            czyBackspace = 0;
            BackspaceCykl = 5;
            break;
        case 215:
            czyShift = 0;
            break;
        case 216:
            czyShift = 0;
            break;
        case 61: // podloga / minus
            czyPodloga = 0;
            break;
        default:
            cout << event.keyboard.keycode << endl;
            break;
        }
        if (czyZmienionoPoleWyboru == 1)
        {
            czyZmienionoPoleWyboru = 0;
        }
    }

    if (event.type == ALLEGRO_EVENT_KEY_DOWN)
    {
        switch (event.keyboard.keycode)
        {
        case ALLEGRO_KEY_ESCAPE:
            czyESC = 1;
            break;
        case 67: // enter
            czyEnter = 1;
            break;
        case 84: // stralka gora
            czyStrzalkaGora = 1;
            break;
        case 83: // strzalka prawo
            czyStrzalkaPrawo = 1;
            break;
        case 85: // strzalka dol
            czyStrzalkaDol = 1;
            break;
        case 82: // strzalka lewo
            czyStrzalkaLewo = 1;
            break;
        case 23: // W
            czyW = 1;
            break;
        case 19: // S
            czyS = 1;
            break;
        case 1: // A
            czyA = 1;
            break;
        case 4: // D
            czyD = 1;
            break;
        case 75: // spacja
            czySpacja = 1;
            //wyborMenuG = "wylanczenie";
            break;
        case 77: // Delete
            //czyDelete = 1;
            wyborMenuG = "wylanczenie";
            break;
        case 79: // End
            wyborMenuG = "wylanczenie";
            break;
        case 11: // K
            czyK = 1;
            break;
        case 221: // przycisk startu
            czyWindowsStart = 1;
            break;
        case 60: // tylda
            czyTylda = 1;
            break;
        case 76: // Insert
            czyInsert = 1;
            break;
        case 17: // Q
            czyQ = 1;
            break;
        case 5: // E
            czyE = 1;
            break;
        case 18: // R
            czyR = 1;
            break;
        case 20: // T
            czyT = 1;
            break;
        case 25: // Y
            czyY = 1;
            break;
        case 21: // U
            czyU = 1;
            break;
        case 9: // I
            czyI = 1;
            break;
        case 15: // O
            czyO = 1;
            break;
        case 16: // P
            czyP = 1;
            break;
        case 6: // F
            czyF = 1;
            break;
        case 7: // G
            czyG = 1;
            break;
        case 8: // H
            czyH = 1;
            break;
        case 10: // J
            czyJ = 1;
            break;
        case 12: //L
            czyL = 1;
            break;
        case 26: //Z
            czyZ = 1;
            break;
        case 24: // X
            czyX = 1;
            break;
        case 3: // C
            czyC = 1;
            break;
        case 22: // V
            czyV = 1;
            break;
        case 2: // B
            czyB = 1;
            break;
        case 14: // N
            czyN = 1;
            break;
        case 13: // M
            czyM = 1;
            break;
        case 28: // 1
            czy1 = 1;
            break;
        case 29: // 2
            czy2 = 1;
            break;
        case 30: // 3
            czy3 = 1;
            break;
        case 31: // 4
            czy4 = 1;
            break;
        case 32: // 5
            czy5 = 1;
            break;
        case 33: // 6
            czy6 = 1;
            break;
        case 34: // 7
            czy7 = 1;
            break;
        case 35: // 8
            czy8 = 1;
            break;
        case 36: // 9
            czy9 = 1;
            break;
        case 27: // 0
            czy0 = 1;
            break;
        case 37: // num 0
            czy0 = 1;
            break;
        case 38: // num 1
            czy1 = 1;
            break;
        case 39: // num 2
            czy2 = 1;
            break;
        case 40: // num 3
            czy3 = 1;
            break;
        case 41: // num 4
            czy4 = 1;
            break;
        case 42: // num 5
            czy5 = 1;
            break;
        case 43: // num 6
            czy6 = 1;
            break;
        case 44: // num 7
            czy7 = 1;
            break;
        case 45: // num 8
            czy8 = 1;
            break;
        case 46: // num 9
            czy9 = 1;
            break;
        case 63: // Backspace
            czyBackspace = 1;
            break;
        case 215:
            czyShift = 1;
            break;
        case 216:
            czyShift = 1;
            break;
        case 61: // podloga / minus
            czyPodloga = 1;
            break;
        default:
            cout << event.keyboard.keycode << endl;
            break;
        }
    }
}

void mysz(ALLEGRO_EVENT event)
{
    if (event.type == ALLEGRO_EVENT_MOUSE_AXES) // koordynaty kursona
    {
        myszX = event.mouse.x;
        myszY = event.mouse.y;
    }
    if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) // LPM wcisniety
    {
        if (event.mouse.button & 1)
        {
            czyLewyMyszyOn = 1;
        }
    }
    if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) // LPM puszczony
    {
        if (event.mouse.button & 1)
        {
            czyLewyMyszyOn = 0;
        }
        if (czyKliknieto == 1)
        {
            czyKliknieto = 0;
        }
    }
}

void rysujGrafPrzycTKat(ALLEGRO_DISPLAY* okno, ALLEGRO_BITMAP* oko, ALLEGRO_BITMAP* olowek, ALLEGRO_BITMAP* kosz, ALLEGRO_COLOR czerwony, int i)
{
    al_draw_bitmap(oko, polowaObrazX + 73, polowaObrazY - 132 + (i * 52), 0);
    al_draw_bitmap(olowek, polowaObrazX + 125, polowaObrazY - 132 + (i * 52), 0);
    al_draw_bitmap(kosz, polowaObrazX + 177, polowaObrazY - 132 + (i * 52), 0);

    if (zaznaczenie == "oko")
    {
        al_draw_filled_rectangle(polowaObrazX + 73, polowaObrazY - 132 + (tKatPrzycY * 52), polowaObrazX + 123, polowaObrazY - 82 + (tKatPrzycY * 52), czerwony);
        al_draw_bitmap(oko, polowaObrazX + 73, polowaObrazY - 136 + (tKatPrzycY * 52), 0);
    }
    else if (zaznaczenie == "edycja")
    {
        al_draw_filled_rectangle(polowaObrazX + 125, polowaObrazY - 132 + (tKatPrzycY * 52), polowaObrazX + 175, polowaObrazY - 82 + (tKatPrzycY * 52), czerwony);
        al_draw_bitmap(olowek, polowaObrazX + 125, polowaObrazY - 136 + (tKatPrzycY * 52), 0);
    }
    else if (zaznaczenie == "kosz")
    {
        al_draw_filled_rectangle(polowaObrazX + 177, polowaObrazY - 132 + (tKatPrzycY * 52), polowaObrazX + 227, polowaObrazY - 82 + (tKatPrzycY * 52), czerwony);
        al_draw_bitmap(kosz, polowaObrazX + 177, polowaObrazY - 136 + (tKatPrzycY * 52), 0);
    }
}

void rysujGrafPrzycItemow(ALLEGRO_DISPLAY* okno, ALLEGRO_BITMAP* olowek, ALLEGRO_BITMAP* kosz, ALLEGRO_COLOR czerwony, int i)
{
    al_draw_bitmap(olowek, polowaObrazX + 191, polowaObrazY - 132 + (i * 52), 0);
    al_draw_bitmap(kosz, polowaObrazX + 243, polowaObrazY - 132 + (i * 52), 0);

    if (zaznaczenie == "edycja")
    {
        al_draw_filled_rectangle(polowaObrazX + 191, polowaObrazY - 132 + (tItemPrzycY * 52), polowaObrazX + 241, polowaObrazY - 82 + (tItemPrzycY * 52), czerwony);
        al_draw_bitmap(olowek, polowaObrazX + 191, polowaObrazY - 136 + (tItemPrzycY * 52), 0);
    }
    else if (zaznaczenie == "kosz")
    {
        al_draw_filled_rectangle(polowaObrazX + 243, polowaObrazY - 132 + (tItemPrzycY * 52), polowaObrazX + 293, polowaObrazY - 82 + (tItemPrzycY * 52), czerwony);
        al_draw_bitmap(kosz, polowaObrazX + 243, polowaObrazY - 136 + (tItemPrzycY * 52), 0);
    }
}

void blad(string komunikat, ALLEGRO_DISPLAY* okno, ALLEGRO_EVENT event, ALLEGRO_EVENT_QUEUE* kolejka, ALLEGRO_TIMER* zegar)
{
    bool czyBlad = 1;
    int kontrolaPetli = 0;
    wyborMenuG = "Blad";
    ALLEGRO_FONT* fontB = al_load_ttf_font("arial.ttf", 20, 0);;
    ALLEGRO_COLOR zielony;
    ALLEGRO_COLOR czarny;
    ALLEGRO_COLOR czerwony;
    ALLEGRO_COLOR bialy;
    ALLEGRO_COLOR CBrazowy;
    ALLEGRO_COLOR tloMenu;

    zielony = al_map_rgb(0, 255, 0);
    czarny = al_map_rgb(0, 0, 0);
    czerwony = al_map_rgb(255, 10, 10);
    bialy = al_map_rgb(255, 255, 255);
    CBrazowy = al_map_rgb(63, 31, 0);
    tloMenu = al_map_rgb(17, 36, 53);
    do
    {
        al_wait_for_event(kolejka, &event);
        klawiatura(event);
        mysz(event);
        al_clear_to_color(tloMenu);
        al_draw_filled_rectangle(polowaObrazX - 200, polowaObrazY - 75, polowaObrazX + 200, polowaObrazY + 75, CBrazowy);
        al_draw_text(fontB, bialy, polowaObrazX - 150, polowaObrazY - 45, 0, komunikat.c_str());
        al_draw_filled_rectangle(polowaObrazX - 25, polowaObrazY + 5, polowaObrazX + 25, polowaObrazY + 45, czerwony);
        al_draw_text(fontB, bialy, polowaObrazX - 15, polowaObrazY + 13, 0, "OK");
        // odswierzanie oobrazu - start
        if (event.type == ALLEGRO_EVENT_TIMER)
        {
            if (czyZmienionoPoleWyboru == 1 && czyEnter == 0)
            {
                czyZmienionoPoleWyboru = 0;
            }
            przyciski(okno);
            al_flip_display(); // narysowanie tego co ma zostac narysowane
        }
        // odswierzanie obrazu - stop
        if (wyborMenuG != "Blad") // prawidlowe zamkniecie okna bledu
        {
            czyBlad = 0;
        }
        kontrolaPetli++;
        if (kontrolaPetli >= 10000) // awaryjne zamkniecie petli po 10 000 wywolan
        {
            czyBlad = 0;
            wyborMenuG = "menu glowne";
            break;
        }
    } while (czyBlad != 0);
    al_destroy_font(fontB);
}

void wczytajDaneZPliku()
{
    cout << "Wczytywanie danych z pliku do tablic." << endl;
    iloscWczytanychKat = 0;
    iloscKresek = 0;
    czyKoniecWczytywania = 0;
    nrWczytywanejKat = 0;
    nrWczytywanegoItemu = 0;
    liniaPliku = "";
    buffId = "";
    if (Baza.good())
    {
        for (int i = 0; czyKoniecWczytywania != 1; i++)
        {
            cout << "- - - - - - -" << endl;
            cout << "Poz w pliku: " << Baza.tellg() << endl;
            Baza >> znak;
            cout << "Znak na pozycji: " << znak << endl;
            if (znak != '|' && znak != ':')
            {
                liniaPliku += znak;
                cout << "liniaPliku: " << liniaPliku << endl;
            }
            else if (znak == ':')
            {
                if (etapWczytywania == 0)
                {
                    cout << "liniaPliku: " << liniaPliku << endl;
                    if (liniaPliku == "koniec_pliku" || liniaPliku == "koniec_pliku:")
                    {
                        cout << "Koniec pliku." << endl;
                        czyKoniecWczytywania = 1;
                        liniaPliku = "";
                        break;
                    }
                    else
                    {
                        liniaPliku.erase(liniaPliku.size(), 1);
                        cout << "liniaPliku: " << liniaPliku << endl;
                        tKat[nrWczytywanejKat].nazwa = liniaPliku;
                        iloscWczytanychKat++;
                        etapWczytywania = 1;
                        liniaPliku = "";
                        cout << "Nazwa wczytywanej kat: " << tKat[nrWczytywanejKat].nazwa << endl;
                    }
                }
                else if (etapWczytywania == 1)
                {
                    cout << "Cos poszlo nie tak przy wczytywaniu itemow." << endl;
                }
            }
            else if (znak == '|')
            {
                if (etapWczytywania == 0)
                {
                    cout << "cos poszlo nie tak przy wczytywaniu kategorii." << endl;
                }
                else if (etapWczytywania == 1)
                {
                    cout << "iloscKresek: " << iloscKresek << endl;
                    cout << "liniaPliku: " << liniaPliku << endl;
                    if (iloscKresek == 0) // wczytane id itemu
                    {
                        buffId = liniaPliku;
                        cout << "buffId: " << buffId << endl;
                    }
                    else if (iloscKresek == 1) // wczytanie nazwy itemu
                    {
                        cout << "liniaPliku: " << liniaPliku << endl;
                        if (buffId != "" && liniaPliku == "") //pusta kat
                        {
                            cout << "Pusta kategoria" << endl;
                            iloscKresek = -1;
                            etapWczytywania = 0;
                            nrWczytywanejKat++;
                        }
                        else if (buffId != "" && liniaPliku != "")// wczytano nazwe dla itemu
                        {
                            cout << "Wczytanie id itemu" << endl;
                            nrWczytywanegoItemu = stoi(buffId);
                            cout << "nrWczytywanegoIemu: " << nrWczytywanegoItemu << endl;
                            tKat[nrWczytywanejKat].Itemy[nrWczytywanegoItemu].nazwa = liniaPliku;
                            cout << "Nazwa Itemu: " << tKat[nrWczytywanejKat].Itemy[nrWczytywanegoItemu].nazwa << endl;
                        }
                        else if (buffId == "" && liniaPliku == "") // koniec kategorii
                        {
                            cout << "Wczytywanie nastepnej kategorii." << endl;
                            iloscKresek = -1;
                            etapWczytywania = 0;
                            tKat[nrWczytywanejKat].iloscWczytanychItemow = nrWczytywanegoItemu + 1;
                            nrWczytywanejKat++;
                        }
                    }
                    else if (iloscKresek == 2) // wczytanie ilosci itemu
                    {
                        cout << "liniaPliku: " << liniaPliku << endl;
                        tKat[nrWczytywanejKat].Itemy[nrWczytywanegoItemu].ilosc = stoi(liniaPliku);
                        cout << "Ilosc Itemu: " << tKat[nrWczytywanejKat].Itemy[nrWczytywanegoItemu].ilosc << endl;
                    }
                    else if (iloscKresek == 3) // wczytanie stanu itemu
                    {
                        cout << "liniaPliku: " << liniaPliku << endl;
                        tKat[nrWczytywanejKat].Itemy[nrWczytywanegoItemu].stan = liniaPliku;
                        cout << "Stan Itemu: " << tKat[nrWczytywanejKat].Itemy[nrWczytywanegoItemu].stan << endl;
                    }
                    else if (iloscKresek == 4)
                    {
                        cout << "koncowa kreska pierwsza" << endl;
                        iloscKresek = -1;
                        cout << "Wczytywanie nastepnego itemu." << endl;
                    }
                    else if (iloscKresek == 5)
                    {
                        cout << "Koncowa kreska druga" << endl;
                        iloscKresek = -1;
                        cout << "Wczytywanie nastepnego itemu." << endl;
                    }
                    liniaPliku = "";
                    iloscKresek++;
                    cout << "iloscKresek++" << endl;
                }
            }
            if (i >= 10000) // bezpiecznik przed wieczna petla
            {
                cout << "Awaryjne zamkniecie petli - petla wczytania kategorii." << endl;
                break;
            }
        }
    }
    else
    {
        cout << "Wystapil problem z plikiem." << endl;
    }
    cout << "Koniec wczytywania." << endl;
}

void zapiszDaneDoPliku()
{
    cout << "Zapis danych z tablicy do pliku." << endl;
    if (Baza.good())
    {
        cout << "Tutaj bedzie zapis tablic do pliku." << endl;
        buforZawartosciPliku = "";
        for (int i = 0; tKat[i].nazwa != ""; i++)
        {
            cout << "- - - - - - -" << endl;
            buforZawartosciPliku += tKat[i].nazwa;
            buforZawartosciPliku += ":\n";
            for (int j = 0; tKat[i].Itemy[j].nazwa != ""; j++)
            {
                buforZawartosciPliku += to_string(j);
                buforZawartosciPliku += "|";
                buforZawartosciPliku += tKat[i].Itemy[j].nazwa;
                buforZawartosciPliku += "|";
                buforZawartosciPliku += to_string(tKat[i].Itemy[j].ilosc);
                buforZawartosciPliku += "|";
                buforZawartosciPliku += tKat[i].Itemy[j].stan;
                buforZawartosciPliku += "||\n";
                if (j + 1 >= iloscObslugiwanychItemow)
                {
                    cout << "Awaryjne zamkniecie petli - petla zapisu danych do pliku B" << endl;
                    break;
                }
            }
            buforZawartosciPliku += "|||\n";
            if (i + 1 >= iloscObslugiwanychKat)
            {
                cout << "Awaryjne zamkniecie petli - petla zapisu danych do pliku A" << endl;
                break;
            }
        }
        buforZawartosciPliku += "koniec_pliku:\n";
        cout << "buforZawartosciPliku: " << endl;
        cout << buforZawartosciPliku << endl;
        Baza.seekg(0, ios::beg);
        Baza.seekp(0, ios::beg);
        cout << "Pozycja odczytu pliku: " << Baza.tellg() << endl;
        cout << "Pozycja zapisu pliku: " << Baza.tellp() << endl;
        Baza << buforZawartosciPliku;
        Baza.flush();
    }
    else
    {
        cout << "Wystapil problem z plikiem." << endl;
    }
    cout << "Koniec zapisu." << endl;
}

void sotrujTablice()
{
    cout << "Sortowanie tab kat start" << endl;
    for (int i = 0; i <= iloscObslugiwanychKat; i++)
    {
        cout << "i: " << i << endl;
        cout << "nazwa kat: " << tKat[i].nazwa << endl;
        if (tKat[i].nazwa == "" && tKat[i + 1].nazwa != "")
        {
            cout << "nr brakujacej kat: " << i << endl;
            tKat[i].nazwa = tKat[i + 1].nazwa;
            cout << "tKat[" << i << "].nazwa: " << tKat[i].nazwa;
            tKat[i + 1].nazwa = "";
            cout << "tKat[" << i + 1 << "].nazwa: " << tKat[i + 1].nazwa;
            for (int j = 0; tKat[i + 1].Itemy[j].nazwa != ""; j++) // przesuwanie itemow do przesunietej kategorii
            {
                cout << "prznoszenie itemu: " << tKat[i + 1].Itemy[j].nazwa;
                tKat[i].Itemy[j].nazwa = tKat[i + 1].Itemy[j].nazwa;
                cout << "tKat[" << i << "].Itemy[" << j << "].nazwa: " << tKat[i].Itemy[j].nazwa << endl;
                tKat[i].Itemy[j + 1].nazwa = "";
                cout << "tKat[" << i << "].Itemy[" << j << "].nazwa: " << tKat[i].Itemy[j].nazwa << endl;
                tKat[i].Itemy[j].ilosc = tKat[i + 1].Itemy[j].ilosc;
                tKat[i].Itemy[j + 1].ilosc = 0;
                tKat[i].Itemy[j].stan = tKat[i + 1].Itemy[j].stan;
                tKat[i].Itemy[j + 1].stan = "";
                if (j + 1 >= iloscObslugiwanychItemow)
                {
                    cout << "Awaryjne zamknięcie pętli - kopiowanie itemow do przesunietej kategorii." << endl;
                    break;
                }
            }
        }
        for (int j = 0; (tKat[i].Itemy[j].nazwa != "" || tKat[i].Itemy[j + 1].nazwa != ""); j++) // sortowanie itemow w kategorii
        {
            if (tKat[i].Itemy[j].nazwa == "" && tKat[i].Itemy[j + 1].nazwa != "")
            {
                tKat[i].Itemy[j].nazwa = tKat[i].Itemy[j + 1].nazwa;
                tKat[i].Itemy[j].stan = tKat[i].Itemy[j + 1].stan;
                tKat[i].Itemy[j].ilosc = tKat[i].Itemy[j + 1].ilosc;
                tKat[i].Itemy[j + 1].nazwa = "";
                tKat[i].Itemy[j + 1].stan = "";
                tKat[i].Itemy[j + 1].ilosc = 0;
            }
            else if (tKat[i].Itemy[j].nazwa == "" && tKat[i].Itemy[j + 1].nazwa == "")
            {
                cout << "Koniec itemow do sortowania." << endl;
                break;
            }
            if (j + 1 >= iloscObslugiwanychItemow)
            {
                cout << "Awaryjne zamkniecie petli - petla sortujaca itemy" << endl;
                break;
            }
        }
        if (i + 1 >= iloscObslugiwanychKat)
        {
            cout << "Awaryjne zamknięcie pętli" << endl;
            cout << "Koniec tablicy kategorii do sortowania" << endl;
            break;
        }
    }
    cout << "Sortowanie tab kat stop" << endl;
    daneKatB();
}

int main()
{
    LogError.open("Error.txt", fstream::out);
    if (!LogError.good()) 
    {
        cout << "Nie udalo sie wczytac pliku Error.txt" << endl;
    }
    else
    {
        LogError << "" << endl;
    }
    cout << "Inicjalizacja allegro...\n";
    if (!al_init())
    {
        cout << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        cout << "al_init() fail" << endl;
        LogError << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        LogError << "al_init() fail" << endl;
        return 0;
    }
    if (!al_init_font_addon())
    {
        cout << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        cout << "al_init_font_addon()" << endl;
        LogError << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        LogError << "al_init_font_addon() fail" << endl;
        return 0;
    }
    if (!al_init_ttf_addon())
    {
        cout << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        cout << "al_init_ttf_addon() fail" << endl;
        LogError << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        LogError << "al_init_ttf_addon() fail" << endl;
        return 0;
    }
    if (!al_init_image_addon())
    {
        cout << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        cout << "al_init_image_addon() fail" << endl;
        LogError << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        LogError << "al_init_image_addon() fail" << endl;
        return 0;
    }
    if (!al_init_primitives_addon())
    {
        cout << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        cout << "al_init_primitives_addon() fail" << endl;
        LogError << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        LogError << "al_init_primitives_addon() fail" << endl;
        return 0;
    }
    if (!al_install_keyboard())
    {
        cout << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        cout << "al_install_keyboard() fail" << endl;
        LogError << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        LogError << "al_install_keyboard() fail" << endl;
        return 0;
    }
    if (!al_install_mouse())
    {
        cout << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        cout << "al_install_mouse() fail" << endl;
        LogError << "BLAD: Nie udalo sie zainicjalizowac biblioteki allegro 5." << endl;
        LogError << "al_install_mouse() fail" << endl;
        return 0;
    }

    al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);

    ALLEGRO_DISPLAY* okno = al_create_display(1200, 900);
    ALLEGRO_FONT* font = al_load_ttf_font("arial.ttf", 20, 0);;
    ALLEGRO_EVENT_QUEUE* kolejka;
    ALLEGRO_EVENT event;
    ALLEGRO_TIMER* zegar;

    if (!al_load_ttf_font("arial.ttf", 20, 0))
    {
        cout << "BLAD: Nie udalo sie zaladowac czcionki 'arial.ttf'. Sprawdz czy plik czcionki znajduje sie w opdowiednim folderze." << endl;
        cout << "al_load_ttf_font() fail" << endl;
        LogError << "BLAD: Nie udalo sie zaladowac czcionki 'arial.ttf'. Sprawdz czy plik czcionki znajduje sie w opdowiednim folderze." << endl;
        LogError << "al_install_keyboard() fail" << endl;
        return 0;
    }

    ALLEGRO_COLOR zielony;
    ALLEGRO_COLOR czarny;
    ALLEGRO_COLOR czerwony;
    ALLEGRO_COLOR bialy;
    ALLEGRO_COLOR CBrazowy;
    ALLEGRO_COLOR tloMenu;
    ALLEGRO_COLOR tloText;

    zielony = al_map_rgb(0, 255, 0);
    czarny = al_map_rgb(0, 0, 0);
    czerwony = al_map_rgb(255, 10, 10);
    bialy = al_map_rgb(255, 255, 255);
    CBrazowy = al_map_rgb(63, 31, 0);
    tloMenu = al_map_rgb(17, 36, 53);
    tloText = al_map_rgb(12, 43, 42);

    // grafiki
    ALLEGRO_BITMAP* oko = NULL;
    ALLEGRO_BITMAP* olowek = NULL;
    ALLEGRO_BITMAP* kosz = NULL;
    ALLEGRO_BITMAP* strzalkaGora = NULL;
    ALLEGRO_BITMAP* strzalkaDol = NULL;
    ALLEGRO_BITMAP* duzeDrzewo = NULL;

    oko = al_load_bitmap("grafiki/podglad_ikona.png");
    olowek = al_load_bitmap("grafiki/edytuj_ikona.png");
    kosz = al_load_bitmap("grafiki/usun_ikona.png");
    strzalkaGora = al_load_bitmap("grafiki/strzalka_gora.png");
    strzalkaDol = al_load_bitmap("grafiki/strzalka_dol.png");
    duzeDrzewo = al_load_bitmap("grafiki/duzeDrzewo.png");

    if (oko != NULL && olowek != NULL && kosz != NULL && strzalkaGora != NULL && strzalkaDol != NULL && duzeDrzewo != NULL)
    {
        czyUdaloSieZaladowacGrafiki = 1;
        cout << "Zaladowano grafiki." << endl;
    }
    else
    {
        cout << "Nie udalo sie zaladowac jednej (lub wiecej) grafik." << endl;
        LogError << "BLAD: Nie udalo sie zaladowac jednej lub wiecej grafik. Sprawdz czy pliki znajduja sie w opdowiednim folderze oraz ich nazwy i rozszerzenia." << endl;
    }

    kolejka = al_create_event_queue();
    zegar = al_create_timer(1.0 / 60);

    al_register_event_source(kolejka, al_get_keyboard_event_source());
    al_register_event_source(kolejka, al_get_timer_event_source(zegar));
    al_register_event_source(kolejka, al_get_mouse_event_source());

    obrazX = al_get_display_width(okno);
    obrazY = al_get_display_height(okno);
    polowaObrazX = obrazX / 2;
    polowaObrazY = obrazY / 2;

     //=================================================================
     // - - - - - - - - - - - Koniec inicjalizacji - - - - - - - - - - -
     //=================================================================

    cout << "Aktywacja petli programu...\n";

    al_start_timer(zegar);
    while (wyborMenuG != "wylanczenie")
    {
        al_wait_for_event(kolejka, &event);

        klawiatura(event);
        mysz(event);

        al_clear_to_color(tloMenu);

        // pętla interfejsu
        if (wyborMenuG == "menu glowne")
        {
            al_draw_text(font, zielony, polowaObrazX - 80, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY - 95, polowaObrazX + 150, polowaObrazY - 45, CBrazowy);
            al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY - 25, polowaObrazX + 150, polowaObrazY + 25, CBrazowy);
            al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY + 45, polowaObrazX + 150, polowaObrazY + 95, CBrazowy);

            if (zaznaczenie == "Baza")
            {
                al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY - 95, polowaObrazX + 150, polowaObrazY - 45, czerwony);
            }
            else if (zaznaczenie == "Informacje")
            {
                al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY - 25, polowaObrazX + 150, polowaObrazY + 25, czerwony);
            }
            else if (zaznaczenie == "Exit")
            {
                al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY + 45, polowaObrazX + 150, polowaObrazY + 95, czerwony);
            }

            al_draw_text(font, bialy, polowaObrazX - 25, polowaObrazY - 85, 0, "Baza"); // Kategorie
            al_draw_text(font, bialy, polowaObrazX - 50, polowaObrazY - 15, 0, "Informacje"); // Opcje
            al_draw_text(font, bialy, polowaObrazX - 20, polowaObrazY + 55, 0, "Exit"); // Wyjscie
        }

        if (wyborMenuG == "chyba Wyjscie")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");
            al_draw_filled_rectangle(polowaObrazX - 200, polowaObrazY - 75, polowaObrazX + 200, polowaObrazY + 75, CBrazowy);
            al_draw_text(font, bialy, polowaObrazX - 130, polowaObrazY - 55, 0, "Czy na pewno chcesz wyjsc?");
            if (zaznaczenie == "Tak")
            {
                al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY + 5, polowaObrazX - 100, polowaObrazY + 55, czerwony);
                al_draw_text(font, bialy, polowaObrazX - 140, polowaObrazY + 15, 0, "Tak");
                al_draw_rectangle(polowaObrazX + 100, polowaObrazY + 5, polowaObrazX + 150, polowaObrazY + 55, CBrazowy, 4);
                al_draw_text(font, bialy, polowaObrazX + 110, polowaObrazY + 15, 0, "Nie");
            }
            else if (zaznaczenie == "Nie")
            {
                al_draw_rectangle(polowaObrazX - 150, polowaObrazY + 5, polowaObrazX - 100, polowaObrazY + 55, CBrazowy, 4);
                al_draw_text(font, bialy, polowaObrazX - 140, polowaObrazY + 15, 0, "Tak");
                al_draw_filled_rectangle(polowaObrazX + 100, polowaObrazY + 5, polowaObrazX + 150, polowaObrazY + 55, czerwony);
                al_draw_text(font, bialy, polowaObrazX + 110, polowaObrazY + 15, 0, "Nie");
            }
        }

        if (wyborMenuG == "Informacje")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            al_draw_bitmap(duzeDrzewo, 15, 15, 0); // certyfikat duzego drzewa

            al_draw_filled_rectangle(polowaObrazX - 50, polowaObrazY + 190, polowaObrazX + 50, polowaObrazY + 230, czerwony);
            al_draw_text(font, bialy, polowaObrazX - 33, polowaObrazY + 198, 0, "Powrot");

            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY - 120, 0, "Funkcje niektorych przyciskow:");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY - 80, 0, "ESC - powrot do poprzedniego menu");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY - 50, 0, "END / DELETE - zamkniecie programu");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY - 20, 0, "~ - wywolanie funkcji dane (konsola)");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY + 10, 0, "ENTER - zatwierdz");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY + 40, 0, "strzalki - nawigacja");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY + 70, 0, "Insert - wyswietlenie zawartosci 5 pierwszych kat (konsola)");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY + 100, 0, "WinButton - minimalizacja / maksymalizacja");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY + 130, 0, "Ver.: 1.0");
            al_draw_text(font, zielony, polowaObrazX - 130, polowaObrazY + 160, 0, "Plik bazodanowy: plik_bazodanowy.txt");
        }

        if (wyborMenuG == "Blad")// to chyba jest tutaj tylko w ramach testu ale pewien nie jestem
        {
            blad("Tutaj bedzie tresc bledu.",okno, event, kolejka, zegar);
        }

        if (wyborMenuG == "Baza")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            if (czyWczytanoPlik == 0) // ladowanie pliku bazy
            {
                if (czyWczytywacPlik == 0) // proba zaladowania pliku bazy
                {
                    cout << "Proba wczytania pliku." << endl;
                    Baza.open("plik_bazodanowy.txt", ios::in | ios::out);
                    czyWczytywacPlik = 1;
                }
                if (!Baza.good()) // jesli sie nie udalo zaladowac pliku bazy
                {

                    cout << "Nie udalo sie wczytac pliku bazy: plik_bazodanowy.txt" << endl;
                    LogError << "BLAD: Nie udalo sie wczytac pliku bazy." << endl;
                    LogError << "plik_bazodanowy.txt" << endl;
                    blad("Nie udalo sie wczytac pliku bazy: plik_bazodanowy.txt.", okno, event, kolejka, zegar);
                    wyborMenuG = "menu glowne";
                    poprzednieOkno = "Baza";
                }
                czyWczytanoPlik = 1;
            }
            if (Baza.good()) // wczytywanie kategorii z pliku bazy
            {
                if (czyZaladowacKategorie == 1)
                {
                    cout << "Udalo sie wczytac plik" << endl;
                    Baza.seekg(0);
                    wczytajDaneZPliku();
                    czyZaladowacKategorie = 0;
                    czyTworzycPrzyciskiTKat = 1;
                    tKatPrzycX = 0;
                    tKatPrzycY = 0;
                }
            }
            if (czyWczytywacPlik == 1 && czyZaladowacKategorie == 0) // tabela z kategoriami
            {
                al_draw_text(font, zielony, polowaObrazX - 80, polowaObrazY - 235, 0, "Przeglad kategorii:");

                // rysowanie naglowka tabeli
                al_draw_rectangle(polowaObrazX - 230, polowaObrazY - 185, polowaObrazX + 228, polowaObrazY - 133, zielony, 2);
                al_draw_line(polowaObrazX + 72, polowaObrazY - 185, polowaObrazX + 72, polowaObrazY - 133, zielony, 2);
                al_draw_line(polowaObrazX + 124, polowaObrazY - 185, polowaObrazX + 124, polowaObrazY - 133, zielony, 2);
                al_draw_line(polowaObrazX + 176, polowaObrazY - 185, polowaObrazX + 176, polowaObrazY - 133, zielony, 2);

                al_draw_text(font, zielony, polowaObrazX - 220, polowaObrazY - 170, 0, "Nazwa kategorii:");
                al_draw_bitmap(oko, polowaObrazX + 73, polowaObrazY - 185, 0);
                al_draw_bitmap(olowek, polowaObrazX + 125, polowaObrazY - 185, 0);
                al_draw_bitmap(kosz, polowaObrazX + 177, polowaObrazY - 185, 0);

                // rysowanie pol na kategorie
                al_draw_rectangle(polowaObrazX - 230, polowaObrazY - 133, polowaObrazX + 228, polowaObrazY + 127, zielony, 2);
                al_draw_line(polowaObrazX + 72, polowaObrazY - 133, polowaObrazX + 72, polowaObrazY + 127, zielony, 2);
                al_draw_line(polowaObrazX + 124, polowaObrazY - 133, polowaObrazX + 124, polowaObrazY + 127, zielony, 2);
                al_draw_line(polowaObrazX + 176, polowaObrazY - 133, polowaObrazX + 176, polowaObrazY + 127, zielony, 2);

                al_draw_line(polowaObrazX - 230, polowaObrazY - 81, polowaObrazX + 230, polowaObrazY - 81, zielony, 2);
                al_draw_line(polowaObrazX - 230, polowaObrazY - 29, polowaObrazX + 230, polowaObrazY - 29, zielony, 2);
                al_draw_line(polowaObrazX - 230, polowaObrazY + 23, polowaObrazX + 230, polowaObrazY + 23, zielony, 2);
                al_draw_line(polowaObrazX - 230, polowaObrazY + 75, polowaObrazX + 230, polowaObrazY + 75, zielony, 2);

                // strzalki
                al_draw_rectangle(polowaObrazX - 259, polowaObrazY - 185, polowaObrazX - 230, polowaObrazY - 133, zielony, 2);
                if (zaznaczenie == "KatStronaUp")
                {
                    al_draw_filled_rectangle(polowaObrazX - 258, polowaObrazY - 184, polowaObrazX - 231, polowaObrazY - 134, czerwony);
                }
                al_draw_bitmap(strzalkaGora, polowaObrazX - 257, polowaObrazY - 184, 0);
                al_draw_rectangle(polowaObrazX - 259, polowaObrazY + 75, polowaObrazX - 230, polowaObrazY + 127, zielony, 2);
                if (zaznaczenie == "KatStronaDown")
                {
                    al_draw_filled_rectangle(polowaObrazX - 258, polowaObrazY + 76, polowaObrazX - 231, polowaObrazY + 126, czerwony);
                }
                al_draw_bitmap(strzalkaDol, polowaObrazX - 257, polowaObrazY + 76, 0);

                iloscKatNaStronie = 0;
                for (int i = 0; tKat[i + (5 * stronaTabeliKat)].nazwa != ""; i++)
                {
                    al_draw_text(font, zielony, polowaObrazX - 220, polowaObrazY - 118 + (i * 52), 0, tKat[i + (5 * stronaTabeliKat)].nazwa.c_str());
                    rysujGrafPrzycTKat(okno, oko, olowek, kosz, czerwony, i); // rysowanie grafik przyciskow dla poszczegolnych kategorii
                    if (czyTworzycPrzyciskiTKat == 1)
                    {
                        tKatPrzyc[0][i] = "oko";
                        tKatPrzyc[1][i] = "edycja";
                        tKatPrzyc[2][i] = "kosz";
                        tKatPrzycY++;
                    }
                    iloscKatNaStronie++;
                    if (iloscKatNaStronie >= 5)
                    {
                        break;
                    }
                    if (i+1>=iloscObslugiwanychKat)
                    {
                        cout << "Awaryjne zamkniecie petli." << endl;
                        czyZaladowacKategorie = 2;
                        cout << "Zablokowano wczytywanie kategorii do nastepnego zaladowania programu." << endl;
                        cout << "Wystapil blad podczas rysowania nazw kategorii w tabeli." << endl;
                        LogError << "BLAD: Przerwano nieskonczone wczytywanie nazw kategorii - okno Baza." << endl;
                        break;
                    }
                }
                czyTworzycPrzyciskiTKat = 0;

                // przycisk nowej kategorii
                al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY + 144, polowaObrazX + 10, polowaObrazY + 194, CBrazowy);
                if (zaznaczenie == "Nowa kategoria")
                {
                    al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY + 144, polowaObrazX + 10, polowaObrazY + 194, czerwony);
                }
                al_draw_text(font, bialy, polowaObrazX - 140, polowaObrazY + 156, 0, "Nowa kategoria");
            }
            // przycisk powrotu
            al_draw_filled_rectangle(polowaObrazX + 50, polowaObrazY + 144, polowaObrazX + 140, polowaObrazY + 194, CBrazowy);
            if (zaznaczenie == "Powrot")
            {
                al_draw_filled_rectangle(polowaObrazX + 50, polowaObrazY + 144, polowaObrazX + 140, polowaObrazY + 194, czerwony);
            }
            al_draw_text(font, bialy, polowaObrazX + 60, polowaObrazY + 156, 0, "Powrot");
        }
        if (wyborMenuG == "NowaKat")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            al_draw_text(font, zielony, polowaObrazX - 125, polowaObrazY - 85, 0, "Tworzenie nowej Kategorii:");
            al_draw_text(font, zielony, polowaObrazX - 175, polowaObrazY - 45, 0, "Nazwa:");

            // pole tekstowe start
            al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 55, polowaObrazX + 175, polowaObrazY - 5, tloText);
            al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 55, polowaObrazX + 175, polowaObrazY - 5, zielony, 2);
            if (zaznaczenie == "PoleText")
            {
                al_draw_filled_rectangle(polowaObrazX - 80, polowaObrazY - 60, polowaObrazX + 180, polowaObrazY, czerwony);
                al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 55, polowaObrazX + 175, polowaObrazY - 5, tloText);
                al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 55, polowaObrazX + 175, polowaObrazY - 5, zielony, 2);
            }
            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY - 45, 0, wartoscPolaText.c_str());
            // pole tekstowe stop

            al_draw_filled_rectangle(polowaObrazX - 175, polowaObrazY + 35, polowaObrazX - 100, polowaObrazY + 85, CBrazowy);
            if (zaznaczenie == "Anuluj")
            {
                al_draw_filled_rectangle(polowaObrazX - 175, polowaObrazY + 35, polowaObrazX - 100, polowaObrazY + 85, czerwony);
            }
            al_draw_text(font, zielony, polowaObrazX - 165, polowaObrazY + 45, 0, "Anuluj");

            al_draw_filled_rectangle(polowaObrazX + 65, polowaObrazY + 35, polowaObrazX + 175, polowaObrazY + 85, CBrazowy);
            if (zaznaczenie == "Zatwierdz")
            {
                al_draw_filled_rectangle(polowaObrazX + 65, polowaObrazY + 35, polowaObrazX + 175, polowaObrazY + 85, czerwony);
            }
            al_draw_text(font, zielony, polowaObrazX + 75, polowaObrazY + 45, 0, "Zatwierdz");

            if (czyZatwierdzonoNazweKat == 1 && czyWartoscPolaTextPusta == 1)
            {
                cout << "Blad: Pusta nazwa kategorii." << endl;
                blad("Nazwa kategorii jest pusta.", okno, event, kolejka, zegar);
                czyZatwierdzonoNazweKat = 0;
                wyborMenuG = "NowaKat";
            }
            else if (czyZatwierdzonoNazweKat == 1 && czyWartoscPolaTextPusta == 0)
            {
                cout << "Proba dodania kategorii do tablicy." << endl;
                if (iloscWczytanychKat >= iloscObslugiwanychKat)
                {
                    cout << "Blad: Tablica kategorii jest juz pelna." << endl;
                }
                else
                {
                    tKat[iloscWczytanychKat].nazwa = wartoscPolaText;
                    zapiszDaneDoPliku();
                }
                czyZatwierdzonoNazweKat = 0;
                wyborMenuG = "Baza";
                czyZmianaMenu = 1;
                poprzednieOkno = "NowaKat";
                czyZaladowacKategorie = 1;
            }
        }
        if (wyborMenuG == "chybaUsunKat")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            al_draw_filled_rectangle(polowaObrazX - 300, polowaObrazY - 75, polowaObrazX + 300, polowaObrazY + 75, CBrazowy);
            wartoscPolaText = "Czy na pewno chcesz usunac kategorie :";
            wartoscPolaText += tKat[tKatPrzycY].nazwa.c_str();
            wartoscPolaText += " ?";
            al_draw_text(font, bialy, polowaObrazX - 280, polowaObrazY  - 55, 0, wartoscPolaText.c_str());
            wartoscPolaText = "";
            if (zaznaczenie == "Tak")
            {
                al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY + 5, polowaObrazX - 100, polowaObrazY + 55, czerwony);
                al_draw_text(font, bialy, polowaObrazX - 140, polowaObrazY + 15, 0, "Tak");
                al_draw_rectangle(polowaObrazX + 100, polowaObrazY + 5, polowaObrazX + 150, polowaObrazY + 55, CBrazowy, 4);
                al_draw_text(font, bialy, polowaObrazX + 110, polowaObrazY + 15, 0, "Nie");
            }
            else if (zaznaczenie == "Nie")
            {
                al_draw_rectangle(polowaObrazX - 150, polowaObrazY + 5, polowaObrazX - 100, polowaObrazY + 55, CBrazowy, 4);
                al_draw_text(font, bialy, polowaObrazX - 140, polowaObrazY + 15, 0, "Tak");
                al_draw_filled_rectangle(polowaObrazX + 100, polowaObrazY + 5, polowaObrazX + 150, polowaObrazY + 55, czerwony);
                al_draw_text(font, bialy, polowaObrazX + 110, polowaObrazY + 15, 0, "Nie");
            }
            if (czyUsuwamyKat == 1)
            {
                cout << "Usuwanko Kat" << endl;

                tKat[tKatPrzycY + (5 * stronaTabeliKat)].nazwa = ""; // usuniecie kat z tablicy
                for (int i = 0; tKat[tKatPrzycY + (5 * stronaTabeliKat)].Itemy[i].nazwa != ""; i++)
                {
                    tKat[tKatPrzycY + (5 * stronaTabeliKat)].Itemy[i].nazwa = ""; // usuwanie wszystkich itemow z usunietej kategorii
                    if (i + 1 >= iloscObslugiwanychItemow)
                    {
                        cout << "Awaryjne zamkniecie petli - usuwanie itemow wraz z kategoria." << endl;
                        break;
                    }
                }
                sotrujTablice();
                zapiszDaneDoPliku();
                cout << "Usunieto kategorie wraz z itemami." << endl; // po usunieciu kat nie mozna dodac nastepnej kat
                czyUsuwamyKat = 0;
                wyborMenuG = "Baza";
                poprzednieOkno = "chybaUsunKat";
                czyZaladowacKategorie = 1;
                czyZmianaMenu = 1;
            }
        }
        if (wyborMenuG == "EdycjaKat")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            al_draw_text(font, zielony, polowaObrazX - 125, polowaObrazY - 85, 0, "Edycja nazwy Kategorii:");
            al_draw_text(font, zielony, polowaObrazX - 175, polowaObrazY - 45, 0, "Nazwa:");

            // pole tekstowe start
            al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 55, polowaObrazX + 175, polowaObrazY - 5, tloText);
            al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 55, polowaObrazX + 175, polowaObrazY - 5, zielony, 2);
            if (zaznaczenie == "PoleText")
            {
                al_draw_filled_rectangle(polowaObrazX - 80, polowaObrazY - 60, polowaObrazX + 180, polowaObrazY, czerwony);
                al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 55, polowaObrazX + 175, polowaObrazY - 5, tloText);
                al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 55, polowaObrazX + 175, polowaObrazY - 5, zielony, 2);
            }
            if (czyPrzypisacNowyTekstPola == 1)
            {
                wartoscPolaText = tKat[tKatPrzycY + (5 * stronaTabeliKat)].nazwa;
                czyPrzypisacNowyTekstPola = 0;
            }
            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY - 45, 0, wartoscPolaText.c_str());
            // pole tekstowe stop

            al_draw_filled_rectangle(polowaObrazX - 175, polowaObrazY + 35, polowaObrazX - 100, polowaObrazY + 85, CBrazowy);
            if (zaznaczenie == "Anuluj")
            {
                al_draw_filled_rectangle(polowaObrazX - 175, polowaObrazY + 35, polowaObrazX - 100, polowaObrazY + 85, czerwony);
            }
            al_draw_text(font, zielony, polowaObrazX - 165, polowaObrazY + 45, 0, "Anuluj");

            al_draw_filled_rectangle(polowaObrazX + 65, polowaObrazY + 35, polowaObrazX + 175, polowaObrazY + 85, CBrazowy);
            if (zaznaczenie == "Zatwierdz")
            {
                al_draw_filled_rectangle(polowaObrazX + 65, polowaObrazY + 35, polowaObrazX + 175, polowaObrazY + 85, czerwony);
            }
            al_draw_text(font, zielony, polowaObrazX + 75, polowaObrazY + 45, 0, "Zatwierdz");

            if (czyZatwierdzonoNazweKat == 1 && czyWartoscPolaTextPusta == 1)
            {
                cout << "Blad: Pusta nazwa kategorii." << endl;
                blad("Nazwa kategorii jest pusta.", okno, event, kolejka, zegar);
                czyZatwierdzonoNazweKat = 0;
                wyborMenuG = "NowaKat";
            }
            else if (czyZatwierdzonoNazweKat == 1 && czyWartoscPolaTextPusta == 0)
            {
                cout << "Proba dodania kategorii do tablicy." << endl;
                if (iloscWczytanychKat >= iloscObslugiwanychKat)
                {
                    cout << "Blad: Tablica kategorii jest juz pelna." << endl;
                }
                else
                {
                    tKat[tKatPrzycY + (5 * stronaTabeliKat)].nazwa = wartoscPolaText;
                    zapiszDaneDoPliku();
                    czyZatwierdzonoNazweKat = 0;
                    wyborMenuG = "Baza";
                    czyZmianaMenu = 1;
                    poprzednieOkno = "EdycjaKat";
                    czyZaladowacKategorie = 1;
                }
            }
        }
        if (wyborMenuG == "kat_X_przeglad")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            nrWybranejKat = tKatPrzycY + (5 * stronaTabeliKat);
            buforKomunikatu = "Przeglad zawartosci kategorii ";
            buforKomunikatu += tKat[nrWybranejKat].nazwa;
            buforKomunikatu += " :";
            al_draw_text(font, zielony, polowaObrazX - 180, polowaObrazY - 235, 0, buforKomunikatu.c_str());

            // rysowanie naglowka tabeli
            al_draw_rectangle(polowaObrazX - 296, polowaObrazY - 185, polowaObrazX + 294, polowaObrazY - 133, zielony, 2);
            al_draw_line(polowaObrazX + 6, polowaObrazY - 185, polowaObrazX + 6, polowaObrazY - 133, zielony, 2);
            al_draw_line(polowaObrazX + 58, polowaObrazY - 185, polowaObrazX + 58, polowaObrazY - 133, zielony, 2);
            al_draw_line(polowaObrazX + 190, polowaObrazY - 185, polowaObrazX + 190, polowaObrazY - 133, zielony, 2);
            al_draw_line(polowaObrazX + 242, polowaObrazY - 185, polowaObrazX + 242, polowaObrazY - 133, zielony, 2);

            al_draw_text(font, zielony, polowaObrazX - 286, polowaObrazY - 170, 0, "Przedmiot:");
            al_draw_text(font, zielony, polowaObrazX + 9, polowaObrazY - 170, 0, "Ilosc:");
            al_draw_text(font, zielony, polowaObrazX + 63, polowaObrazY - 170, 0, "Stan:");
            al_draw_bitmap(olowek, polowaObrazX + 191, polowaObrazY - 185, 0);
            al_draw_bitmap(kosz, polowaObrazX + 243, polowaObrazY - 185, 0);

            // rysowanie pol na itemy
            al_draw_rectangle(polowaObrazX - 296, polowaObrazY - 133, polowaObrazX + 294, polowaObrazY + 127, zielony, 2);
            al_draw_line(polowaObrazX + 6, polowaObrazY - 133, polowaObrazX + 6, polowaObrazY + 127, zielony, 2);
            al_draw_line(polowaObrazX + 58, polowaObrazY - 133, polowaObrazX + 58, polowaObrazY + 127, zielony, 2);
            al_draw_line(polowaObrazX + 190, polowaObrazY - 133, polowaObrazX + 190, polowaObrazY + 127, zielony, 2);
            al_draw_line(polowaObrazX + 242, polowaObrazY - 133, polowaObrazX + 242, polowaObrazY + 127, zielony, 2);

            al_draw_line(polowaObrazX - 296, polowaObrazY - 81, polowaObrazX + 294, polowaObrazY - 81, zielony, 2);
            al_draw_line(polowaObrazX - 296, polowaObrazY - 29, polowaObrazX + 294, polowaObrazY - 29, zielony, 2);
            al_draw_line(polowaObrazX - 296, polowaObrazY + 23, polowaObrazX + 294, polowaObrazY + 23, zielony, 2);
            al_draw_line(polowaObrazX - 296, polowaObrazY + 75, polowaObrazX + 294, polowaObrazY + 75, zielony, 2);

            // strzalki
            al_draw_rectangle(polowaObrazX - 325, polowaObrazY - 185, polowaObrazX - 296, polowaObrazY - 133, zielony, 2);
            if (zaznaczenie == "ItemStronaUp")
            {
                al_draw_filled_rectangle(polowaObrazX - 324, polowaObrazY - 184, polowaObrazX - 297, polowaObrazY - 134, czerwony);
            }
            al_draw_bitmap(strzalkaGora, polowaObrazX - 323, polowaObrazY - 184, 0);
            al_draw_rectangle(polowaObrazX - 325, polowaObrazY + 75, polowaObrazX - 296, polowaObrazY + 127, zielony, 2);
            if (zaznaczenie == "ItemStronaDown")
            {
                al_draw_filled_rectangle(polowaObrazX - 324, polowaObrazY + 76, polowaObrazX - 297, polowaObrazY + 126, czerwony);
            }
            al_draw_bitmap(strzalkaDol, polowaObrazX - 323, polowaObrazY + 76, 0);

            // rysowanie itemow w tabeli
            iloscItemowNaStronie = 0;
            for (int i = 0; tKat[nrWybranejKat].Itemy[i + (5 * stronaTabeliItemow)].nazwa != ""; i++)
            {
                al_draw_text(font, zielony, polowaObrazX - 286, polowaObrazY - 118 + (i * 52), 0, tKat[nrWybranejKat].Itemy[i + (5 * stronaTabeliItemow)].nazwa.c_str());
                buforKomunikatu = to_string(tKat[nrWybranejKat].Itemy[i + (5 * stronaTabeliItemow)].ilosc);
                al_draw_text(font, zielony, polowaObrazX + 12, polowaObrazY - 118 + (i * 52), 0, buforKomunikatu.c_str());
                al_draw_text(font, zielony, polowaObrazX + 63, polowaObrazY - 118 + (i * 52), 0, tKat[nrWybranejKat].Itemy[i + (5 * stronaTabeliItemow)].stan.c_str());
                rysujGrafPrzycItemow(okno, olowek, kosz, czerwony, i); // rysowanie grafik przyciskow dla poszczegolnych itemow
                if (czyTworzycPrzyciskiTKat == 1)
                {
                    tItemPrzyc[0][i] = "edycja";
                    tItemPrzyc[1][i] = "kosz";
                    tItemPrzycY++;
                }
                iloscItemowNaStronie++;
                if (iloscItemowNaStronie >= 5)
                {
                    break;
                }
                if (i+1 >= iloscObslugiwanychItemow)
                {
                    cout << "Awaryjne zamkniecie petli." << endl;
                    cout << "Wystapil blad podczas rysowania itemow w tabeli." << endl;
                    LogError << "BLAD: przerwano nieskonczone wczytywanie itemow do kat_X_przeglad." << endl;
                    break;
                }
            }
            czyTworzycPrzyciskiTKat = 0;

            // przycisk nowego przedmiotu
            al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY + 144, polowaObrazX + 10, polowaObrazY + 194, CBrazowy);
            if (zaznaczenie == "Nowy przedmiot")
            {
                al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY + 144, polowaObrazX + 10, polowaObrazY + 194, czerwony);
            }
            al_draw_text(font, bialy, polowaObrazX - 140, polowaObrazY + 156, 0, "Nowy przedmiot");

            // przycisk powrotu
            al_draw_filled_rectangle(polowaObrazX + 50, polowaObrazY + 144, polowaObrazX + 140, polowaObrazY + 194, CBrazowy);
            if (zaznaczenie == "Powrot")
            {
                al_draw_filled_rectangle(polowaObrazX + 50, polowaObrazY + 144, polowaObrazX + 140, polowaObrazY + 194, czerwony);
            }
            al_draw_text(font, bialy, polowaObrazX + 60, polowaObrazY + 156, 0, "Powrot");
        }
        if (wyborMenuG == "kat_X_nowyItem")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            al_draw_text(font, zielony, polowaObrazX - 120, polowaObrazY - 148, 0, "Tworzenie nowego Itemu:");

            // pola tekstowe start
            al_draw_text(font, zielony, polowaObrazX - 175, polowaObrazY - 108, 0, "Nazwa:");
            al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 118, polowaObrazX + 175, polowaObrazY - 68, tloText);// wys 50 px
            al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 118, polowaObrazX + 175, polowaObrazY - 68, zielony, 2);// wys 50 px
            if (zaznaczenie == "PoleTextNazwa")
            {
                al_draw_filled_rectangle(polowaObrazX - 80, polowaObrazY - 123, polowaObrazX + 180, polowaObrazY - 63, czerwony);// wys 60 px
                al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 118, polowaObrazX + 175, polowaObrazY - 68, tloText);// wys 50 px
                al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 118, polowaObrazX + 175, polowaObrazY - 68, zielony, 2);// wys 50 px
            }
            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY - 108, 0, wartoscPolaTextNazwa.c_str());

            al_draw_text(font, zielony, polowaObrazX - 175, polowaObrazY - 33, 0, "Ilosc:");
            al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 43, polowaObrazX + 175, polowaObrazY + 7, tloText);
            al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 43, polowaObrazX + 175, polowaObrazY + 7, zielony, 2);
            if (zaznaczenie == "PoleTextIlosc")
            {
                al_draw_filled_rectangle(polowaObrazX - 80, polowaObrazY - 48, polowaObrazX + 180, polowaObrazY + 12, czerwony);
                al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 43, polowaObrazX + 175, polowaObrazY + 7, tloText);
                al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 43, polowaObrazX + 175, polowaObrazY + 7, zielony, 2);
            }
            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY - 33, 0, wartoscPolaTextIlosc.c_str());

            al_draw_text(font, zielony, polowaObrazX - 175, polowaObrazY + 42, 0, "Stan:");
            al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY + 32, polowaObrazX + 175, polowaObrazY + 82, tloText);
            al_draw_rectangle(polowaObrazX - 75, polowaObrazY + 32, polowaObrazX + 175, polowaObrazY + 82, zielony, 2);
            if (zaznaczenie == "PoleTextStan")
            {
                al_draw_filled_rectangle(polowaObrazX - 80, polowaObrazY + 27, polowaObrazX + 180, polowaObrazY + 87, czerwony);
                al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY + 32, polowaObrazX + 175, polowaObrazY + 82, tloText);
                al_draw_rectangle(polowaObrazX - 75, polowaObrazY + 32, polowaObrazX + 175, polowaObrazY + 82, zielony, 2);
            }
            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY + 42, 0, wartoscPolaTextStan.c_str());
            // pola tekstowe stop

            al_draw_filled_rectangle(polowaObrazX - 175, polowaObrazY + 112, polowaObrazX - 100, polowaObrazY + 162, CBrazowy);
            if (zaznaczenie == "Anuluj")
            {
                al_draw_filled_rectangle(polowaObrazX - 175, polowaObrazY + 112, polowaObrazX - 100, polowaObrazY + 162, czerwony);
            }
            al_draw_text(font, zielony, polowaObrazX - 165, polowaObrazY + 122, 0, "Anuluj");

            al_draw_filled_rectangle(polowaObrazX + 50, polowaObrazY + 112, polowaObrazX + 175, polowaObrazY + 162, CBrazowy);
            if (zaznaczenie == "Zatwierdz")
            {
                al_draw_filled_rectangle(polowaObrazX + 50, polowaObrazY + 112, polowaObrazX + 175, polowaObrazY + 162, czerwony);
            }
            al_draw_text(font, zielony, polowaObrazX + 60, polowaObrazY + 122, 0, "Zatwierdz");

            if (czyZatwierdzonoNazweItemu == 1 && czyWartoscPolaTextPusta == 1)
            {
                cout << "Blad: Puste pole itemu." << endl;
                blad("Nazwa, ilosc lub stan itemu jest puste.", okno, event, kolejka, zegar);
                czyZatwierdzonoNazweItemu = 0;
                wyborMenuG = "kat_X_nowyItem";
            }
            else if (czyZatwierdzonoNazweItemu == 1 && czyWartoscPolaTextPusta == 0)
            {
                cout << "Proba dodania itemu do tablicy." << endl;
                if (iloscWczytanychItemow >= iloscObslugiwanychItemow)
                {
                    cout << "Blad: Tablica itemow jest juz pelna." << endl;
                }
                else
                {
                    for (int i = 0; tKat[nrWybranejKat].Itemy[i].nazwa != ""; i++)
                    {
                        cout << "Item" << endl;
                        iloscWczytanychItemow++;
                        if (i + 1 >= iloscObslugiwanychItemow)
                        {
                            cout << "Awaryjne zamkniecie petli - petla liczace ilosc itemow w funkcji dodawania." << endl;
                            break;
                        }
                    }
                    cout << "Ilosc itemow: " << iloscWczytanychItemow << endl;
                    tKat[nrWybranejKat].Itemy[iloscWczytanychItemow].nazwa = wartoscPolaTextNazwa;
                    tKat[nrWybranejKat].Itemy[iloscWczytanychItemow].ilosc = atoi(wartoscPolaTextIlosc.c_str());
                    tKat[nrWybranejKat].Itemy[iloscWczytanychItemow].stan = wartoscPolaTextStan;
                    zapiszDaneDoPliku();
                }
                czyZatwierdzonoNazweItemu = 0;
                wyborMenuG = "kat_X_przeglad";
                czyZmianaMenu = 1;
                poprzednieOkno = "kat_X_nowyItem";
                czyZaladowacKategorie = 1;
                iloscWczytanychItemow = 0;
            }
        }
        if (wyborMenuG == "kat_X_usunItem")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            al_draw_filled_rectangle(polowaObrazX - 300, polowaObrazY - 75, polowaObrazX + 300, polowaObrazY + 75, CBrazowy);
            wartoscPolaText = "Czy na pewno chcesz usunac przedmiot :";
            wartoscPolaText += tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].nazwa.c_str();
            wartoscPolaText += " ?";
            al_draw_text(font, bialy, polowaObrazX - 280, polowaObrazY - 55, 0, wartoscPolaText.c_str());
            wartoscPolaText = "";
            if (zaznaczenie == "Tak")
            {
                al_draw_filled_rectangle(polowaObrazX - 150, polowaObrazY - 5, polowaObrazX - 100, polowaObrazY + 45, czerwony);
                al_draw_text(font, bialy, polowaObrazX - 140, polowaObrazY + 5, 0, "Tak");
                al_draw_rectangle(polowaObrazX + 100, polowaObrazY - 5, polowaObrazX + 150, polowaObrazY + 45, CBrazowy, 4);
                al_draw_text(font, bialy, polowaObrazX + 110, polowaObrazY + 5, 0, "Nie");
            }
            else if (zaznaczenie == "Nie")
            {
                al_draw_rectangle(polowaObrazX - 150, polowaObrazY - 5, polowaObrazX - 100, polowaObrazY + 45, CBrazowy, 4);
                al_draw_text(font, bialy, polowaObrazX - 140, polowaObrazY + 5, 0, "Tak");
                al_draw_filled_rectangle(polowaObrazX + 100, polowaObrazY - 5, polowaObrazX + 150, polowaObrazY + 45, czerwony);
                al_draw_text(font, bialy, polowaObrazX + 110, polowaObrazY + 5, 0, "Nie");
            }
            if (czyUsuwamyItem == 1)
            {
                cout << "Usuwanko itemu" << endl;

                tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].nazwa = ""; // usuniecie itemu z tablicy
                tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].ilosc = 0;
                tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].stan = "";
                sotrujTablice();
                zapiszDaneDoPliku();
                cout << "Usunieto item." << endl;
                czyUsuwamyItem = 0;
                wyborMenuG = "kat_X_przeglad";
                poprzednieOkno = "kat_X_usunItem";
                czyZaladowacKategorie = 1;
                czyZmianaMenu = 1;
                wartoscPolaText = "";
            }
        }
        if (wyborMenuG == "kat_X_EdytujItem")
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, 100, 70, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 100, 0, "Powrot do menu: ESC");

            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY - 148, 0, "Edycja Itemu:");

            if (czyPrzypisacNowyTekstPola == 1)
            {
                wartoscPolaTextNazwa = tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].nazwa;
                wartoscPolaTextIlosc = to_string(tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].ilosc);
                wartoscPolaTextStan = tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].stan;
                czyPrzypisacNowyTekstPola = 0;
            }

            // pola tekstowe start
            al_draw_text(font, zielony, polowaObrazX - 175, polowaObrazY - 108, 0, "Nazwa:");
            al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 118, polowaObrazX + 175, polowaObrazY - 68, tloText);// wys 50 px
            al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 118, polowaObrazX + 175, polowaObrazY - 68, zielony, 2);// wys 50 px
            if (zaznaczenie == "PoleTextNazwa")
            {
                al_draw_filled_rectangle(polowaObrazX - 80, polowaObrazY - 123, polowaObrazX + 180, polowaObrazY - 63, czerwony);// wys 60 px
                al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 118, polowaObrazX + 175, polowaObrazY - 68, tloText);// wys 50 px
                al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 118, polowaObrazX + 175, polowaObrazY - 68, zielony, 2);// wys 50 px
            }
            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY - 108, 0, wartoscPolaTextNazwa.c_str());

            al_draw_text(font, zielony, polowaObrazX - 175, polowaObrazY - 33, 0, "Ilosc:");
            al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 43, polowaObrazX + 175, polowaObrazY + 7, tloText);
            al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 43, polowaObrazX + 175, polowaObrazY + 7, zielony, 2);
            if (zaznaczenie == "PoleTextIlosc")
            {
                al_draw_filled_rectangle(polowaObrazX - 80, polowaObrazY - 48, polowaObrazX + 180, polowaObrazY + 12, czerwony);
                al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY - 43, polowaObrazX + 175, polowaObrazY + 7, tloText);
                al_draw_rectangle(polowaObrazX - 75, polowaObrazY - 43, polowaObrazX + 175, polowaObrazY + 7, zielony, 2);
            }
            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY - 33, 0, wartoscPolaTextIlosc.c_str());

            al_draw_text(font, zielony, polowaObrazX - 175, polowaObrazY + 42, 0, "Stan:");
            al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY + 32, polowaObrazX + 175, polowaObrazY + 82, tloText);
            al_draw_rectangle(polowaObrazX - 75, polowaObrazY + 32, polowaObrazX + 175, polowaObrazY + 82, zielony, 2);
            if (zaznaczenie == "PoleTextStan")
            {
                al_draw_filled_rectangle(polowaObrazX - 80, polowaObrazY + 27, polowaObrazX + 180, polowaObrazY + 87, czerwony);
                al_draw_filled_rectangle(polowaObrazX - 75, polowaObrazY + 32, polowaObrazX + 175, polowaObrazY + 82, tloText);
                al_draw_rectangle(polowaObrazX - 75, polowaObrazY + 32, polowaObrazX + 175, polowaObrazY + 82, zielony, 2);
            }
            al_draw_text(font, zielony, polowaObrazX - 70, polowaObrazY + 42, 0, wartoscPolaTextStan.c_str());
            // pola tekstowe stop

            al_draw_filled_rectangle(polowaObrazX - 175, polowaObrazY + 112, polowaObrazX - 100, polowaObrazY + 162, CBrazowy);
            if (zaznaczenie == "Anuluj")
            {
                al_draw_filled_rectangle(polowaObrazX - 175, polowaObrazY + 112, polowaObrazX - 100, polowaObrazY + 162, czerwony);
            }
            al_draw_text(font, zielony, polowaObrazX - 165, polowaObrazY + 122, 0, "Anuluj");

            al_draw_filled_rectangle(polowaObrazX + 50, polowaObrazY + 112, polowaObrazX + 175, polowaObrazY + 162, CBrazowy);
            if (zaznaczenie == "Zatwierdz")
            {
                al_draw_filled_rectangle(polowaObrazX + 50, polowaObrazY + 112, polowaObrazX + 175, polowaObrazY + 162, czerwony);
            }
            al_draw_text(font, zielony, polowaObrazX + 60, polowaObrazY + 122, 0, "Zatwierdz");

            if (czyZatwierdzonoNazweItemu == 1 && czyWartoscPolaTextPusta == 1)
            {
                cout << "Blad: Puste pole itemu." << endl;
                blad("Nazwa, ilosc lub stan itemu jest puste.", okno, event, kolejka, zegar);
                czyZatwierdzonoNazweItemu = 0;
                wyborMenuG = "kat_X_nowyItem";
            }
            else if (czyZatwierdzonoNazweItemu == 1 && czyWartoscPolaTextPusta == 0)
            {
                cout << "Proba edycji itemu w tablicy." << endl;
                tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].nazwa = wartoscPolaTextNazwa;
                tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].ilosc = atoi(wartoscPolaTextIlosc.c_str());
                tKat[nrWybranejKat].Itemy[tItemPrzycY + (5 * stronaTabeliItemow)].stan = wartoscPolaTextStan;
                zapiszDaneDoPliku();
                czyZatwierdzonoNazweItemu = 0;
                wyborMenuG = "kat_X_przeglad";
                czyZmianaMenu = 1;
                poprzednieOkno = "kat_X_EdytujItem";
                czyZaladowacKategorie = 1;
                iloscWczytanychItemow = 0;
            }
        }
        if (wyborMenuG == "OknoTestowe") // dla dokladnej analizy dziwnych zachowan programu
        {
            al_draw_text(font, zielony, polowaObrazX - 70, 50, 0, "Baza Danych Dawida");
            al_draw_text(font, zielony, polowaObrazX - 70, 70, 0, "Onko specjalnej troski");
            al_draw_text(font, zielony, 100, 90, 0, "Emergency Exit: End / Delete");
            al_draw_text(font, zielony, 100, 110, 0, "Powrot do menu: ESC");

            /*
            Istrukcja do okna testowego:
            W - wczytanie znaku oraz wczytanie pliku testowego
            O - obecna pozycja w pliku (odczyt)
            P - pozycja ++ (odczyt)
            I - pozycja -- (odczyt)
            T - wczytanie całej zawartości pliku
            N - wczytanie całej zawartości pliku z normalnej funkcji
            M - zapis danych do pliku z normalnej funkcji
            B - wczytanie pliku bazodanowego
            */
            if (OknoTestoweZmiennaA == 1)
            {
                cout << "Poz w pliku: " << Baza.tellg() << endl;
                Baza >> znak;
                cout << "Znak w pliku: " << znak << endl;
                OknoTestoweZmiennaA = 0;
                if (OknoTestoweZmiennaB != 2)
                {
                    OknoTestoweZmiennaB = 1;// zaladowanie pliku
                }
            }
            if (OknoTestoweZmiennaB == 1)
            {
                cout << "Proba wczytania pliku." << endl;
                Baza.open("plikTestowy.txt", ios::in | ios::out);
                OknoTestoweZmiennaB = 2;
            }
            if (OknoTestoweZmiennaC == 1)
            {
                cout << "- - - - - - - -" << endl;
                cout << "Obecna pozycja w pliku to: " << Baza.tellg() << endl;
                OknoTestoweZmiennaC = 0;
            }
            if (OknoTestoweZmiennaTekst == "P")
            {
                cout << "- - - - - - - -" << endl;
                Baza.seekg(+1, ios::cur);
                cout << "Pozycja w pliku ++" << endl;
                OknoTestoweZmiennaTekst = "";
            }
            if (OknoTestoweZmiennaTekst == "I")
            {
                cout << "- - - - - - - -" << endl;
                Baza.seekg(-1, ios::cur);
                cout << "Pozycja w pliku --" << endl;
                OknoTestoweZmiennaTekst = "";
            }
            if (OknoTestoweZmiennaTekst == "T")
            {
                cout << "- - - - - - - -" << endl;
                for (int i=0;!Baza.eof();i++)
                {
                    getline(Baza, buforLiniiPliku);
                    buforZawartosciPliku += "\n";
                    buforZawartosciPliku += buforLiniiPliku;
                    if (i >= 200)
                    {
                        break;
                    }
                }
                cout << "buforZawartosciPliku: " << endl;
                cout << buforZawartosciPliku << endl;
                Baza.clear();
                OknoTestoweZmiennaTekst = "";
            }
            if (OknoTestoweZmiennaTekst == "Y")
            {
                cout << "- - - - - - - -" << endl;
                Baza.seekg(0, ios::beg);
                Baza.seekp(36, ios::beg);
                Baza << "ukulele";
                cout << "Nadpisano plik danymi ze zmiennej" << endl;
                OknoTestoweZmiennaTekst = "";
            }
            if (OknoTestoweZmiennaTekst == "N")
            {
                cout << "- - - - - - - -" << endl;
                cout << "Proba wczytania zawartosci pliku." << endl;
                wczytajDaneZPliku();
                OknoTestoweZmiennaTekst = "";
            }
            if (OknoTestoweZmiennaTekst == "M")
            {
                cout << "- - - - - - - -" << endl;
                cout << "Proba zapisu talic do pliku." << endl;
                zapiszDaneDoPliku();
                OknoTestoweZmiennaTekst = "";
            }
            if (OknoTestoweZmiennaTekst == "B")
            {
                cout << "- - - - - - - -" << endl;
                cout << "Proba wczytania pliku." << endl;
                Baza.open("plik_bazodanowy.txt", ios::in | ios::out);
                OknoTestoweZmiennaB = 2;
                OknoTestoweZmiennaTekst = "";
            }
        }

        // odswierzanie obrazu (zegar , zmianaMenu==1) - start
        if (event.type == ALLEGRO_EVENT_TIMER)
        {
            przyciski(okno); // przewiniencie
            if (czyZmianaMenu == 1) // zmiana zaznaczonego przycisku po zmnianie menu
            {
                if (wyborMenuG == "chyba Wyjscie")
                {
                    zaznaczenie = "Nie";
                }
                else if (wyborMenuG == "menu glowne" && poprzednieOkno=="chyba Wyjscie")
                {
                    zaznaczenie = "Exit";
                }
                else if (wyborMenuG == "menu glowne" && poprzednieOkno == "Baza")
                {
                    zaznaczenie = "Baza";
                }
                else if (wyborMenuG == "menu glowne" && poprzednieOkno == "Informacje")
                {
                    zaznaczenie = "Informacje";
                }
                else if (wyborMenuG == "Baza" && (poprzednieOkno == "menu glowne" || poprzednieOkno == "NowaKat" || poprzednieOkno == "chybaUsunKat"))
                {
                    zaznaczenie = "Powrot";
                }
                else if (wyborMenuG == "Baza" && (poprzednieOkno == "EdycjaKat" || poprzednieOkno == "kat_X_przeglad"))
                {
                    zaznaczenie = tKatPrzyc[tKatPrzycX][tKatPrzycY];
                }
                else if (wyborMenuG == "kat_X_przeglad" && (poprzednieOkno=="Baza" || poprzednieOkno=="kat_X_usunItem" || poprzednieOkno == "kat_X_nowyItem" || poprzednieOkno=="kat_X_EdytujItem"))
                {
                    zaznaczenie = "Powrot";
                    czyTworzycPrzyciskiTKat = 1;
                    stronaTabeliItemow = 0;
                }
                else if (wyborMenuG == "kat_X_nowyItem" && poprzednieOkno == "kat_X_przeglad")
                {
                    zaznaczenie = "Anuluj";
                }
                else if (wyborMenuG == "kat_X_usunItem" && poprzednieOkno == "kat_X_przeglad")
                {
                    zaznaczenie = "Nie";
                }
                else if (wyborMenuG == "kat_X_EdytujItem" && poprzednieOkno == "kat_X_przeglad")
                {
                    zaznaczenie = "Anuluj";
                    czyPrzypisacNowyTekstPola = 1;
                }
                else if (wyborMenuG == "NowaKat" && poprzednieOkno == "Baza")
                {
                    zaznaczenie = "Anuluj";
                }
                else if (wyborMenuG == "chybaUsunKat" && (poprzednieOkno == "Baza" || poprzednieOkno == "Informacje"))
                {
                    zaznaczenie = "Nie";
                }
                else if (wyborMenuG == "EdycjaKat" && poprzednieOkno == "Baza")
                {
                    zaznaczenie = "Anuluj";
                }
                czyZmianaMenu = 0;
            }
            al_flip_display(); // narysowanie tego co ma zostac narysowane
        }
        // odswierzanie obrazu (zegar, zmianiaMenu==1) - stop
    }

    cout << "Petla programu zakonczona.\n";

    cout << "Czyszczenie pamieci...\n";

    al_rest(0.5);

    al_destroy_display(okno);
    al_destroy_font(font);
    al_destroy_timer(zegar);
    al_destroy_event_queue(kolejka);
    al_uninstall_keyboard();
    al_uninstall_mouse();

    Baza.close();
    LogError.close();

    cout << "Koniec." << endl;

    return 0;
}