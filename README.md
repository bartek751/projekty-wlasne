# Projekty własne



## Informacje ogólne

Repozytorium te zawiera projekty, które realizowałem samemu lub grupowo ( z dopiską w ile osób ).

## Webowe:
- projektAdRespect   ( html, css, bootstrap 5, projekt strony zrealizowany w ramach zadania rekrutacyjnego dla AdRespect)
- projekt_biblioteka ( html, js, css, php + baza danych na xamppie, projekt nastawiony na ochronę danych, realizowany w grupie 3 osobowej)
- technopoliaA       ( html, js, css, php (głównie w wyszukiwarce wiki), zajmowałęm się głównie wikipedią, stroną główną oraz oprawą graficzną, realizowany w 2 osoby )

## GameDev
Konto na itch.io: https://bartek751.itch.io/

- technopoliaA       ( html, js, css, php (głównie w wyszukiwarce wiki), zajmowałęm się głównie wikipedią, stroną główną oraz oprawą graficzną, realizowany w 2 osoby )
- platformowka2      [dostęp na itch.io] ( html, js, prosta gierka platformowa nie wymagajaca serwera)
- edytorV4           ( html, js, php + prosta baza danych na xamppie, edycja map 2D w formie tablicy)
- wioska Beta        ( html, css, js, jeden z pierwszych projektów)
- wioska Delta       ( html, css, js i dodatek php, jeden z pierwszych projektów)

## C++
- BazaDanychDawida   ( C++, Visual Studio 2016, biblioteka graficzna Allegro 5)

## Projekty realizowane ze wsparciem darmowych źródeł wiedzy dostępnych w internecie.