<html>
<head>
	<title>Wioska Delta</title>
	<meta charset="utf-8">
	<style>
		body{
			margin:0px;
		}
		
		img{
			display:none;
		}
		canvas{
			background-color:#020204;//#173653
			}
	</style>
</head>
<body oncontextmenu="return false;">
	<canvas id="canvasGlowny"></canvas>
	<!--Grafiki depozytów-->
	<img src="grafiki/depozyty/kamienieMale.png" id="kamienieMale">
	<img src="grafiki/depozyty/stosKamieni.png" id="stosKamieni">
	<img src="grafiki/depozyty/stosDrewna.png" id="stosDrewna">
	
	<!--Grafiki budynków-->
	<img src="grafiki/budynki/magazynOgolnyA_trawa.png" id="magazynOgolnyA_trawa">
	
	<!--Grafiki interfejsu-->
	<img src="grafiki/interfejs/drewno.png" id="surowiecDrewno">
	<img src="grafiki/interfejs/kamien.png" id="surowiecKamien">
	<img src="grafiki/interfejs/woda.png" id="surowiecWoda">
	<img src="grafiki/interfejs/jedzenie.png" id="surowiecJedzenie">
	<img src="grafiki/interfejs/ludziki.png" id="surowiecLudziki">
	<img src="grafiki/interfejs/pasekZasobowPoczatek.png" id="pasekZasobowPoczatek">
	<img src="grafiki/interfejs/pasekZasobowZasob.png" id="pasekZasobowZasob">
	<img src="grafiki/interfejs/pasekZasobowKoniec.png" id="pasekZasobowKoniec">
	<img src="grafiki/interfejs/plecPostaci_kobieta.png" id="plecPostaci_kobieta">
	<img src="grafiki/interfejs/plecPostaci_menzczyzna.png" id="plecPostaci_menzczyzna">
	<img src="grafiki/interfejs/slotNaStroj.png" id="slotNaStroj">
	<img src="grafiki/interfejs/slotNaNarzedzie.png" id="slotNaNarzedzie">
	<img src="grafiki/interfejs/menuSrodek_krawedzLewa.png" id="menuSrodek_krawedzLewa">
	<img src="grafiki/interfejs/menuSrodek_krawedzPrawa.png" id="menuSrodek_krawedzPrawa">
	<img src="grafiki/interfejs/menuSrodek_buduj.png" id="menuSrodek_buduj">
	<img src="grafiki/interfejs/menuSrodek_wyburz.png" id="menuSrodek_wyburz">
	<img src="grafiki/interfejs/menuSrodek_menu.png" id="menuSrodek_menu">
	<img src="grafiki/interfejs/dropButton.png" id="dropButton">
	<img src="grafiki/interfejs/stopButton.png" id="stopButton">
	<img src="grafiki/interfejs/strzalkiSzare_lewo.png" id="strzalkiSzare_lewo">
	<img src="grafiki/interfejs/strzalkiSzare_prawo.png" id="strzalkiSzare_prawo">
	
	<!--Grafiki ramki mapy-->
	<img src="grafiki/ramka_mapy/MetalDol.png" id="metalDol">
	<img src="grafiki/ramka_mapy/MetalDolPrawo.png" id="metalDolPrawo">
	<img src="grafiki/ramka_mapy/MetalDolLewo.png" id="metalDolLewo">
	<img src="grafiki/ramka_mapy/MetalLewo.png" id="metalLewo">
	<img src="grafiki/ramka_mapy/MetalPrawo.png" id="metalPrawo">
	<img src="grafiki/ramka_mapy/MetalSufit.png" id="metalSufit">
	<img src="grafiki/ramka_mapy/MetalSufitLewo.png" id="metalSufitLewo">
	<img src="grafiki/ramka_mapy/MetalSufitPrawo.png" id="metalSufitPrawo">
	
	<!--Grafiki ludków-->
	<img src="grafiki/ludziki/WomenStoneAPrawo.png" id="WomenStoneAPrawo">
	<img src="grafiki/ludziki/WomenStoneALewo.png" id="WomenStoneALewo">
	
	<!--Grafiki roślin-->
	<img src="grafiki/rosliny/maleDrzewko.png" id="maleDrzewko">
	
	<!--Grafiki terenu-->
	<img src="grafiki/teren/trawa.png" id="trawa">
	<img src="grafiki/teren/woda.png" id="wodaPlytka">
	
	<img src="grafiki/teren/woda_klatki/woda50-1.png" id="wodaPlytka-1">
	<img src="grafiki/teren/woda_klatki/woda50-2.png" id="wodaPlytka-2">
	<img src="grafiki/teren/woda_klatki/woda50-3.png" id="wodaPlytka-3">
	<img src="grafiki/teren/woda_klatki/woda50-4.png" id="wodaPlytka-4">
	<img src="grafiki/teren/woda_klatki/woda50-5.png" id="wodaPlytka-5">
</body>
<script src="jQuery.js"></script>
<script>
	console.log("rozpoczęcie inicjalizacji...");
	var canvasGlowny=document.getElementById("canvasGlowny");
	canvasGlowny.width=window.innerWidth-1; // -1 gdyż obcinamy nierówności typu 0,6 i zaokrąglamy w górę
	canvasGlowny.height=window.innerHeight-1;
	c=canvasGlowny.getContext('2d');
	
	var szerMaxMapy=25; // w ilości pól mapy
	var wysMaxMapy=16;
	var szerokoscMapyWczytanej=0;
	var wysokoscMapyWczytanej=0;
	var wielkoscPola=50; // wieklosc pola mapy
	function mapaPole()
	{
		this.blok=""; // podłoże (trawa, wodaPlytka)
		this.obiekt=""; // budynek, drzewo, depozyt
		this.depozyt=new depozyt();
		this.czyZyjatko=0; // (człowiek lub zwierze) jeśli tak to 1
	}
	
	var tMapa=new Array(szerMaxMapy);
	for(var i=0; i<szerMaxMapy;i++)
	{
		tMapa[i]=new Array(wysMaxMapy);
	}
	
	for(var i=0;i<szerMaxMapy;i++)
	{
		for(var j=0;j<wysMaxMapy;j++)
		{
			tMapa[i][j]=new mapaPole();
		}
	}
	
	function zerujMape() // majonez
	{
		for(var i=0;i<szerMaxMapy;i++)
		{
			for(var j=0;j<wysMaxMapy;j++)
			{
				tMapa[i][j].blok="";
				tMapa[i][j].obiekt="";
				tMapa[i][j].depozyt.nazwa="";
				tMapa[i][j].depozyt.nazwaPola="";
				tMapa[i][j].depozyt.grafika="";
				tMapa[i][j].depozyt.iloscObecna=0;
				tMapa[i][j].depozyt.wytrzymalosc=0;
				tMapa[i][j].czyZyjatko=0;
			}
		}
	}
	
	function wczytajMapeZBazy()
	{
		if(trybDebugowania==1 && DebugowanieMapy==1){
			console.log("Wczytywanie mapy z bazy...");
		}
		zerujMape();
		$.post("WczytajMape.php",
			{
				level: obecnaMapa,
			},
			function(dane, status)
			{
				if(trybDebugowania==1 && DebugowanieMapy==1){
					console.log(dane);
					console.log("Czy udalo sie wczytac mape: "+status);
				}
				if(dane!="")
				{
					wypelnijMapeDanymi(dane);
				}
			}
		);
	}
	
	function wypelnijMapeDanymi(dane)
	{
		if(trybDebugowania==1 && DebugowanieMapy==1){
			console.log("Uzupełnianie tablicy mapy...");
		}
		var wyniki = dane.split("\n");
		var wynikiBuff;
		var k=wyniki.length;
		szerokoscMapyWczytanej=0;
		wysokoscMapyWczytanej=0;
		czyMoznaPrzesuwacX=0;
		czyMoznaPrzesuwacY=0;
		startRysowaniaMapyX=0;
		startRysowaniaMapyY=0;
		k-=2;
		wynikiBuff=wyniki[0].split(".");
		if(trybDebugowania==1 && DebugowanieMapy==1){
			console.log("wyniki: "+wyniki);
			console.log("k: "+k);
			console.log("tMapa.legth: "+tMapa.length);
		}
		for(var i=0;;i++)
		{
			if(trybDebugowania==1 && DebugowanieMapy==1){
				console.log("tMapa[i].legth: "+tMapa[i].length);
			}
			for(var j=0;;j++)
			{
				wynikiBuff=wyniki[k].split(".");
				if(trybDebugowania==1 && DebugowanieMapy==1){
					console.log("tMapa[i].legth: "+tMapa[i].length);
					console.log("- - - - - - - - - - - -");
					console.log("k : "+k);
					console.log("wyniki["+k+"]: "+wyniki[k]);
					console.log("wynikiBuff[0]: "+wynikiBuff[0]);
					console.log("wynikiBuff[1]: "+wynikiBuff[1]);
					console.log("wynikiBuff[2]: "+wynikiBuff[2]);
					console.log("wynikiBuff[3]: "+wynikiBuff[3]);
					console.log("wynikiBuff[4]: "+wynikiBuff[4]);
				}
				if(wynikiBuff[3]=="blok=")
				{
					if(trybDebugowania==1 && DebugowanieMapy==1){
						console.log("blok");
					}
					tMapa[wynikiBuff[1]][wynikiBuff[2]].blok=wynikiBuff[4];
				}
				else if(wynikiBuff[3]=="obiekt=")
				{
					if(trybDebugowania==1 && DebugowanieMapy==1){
						console.log("obiekt");
					}
					tMapa[wynikiBuff[1]][wynikiBuff[2]].obiekt=wynikiBuff[4];
					if(wynikiBuff[4]=="kamienieMale")
					{
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.nazwa="małe kamienie";
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.nazwaPola="kamienieMale";
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.grafika=kamienieMale;
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.iloscObecna=wielKamienieMale;
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.wytrzymalosc=twarKamienieMale;
					}
					else if(wynikiBuff[4]=="maleDrzewko")
					{
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.nazwa="małe drzewko";
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.nazwaPola="maleDrzewko";
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.grafika=maleDrzewko;
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.iloscObecna=wielMaleDrzewko;
						tMapa[wynikiBuff[1]][wynikiBuff[2]].depozyt.wytrzymalosc=twarMaleDrzewko;
					}
				}
				if(szerokoscMapyWczytanej<parseInt(wynikiBuff[1]))
				{
					szerokoscMapyWczytanej=parseInt(wynikiBuff[1]);
				}
				if(wysokoscMapyWczytanej<parseInt(wynikiBuff[2]))
				{
					wysokoscMapyWczytanej=parseInt(wynikiBuff[2]);
				}
				k--;
				if(k<0)
				{
					//console.log("break");
					break;
				}
			}
			if(k<0)
			{
				//console.log("break");
				break;
			}
		}
		if(trybDebugowania==1 && DebugowanieMapy==1){
			console.log("Wypelinono mape danymi");
		}
		if(szerokoscMapyWczytanej*wielkoscPola>canvasGlowny.width-(canvasGlowny.width/3))// szerokosc mapy większa niż obszar nieruchomej mapy dla osi X
		{
			czyMoznaPrzesuwacX=1;
		}
		if(wysokoscMapyWczytanej*wielkoscPola>canvasGlowny.height-(canvasGlowny.height/3))// wysokość mapy większa niż obszar nieruchomej mapy dla osi Y
		{
			czyMoznaPrzesuwacY=1;
		}
		// ustalenie środka rysowania mapy na canvasie
		szerokoscMapyWczytanej+=1;
		wysokoscMapyWczytanej+=1;
		startRysowaniaMapyX=srodekRysowaniaX-((szerokoscMapyWczytanej*50)/2);
		startRysowaniaMapyY=srodekRysowaniaY-((wysokoscMapyWczytanej*50)/2);
	}
	
	// grafiki depozytów
	var kamienieMale=document.getElementById("kamienieMale");
	var stosKamieni=document.getElementById("stosKamieni");
	var stosDrewna=document.getElementById("stosDrewna");
	
	// grafiki budynków
	var magazynOgolnyA_trawa=document.getElementById("magazynOgolnyA_trawa");
	
	// grafiki interfejsu
	var surowiecDrewno=document.getElementById("surowiecDrewno");
	var surowiecKamien=document.getElementById("surowiecKamien");
	var surowiecWoda=document.getElementById("surowiecWoda");
	var surowiecJedzenie=document.getElementById("surowiecJedzenie");
	var surowiecLudziki=document.getElementById("surowiecLudziki");
	var pasekZasobowPoczatek=document.getElementById("pasekZasobowPoczatek");
	var pasekZasobowZasob=document.getElementById("pasekZasobowZasob");
	var pasekZasobowKoniec=document.getElementById("pasekZasobowKoniec");
	var plecPostaci_kobieta=document.getElementById("plecPostaci_kobieta");
	var plecPostaci_menzczyzna=document.getElementById("plecPostaci_menzczyzna");
	var slotNaStroj=document.getElementById("slotNaStroj");
	var slotNaNarzedzie=document.getElementById("slotNaNarzedzie");
	var menuSrodek_krawedzLewa=document.getElementById("menuSrodek_krawedzLewa");
	var menuSrodek_krawedzPrawa=document.getElementById("menuSrodek_krawedzPrawa");
	var menuSrodek_buduj=document.getElementById("menuSrodek_buduj");
	var menuSrodek_wyburz=document.getElementById("menuSrodek_wyburz");
	var menuSrodek_menu=document.getElementById("menuSrodek_menu");
	var dropButton=document.getElementById("dropButton");
	var stopButton=document.getElementById("stopButton");
	var strzalkiSzare_lewo=document.getElementById("strzalkiSzare_lewo");
	var strzalkiSzare_prawo=document.getElementById("strzalkiSzare_prawo");
	
	// grafiki ramki mapy
	var tRamkaMapy=new Array(8);
	tRamkaMapy[0]=document.getElementById("metalDol");
	tRamkaMapy[1]=document.getElementById("metalDolLewo");
	tRamkaMapy[2]=document.getElementById("metalDolPrawo");
	tRamkaMapy[3]=document.getElementById("metalLewo");
	tRamkaMapy[4]=document.getElementById("metalPrawo");
	tRamkaMapy[5]=document.getElementById("metalSufit");
	tRamkaMapy[6]=document.getElementById("metalSufitLewo");
	tRamkaMapy[7]=document.getElementById("metalSufitPrawo");
	
	// grafiki ludzików
	var kobietaAPrawo=document.getElementById("WomenStoneAPrawo");
	var kobietaALewo=document.getElementById("WomenStoneALewo");
	
	// grafiki roślin
	var maleDrzewko=document.getElementById("maleDrzewko");
	
	// grafiki terenu
	var trawa=document.getElementById("trawa");
	//var wodaPlytka=document.getElementById("wodaPlytka");
	
	var animWodaKlatki=5
	var tWodaPlytka=new Array(animWodaKlatki);
	tWodaPlytka[0]=document.getElementById("wodaPlytka-1");
	tWodaPlytka[1]=document.getElementById("wodaPlytka-2");
	tWodaPlytka[2]=document.getElementById("wodaPlytka-3");
	tWodaPlytka[3]=document.getElementById("wodaPlytka-4");
	tWodaPlytka[4]=document.getElementById("wodaPlytka-5");
	
	// zmienne centrujące
	var srodekRysowaniaX=canvasGlowny.width/2;
	var srodekRysowaniaY=canvasGlowny.height/2;
	var przesuniecieMapyX=0;
	var przesuniecieMapyY=0;
	var wielkoscMapyWczytanejX=0;
	
	var wielkoscMapyWczytanejY=0;
	var pktPoczatkowyKameryX=0;
	var pktPoczatkowyKameryY=0;
	var czyMoznaPrzesuwacX=0;
	var czyMoznaPrzesuwacY=0;
	
	var predkoscPrzesuwaniaMapy=8;
	var startRysowaniaMapyX=0;
	var startRysowaniaMapyY=0;
	
	// zmienne okien i interfejsu
	c.font="14px Arial";
	c.fillStyle="white";
	var szerokoscPaskaGornego=60; // grafiki początkowa oraz kończąca pasek 30 + 30 px, reszta w funkcji rysowania interfejsu
	var iloscWidocznychZasobow=5; // podstawowe 5 zasobów
	
	var poczatekXOknaLudzika=20;
	var poczatekYOknaLudzika=srodekRysowaniaY-155;
	var szerokoscOknaLudzika=250;
	var wysokoscOknaLudzika=300;
	
	// zmienne ludków
	var limitLudkow=20;
	var poczatkowaIloscLudkow=0;
	var tLudziki=new Array(limitLudkow);
	var obecnaIloscLudkow=0;
	var lewaKrawLudzika=10;
	
	var prawaKrawLudzika=40
	
	//var punktSpawnuPoczatkowegoX=0;
	//var punktSpawnuPoczatkowegoY=0;
	
	function PoczatkoweTworzenieLudzikow()
	{
		if(trybDebugowania==1 && DebugowanieLudzikow==1){
			console.log("Generowanie początkowych ludzików...");
		}
		if(obecnaMapa==0){ // ludziki na mapie testowej
			poczatkowaIloscLudkow=2;
			console.log("Początkowa ilość ludków: "+poczatkowaIloscLudkow);
			//punktSpawnuPoczatkowegoX=1;
			//punktSpawnuPoczatkowegoY=1;
		}
		if(poczatkowaIloscLudkow>0) // tworzenie początkowych ludzików
		{
			for(var i=0; i<poczatkowaIloscLudkow;i++)
			{
				console.log("obecna ilość ludków: "+obecnaIloscLudkow);
				if(obecnaIloscLudkow<poczatkowaIloscLudkow)
				{
					if(obecnaMapa==0)
					{
						if(obecnaIloscLudkow==0)
						{
							dodajLudzika(0,0);
						}
						else if(obecnaIloscLudkow==1)
						{
							dodajLudzika(1,1);
						}
					}
				}
				else
				{
					break;
				}
			}
		}
		if(trybDebugowania==1 && DebugowanieLudzikow==1){
			console.log("Generowanie początkowych ludzików zakończone");
			console.log("============================================");
		}
	}
	
	function ludzik(id_ludzika, pozX, pozY)
	{
		this.ID=id_ludzika; // jesli ID = -1 to ludzik nie żyje
		this.x=pozX*wielkoscPola;
		this.y=pozY*wielkoscPola;
		this.celX=0;
		this.celY=0;
		this.predkosc=1.5;
		
		this.zwrot="prawo" // prawo; lewo; przod; tyl (obecnie dostepne tylko prawo i lewo)
		this.plec="kob"; // kob; men
		this.grafika=kobietaAPrawo;
		
		this.stroj="domyslny";
		this.wyposazenie="brak";
		this.najedzenie=100;
		this.napojenie=100;
		this.strojGrafika=slotNaStroj;
		this.narzedzieGrafika=slotNaNarzedzie;
		
		this.zebranyPrzedmiot="";
		this.zebranaIlosc=0;
		this.zebranaIloscMax=10;
		this.predkoscZbierania=2;
		//hp?
		
		this.zadanie="brak"; // "Przemiesza sie" ; "Zbiera"
		this.zadanieBufor="";
		
		// zmienne operacyjne
		if(trybDebugowania==1 && DebugowanieLudzikow==1){
			this.czyRaportowac=1;
			console.log("%cLudzik "+this.ID+", raportowanie aktywne.", 'background-color: grey; color: white');
		}
		else{
			this.czyRaportowac=0;
		}
		this.buforA="";
		this.buforB="";
		this.buforC="";
		this.buforD="";
		this.buforE="";
		this.czySprawdzicZwrot=0;
		
		this.poleBuforA="wolne"; // lewe
		this.poleBuforB="wolne"; // górne
		this.poleBuforC="wolne"; // prawe
		this.poleBuforD="wolne"; // dolne
		
		this.postepWydobycia=0;
		
		// notatki
		/*
		Grafika ludzika jest 50x50 pix gdzie licząc od lewej 11 pix jest puste i 15 od prawej również
		*/
		
		// rysowanie ludzika
		
		this.rysuj = function()
		{
			c.drawImage(this.grafika, this.x+startRysowaniaMapyX+przesuniecieMapyX, this.y+startRysowaniaMapyY+przesuniecieMapyY);
		}
		
		// ustalenie grafiki ludzika
		
		this.ustalGrafike = function()
		{
			if(this.stroj=="domyslny")
			{
				if(this.zwrot=="prawo")
				{
					this.grafika=kobietaAPrawo;
				}
				else if(this.zwrot=="lewo")
				{
					this.grafika=kobietaALewo;
				}
			}
		}
		
		// otrzymanie zadania
		
		this.noweZadanie = function(opisZadania)
		{
			// mechanizm rozpoznawania zadań
			// podziel na człony gdzie "_" jest dzielnikiem
			// jeśli pierwszy człon to "ruch do"
			//     	to sprawdź człon trzeci
			//		jeśli człon drugi to cyfra to sprawdź człon trzeci i przypisz koordynaty punktu
			//		jeśli człon drugi to wyraz to (budynek, obiekt - instrukcja do wytworzenia)
			
			// inne instukcje na razie nie obsługiwane
			
			this.buforA=opisZadania.split("_");
			if(this.czyRaportowac==1)
			{
				console.log("Otrzymane polecenie: "+this.buforA);
			}
			//console.log("buforA.length: "+this.buforA.length);
			if(this.buforA.length==3)
			{
				if(this.buforA[0]=="ruch do")
				{
					this.buforB=this.buforA[1];
					this.buforC=this.buforA[2];
					//console.log("buforB: "+this.buforB);
					//console.log("buforC: "+this.buforC);
					this.zadanieRuchu();
				}
			}
			else if(this.buforA.length==4)
			{
				if(this.buforA[0]=="Zbiera")
				{
					this.buforA[2]=parseInt(this.buforA[2]);
					this.buforA[3]=parseInt(this.buforA[3]);
					this.buforB=0;
					this.buforC=0;
					this.buforD=0;
					this.buforE="";
					this.zadanieBufor="Zbiera";
					if(this.buforA[1]=="kamienieMale" || this.buforA[1]=="maleDrzewko") // zbieranie depozytu 
					{
						// sprawdzenie przyległych pól czy są wolne ( nie zajęte przez inny obiekt )
						if(this.buforA[2]-1>=0)
						{
							if(tMapa[this.buforA[2]-1][this.buforA[3]].obiekt!="") // lewo
							{
								this.poleBuforA="zajete";
							}
						}
						if(this.buforA[3]-1>=0)
						{
							if(tMapa[this.buforA[2]][this.buforA[3]-1].obiekt!="") // góra
							{
								this.poleBuforB="zajete";
							}
						}
						if(this.buforA[2]+1<=szerokoscMapyWczytanej)
						{
							if(tMapa[this.buforA[2]+1][this.buforA[3]].obiekt!="") // prawo
							{
								this.poleBuforC="zajete";
							}
						}
						if(this.buforA[3]+1<=wysokoscMapyWczytanej)
						{
							if(tMapa[this.buforA[2]][this.buforA[3]+1].obiekt!="") // dół
							{
								this.poleBuforD="zajete";
							}
						}
						// sprawdzenie które wolne pole przyległe jest najbliżej i zapisz w buforB dla X oraz w buforC dla Y
						
						if(this.poleBuforA=="wolne") // lewo ; to chyba można uprościć / zautomatyzować
						{
							if(this.x>=(this.buforA[2]-1)*wielkoscPola) // jeśli ludzik na prawo lub w tym samym miejscu co punkt w osi X
							{
								this.buforB=((this.buforA[2]-1)*wielkoscPola)-this.x;
							}
							else // jeśli punkt na lewo względem ludzika
							{
								this.buforB=this.x-((this.buforA[2]-1)*wielkoscPola);
							}
							this.buforC+=this.buforB // odległość w osi X
							if(this.y>=this.buforA[3]*wielkoscPola)
							{
								this.buforB=(this.buforA[3]*wielkoscPola)-this.y;
							}
							else
							{
								this.buforB=this.y-(this.buforA[3]*wielkoscPola);
							}
							this.buforC+=this.buforB // odległość w osi X + Y
							if(this.buforD>this.buforC || this.buforE=="") // jeśli wybrany punkt docelowy ma odległość większą od obecnego lub jeśli nie wybrano jeszcze punktu docelowego
							{
								this.buforD=this.buforC;
								this.buforE="poleBuforA";
							}
							this.buforC=0;
						}
						if(this.poleBuforB=="wolne") // góra
						{
							if(this.x>=this.buforA[2]*wielkoscPola) // oś X
							{
								this.buforB=(this.buforA[2]*wielkoscPola)-this.x;
							}
							else
							{
								this.buforB=this.x-(this.buforA[2]*wielkoscPola);
							}
							this.buforC+=this.buforB 
							if(this.y>=(this.buforA[3]-1)*wielkoscPola) // oś Y
							{
								this.buforB=((this.buforA[3]-1)*wielkoscPola)-this.y;
							}
							else
							{
								this.buforB=this.y-((this.buforA[3]-1)*wielkoscPola);
							}
							this.buforC+=this.buforB
							if(this.buforD>this.buforC || this.buforE=="")
							{
								this.buforD=this.buforC;
								this.buforE="poleBuforB";
							}
							this.buforC=0;
						}
						if(this.poleBuforC=="wolne") // prawo
						{
							if(this.x>=(this.buforA[2]+1)*wielkoscPola) // oś x
							{
								this.buforB=((this.buforA[2]+1)*wielkoscPola)-this.x;
							}
							else
							{
								this.buforB=this.x-((this.buforA[2]+1)*wielkoscPola);
							}
							this.buforC+=this.buforB // oś y
							if(this.y>=this.buforA[3]*wielkoscPola)
							{
								this.buforB=(this.buforA[3]*wielkoscPola)-this.y;
							}
							else
							{
								this.buforB=this.y-(this.buforA[3]*wielkoscPola);
							}
							this.buforC+=this.buforB
							if(this.buforD>this.buforC || this.buforE=="")
							{
								this.buforD=this.buforC;
								this.buforE="poleBuforC";
							}
							this.buforC=0;
						}
						if(this.poleBuforB=="wolne") // dół
						{
							if(this.x>=this.buforA[2]*wielkoscPola) // oś X
							{
								this.buforB=(this.buforA[2]*wielkoscPola)-this.x;
							}
							else
							{
								this.buforB=this.x-(this.buforA[2]*wielkoscPola);
							}
							this.buforC+=this.buforB 
							if(this.y>=(this.buforA[3]+1)*wielkoscPola) // oś Y
							{
								this.buforB=((this.buforA[3]+1)*wielkoscPola)-this.y;
							}
							else
							{
								this.buforB=this.y-((this.buforA[3]+1)*wielkoscPola);
							}
							this.buforC+=this.buforB
							if(this.buforD>this.buforC || this.buforE=="")
							{
								this.buforD=this.buforC;
								this.buforE="poleBuforD";
							}
							this.buforC=0;
						}
						
						// ruch do tamtego pola
						this.zadanieBufor=opisZadania;
						if(this.buforE=="poleBuforA") // lewo
						{
							this.buforB=(this.buforA[2]-1)*wielkoscPola;
							this.buforC=this.buforA[3]*wielkoscPola
						}
						else if(this.buforE=="poleBuforB") // góra
						{
							this.buforB=this.buforA[2]*wielkoscPola;
							this.buforC=(this.buforA[3]-1)*wielkoscPola
						}
						else if(this.buforE=="poleBuforC") // prawo
						{
							this.buforB=(this.buforA[2]+1)*wielkoscPola;
							this.buforC=this.buforA[3]*wielkoscPola
						}
						else if(this.buforE=="poleBuforD") // dół
						{
							this.buforB=this.buforA[2]*wielkoscPola;
							this.buforC=(this.buforA[3]+1)*wielkoscPola
						}
						if(this.buforE!="")
						{
							this.zadanieRuchu();
							this.buforD=1;// jesli 1 to ludzik się przemieszcza do punktu przy zasobie
							//this.buforE="";
						}
						else if(this.buforE=="")
						{
							this.zadanie="brak";
							if(this.czyRaportowac==1)
							{
								console.log("Nie znaleziono punktu przyległego do zasobu");
							}
						}
						// reszta tego zadania w metodzie "wykonajZadanie"
					}
					else if(this.buforA[1].includes("stos")) // zbieranie stosu zasobów
					{
						this.zadanieBufor=opisZadania;
						this.buforB=this.buforA[2]*wielkoscPola;
						this.buforC=this.buforA[3]*wielkoscPola;
						this.zadanieRuchu();
						this.buforD=1;
						this.buforE="centrum";
					}
				}
			}
		}
		
		// wyznaczenie zadania ruchu
		this.zadanieRuchu = function()
		{
			// pominięte sprawdzanie liczby (if buforA[1] is a number) 
			// zatwierdzone zadanie przemieszczenia się do koordynatów rzeczywistych pola
			if(this.x!=this.celX || this.y!=this.celY) // jeśli miejsce na którym się znajduje ludzik inne od tego na którym być powinien ( niedokończony ruch poprzedni )
			{
				console.log("celX: "+ this.celX);
				console.log("celY: "+ this.celY);
				tMapa[this.celX/50][this.celY/50].czyZyjatko=0; // zwolnienie rezerwacji przy zmianie pola docelowego przed dotarciem do pola poprzedniego
			}
			this.celX=parseInt(this.buforB);
			this.celY=parseInt(this.buforC);
			this.zadanie="Przemieszcza sie";
			if(this.x%50==0 && this.y%50==0)// jeśli zajmuje jakieś pole
			{
				tMapa[this.x/50][this.y/50].czyZyjatko=0; // zwolnieie obecnego pola mapy
			}
			tMapa[this.celX/50][this.celY/50].czyZyjatko=1; // rezerwacja pola mapy
			if(this.czyRaportowac==1)
			{
				if(this.zadanie=="Przemieszcza sie")
				{
					console.log("Ludzik "+this.ID+" otrzymano nowe zadanie: "+this.zadanie+" do punktu "+this.celX+" "+this.celY);
				}
			}
			this.buforA=0;
			this.buforB=0;
			//this.buforC=0;
			this.czySprawdzicZwrot=1;
		}
		
		// wykonanie zadania
		
		this.wykonajZadanie = function()
		{
			if(this.zadanie!="brak")
			{
				// wykonywanie zadania
				if(this.zadanie=="Przemieszcza sie")
				{
					this.ruch();
					if(this.buforA==1 && this.buforB==1) // ludzik na miejscu docelowym
					{
						this.zadanie="brak";
						if(this.czyRaportowac==1)
						{
							console.log("[SZARY] ludzik "+this.ID+" zadanie wykonane");
						}
					}
				}
				else if(this.zadanie=="Zbiera")
				{
					// zbliżenie się na 1/3 odległości wgłąb zasobu po dotarciu do punktu docelowego | 50/3 = 16.6
					if(this.buforD==0)
					{
						if(this.buforE=="poleBuforA") // lewo
						{
							if(this.czySprawdzicZwrot==1)
							{
								this.zwrot="prawo";
								this.czySprawdzicZwrot=0;
							}
							if(this.x<(this.buforA[2]*wielkoscPola)+16)
							{
								this.x+=this.predkosc;
							}
							else if(this.x>(this.buforA[2]*wielkoscPola)+16) // ludzik trochę za daleko w głąb pola
							{
								this.x=(this.buforA[2]*wielkoscPola)+16;
							}
							if(this.x==(this.buforA[2]*wielkoscPola)+16)
							{
								this.buforD=1; // ludzik na pozycji do wydobycia
							}
						}
						else if(this.buforE=="poleBuforB") // góra
						{
							if(this.y<(this.buforA[3]*wielkoscPola)+16)
							{
								this.y+=this.predkosc;
							}
							else if(this.y>(this.buforA[3]*wielkoscPola)+16) // ludzik trochę za daleko w głąb pola
							{
								this.y=(this.buforA[3]*wielkoscPola)+16;
							}
							if(this.y==(this.buforA[3]*wielkoscPola)+16)
							{
								this.buforD=1;
							}
						}
						else if(this.buforE=="poleBuforC") // prawo
						{
							if(this.czySprawdzicZwrot==1)
							{
								this.zwrot="lewo";
								this.czySprawdzicZwrot=0;
							}
							if(this.x>((this.buforA[2]+1)*wielkoscPola)-16)
							{
								this.x-=this.predkosc;
							}
							else if(this.x<((this.buforA[2]+1)*wielkoscPola)-16) // ludzik trochę za daleko w głąb pola
							{
								this.x=((this.buforA[2]+1)*wielkoscPola)-16;
							}
							if(this.x==((this.buforA[2]+1)*wielkoscPola)-16)
							{
								this.buforD=1;
							}
						}
						else if(this.buforE=="poleBuforD") // dół
						{
							if(this.y>(this.buforA[3]*wielkoscPola)-16)
							{
								this.y-=this.predkosc;
							}
							else if(this.y<(this.buforA[3]*wielkoscPola)-16) // ludzik trochę za daleko w głąb pola
							{
								this.y=(this.buforA[3]*wielkoscPola)-16;
							}
							if(this.y==(this.buforA[3]*wielkoscPola)-16)
							{
								this.buforD=1;
							}
						}
						else if(this.buforE=="centrum")
						{
							this.buforD=1;
						}
						this.ustalGrafike();
					}
					else if(this.buforD==1) // przygotowanie do zbierania właściwego
					{
						if((this.buforA[1]=="kamienieMale" || this.buforA[1]=="stosKamieni") && this.zebranyPrzedmiot!="male kamienie") // ustalenie co zbiera ludzik
						{
							this.zebranyPrzedmiot="male kamienie";
							this.postepWydobycia=0;
							this.zebranaIlosc=0;
						}
						else if((this.buforA[1]=="maleDrzewko" || this.buforA[1]=="stosDrewna") && this.zebranyPrzedmiot!="drewno")
						{
							this.zebranyPrzedmiot="drewno";
							this.postepWydobycia=0;
							this.zebranaIlosc=0;
						}
						else // zbieranie właściwe
						{
							if(this.zebranaIlosc<this.zebranaIloscMax)
							{
								if(this.postepWydobycia<tMapa[this.buforA[2]][this.buforA[3]].depozyt.wytrzymalosc)
								{
									this.postepWydobycia+=this.predkoscZbierania;
									//console.log("B");
								}
								else
								{
									this.postepWydobycia=0;
									this.zebranaIlosc++;
									tMapa[this.buforA[2]][this.buforA[3]].depozyt.iloscObecna--;
									if(tMapa[this.buforA[2]][this.buforA[3]].depozyt.iloscObecna<=0)
									{
										usunDepozyt(this.buforA[2],this.buforA[3]);
										this.zadanieStop();
									}
								}
							}
							else // ilosc zebranego zasobu równa max
							{
								// jeśli istnieje magazym i jest w nim miejsce
								//		zanieś zasób do magazynu
								// else jak linia poniżej
								this.zadanieStop();
							}
						}
					}
					// gdy zebrane 10, upuszczenie na pole na którym się znajduje lub przeniesienie do składu - i kontynuowanie wydobycia
					// gdy zasób się wyczerpie lub gdy pojawi się nowe zadanie lub gdy gracz karze przestać - zaprzestać wydobycia
				}
				if(this.czyRaportowac==1)
				{
					console.log("[SZARY] ludzik "+this.ID+", zadanie: "+this.zadanie);
				}
			}
			else if(this.zadanieBufor!="")
			{
				this.buforA=this.zadanieBufor.split("_");
				console.log("Polecenie z buforu: "+this.zadanieBufor);
				if(this.buforA[0]=="Zbiera" && this.buforE!="" && this.buforD==1)
				{
					this.zadanie="Zbiera";
					this.buforD=0;
					this.buforA[2]=parseInt(this.buforA[2]);
					this.buforA[3]=parseInt(this.buforA[3]);
					this.czySprawdzicZwrot=1;
					console.log("buforE: "+this.buforE);
				}
				this.zadanieBufor="";
			}
			else
			{
				// sprawdź potrzeby, jeśli brak to "stan spoczynku"
			}
		}
		
		// zatrzymanie zadania
		this.zadanieStop = function()
		{
			if(this.czyRaportowac==1)
			{
				console.log("Otrzymano polecenie STOP");
			}
			this.zadanie="";
			this.zadanieBufor="";
			this.buforA=0;
			this.buforB=0;
			this.buforC=0;
			this.buforD=0;
			this.buforE="";
			if(this.x%50!=0 || this.y%50!=0) // jeśli po zatrzymaniu ludzik nie w centrum pola to równaj z polem
			{
				if(this.x%50>25) // ludzik bardziej po prawej stronie pola
				{
					this.buforB=this.x-this.x%50+wielkoscPola; // więc idzie na pole po prawej
				}
				else // ludzik bardziej po lewej stronie pola
				{
					this.buforB=this.x-this.x%50; // więc zostaje na polu obecnym
				}
				if(this.y%50>25)
				{
					this.buforC=this.y-this.y%50+wielkoscPola;
				}
				else
				{
					this.buforC=this.y-this.y%50;
				}
				this.noweZadanie("ruch do_"+this.buforB.toString()+"_"+this.buforC.toString());
			}
		}
		
		this.upuscZasob = function()
		{
			console.log("upuszczenie zasobu");
			// ustalenie gdzie wyrzucić zebrany zasób
			if(this.x%50>25)
			{
				this.buforB=this.x-this.x%50+wielkoscPola; 
			}
			else
			{
				this.buforB=this.x-this.x%50;
			}
			if(this.y%50>25)
			{
				this.buforC=this.y-this.y%50+wielkoscPola;
			}
			else
			{
				this.buforC=this.y-this.y%50;
			}
			if(this.zebranyPrzedmiot=="male kamienie") // utworzenie stosu z zebranego zasobu
			{
				if(tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.nazwa=="stos kamieni")
				{
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.iloscObecna+=this.zebranaIlosc;
					this.zebranaIlosc=0;
				}
				else if(tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.nazwa!="")
				{
					console.log("Nie można upuścić - pole zajęte przez inny depozyt.");
				}
				else
				{
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].obiekt="stosKamieni";
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.nazwa="stos kamieni";
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.nazwaPola="stosKamieni";
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.grafika=stosKamieni;
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.iloscObecna+=this.zebranaIlosc;
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.wytrzymalosc=twarStosKamieni;
					this.zebranaIlosc=0;
				}
			}
			else if(this.zebranyPrzedmiot=="drewno")
			{
				if(tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.nazwa=="stos drewna")
				{
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.iloscObecna+=this.zebranaIlosc;
					this.zebranaIlosc=0;
				}
				else if(tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.nazwa!="")
				{
					console.log("Nie można upuścić - pole zajęte przez inny depozyt.");
				}
				else
				{
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].obiekt="stosDrewna";
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.nazwa="stos kamieni";
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.nazwaPola="stosDrewna";
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.grafika=stosDrewna;
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.iloscObecna+=this.zebranaIlosc;
					tMapa[this.buforB/wielkoscPola][this.buforC/wielkoscPola].depozyt.wytrzymalosc=twarStosDrewna;
					this.zebranaIlosc=0;
				}
			}
		}
		
		this.odniesZasob = function()
		{
			console.log("odniesienie zasobu do magazynu");
		}
		
		// przemieszczenie ludzika
		
		this.ruch = function()
		{
			// ruch do wyznaczonego punktu
			if(this.x==this.celX)
			{
				this.buforA=1;
			}
			if(this.y==this.celY)
			{
				this.buforB=1;
			}

			if(this.x<this.celX)
			{
				if(this.x<this.celX && this.x+this.predkosc>this.celX) // jeśli po przejściu w kierunku celu ludzik znajdzie się za celem
				{
					this.x=this.celX; // wówczas wyrównaj z celem
				}
				this.x+=this.predkosc;
				if(this.czySprawdzicZwrot==1){
					this.zwrot="prawo";
					this.ustalGrafike();
					this.czySprawdzicZwrot=0;
				}
			}
			else if(this.x>this.celX)
			{
				if(this.x>this.celX && this.x-this.predkosc<this.celX)
				{
					this.x=this.celX;
				}
				this.x-=this.predkosc;
				if(this.czySprawdzicZwrot==1){
					this.zwrot="lewo";
					this.ustalGrafike();
					this.czySprawdzicZwrot=0;
				}
			}
			if(this.y<this.celY)
			{
				if(this.y<this.celY && this.y+this.predkosc>this.celY)
				{
					this.y=this.celY;
				}
				this.y+=this.predkosc;
			}
			else if(this.y>this.celY)
			{
				if(this.y>this.celY && this.y-this.predkosc<this.celY)
				{
					this.y=this.celY;
				}
				this.y-=this.predkosc;
			}
		}
	}
	
	// zmienne klawiszowe myszki
	var czyLpmDown=0;
	var czyLpmUp=1;
	var LpmDonwX=0;
	var LpmDownY=0;
	var LpmUpX=0;
	
	var LpmUpY=0;
	var czyPpmDown=0;
	var czyPpmUp=1;
	var myszX=0;
	var myszY=0;
	
	var czyMyszOknoInfo=0;
	var czyMyszWObszarzeInterfejsu=0;
	var czyKlickLpm=0;
	var czyKlickPpm=0;
	
	// zmienne klawiszowe klawiatury
	var czyW=0;
	var czyS=0;
	var czyA=0;
	var czyD=0;
	var czyStrzalkaGora=0;
	
	var czyStrzalkaDol=0;
	var czyStrzalkaLewo=0;
	var czyStrzalkaPrawo=0;
	
	// zmienne przycisków interfejsu
	var czyDropButton=0;
	var czyStopButton=0;
	
	// zmienne zasobów - tych na mapie
	function depozyt()
	{
		this.nazwa="";
		this.nazwaPola="";
		this.grafika="";
		this.iloscObecna=0;
		this.wytrzymalosc=0;
	}
	
	function usunDepozyt(depX, depY)
	{
		depX=parseInt(depX);
		depY=parseInt(depY);
		if(trybDebugowania==1)
		{
			console.log("Usuwanie zasobu z mapy na koorydnatach :"+depX+" "+depY);
		}
		tMapa[depX][depY].depozyt.nazwa="";
		tMapa[depX][depY].depozyt.grafika="";
		tMapa[depX][depY].depozyt.iloscObecna=0;
		tMapa[depX][depY].depozyt.wytrzymalosc=0;
		tMapa[depX][depY].obiekt="";
	}
	
	var wielKamienieMale=20;
	var wielMaleDrzewko=20;
	var wielStosKamieni=10; // wielkość dla stosów generowanych z obiektu mapy; dla pozostałych ilość nadawana przez poszczególne akcje je tworzące
	
	var twarKamienieMale=100;
	var twarMaleDrzewko=100;
	var twarStosKamieni=60;
	var twarStosDrewna=60;
	
	// zmienne zasobów - tych na pasku górnym
	tZasoby=new Array(5); // spis wszystkich zasobów w grze
	tZasoby[0]=new zasob("Ludzie", surowiecLudziki, 1);
	tZasoby[1]=new zasob("Woda", surowiecWoda, 1);
	tZasoby[2]=new zasob("Jedzenie", surowiecJedzenie, 1);
	tZasoby[3]=new zasob("Drewno", surowiecDrewno, 1);
	tZasoby[4]=new zasob("Kamień", surowiecKamien, 1);
	
	function zasob(nazwa, grafika, czyWidoczny)
	{
		this.nazwa=nazwa;
		this.grafika=grafika;
		this.obecnaIlosc=0;
		this.maxIlosc=0;
		this.opis="<tutaj krótki opis zasobu>";
		this.czyWidoczny=czyWidoczny; // 1 / 0
	}
	
	// zmienne budynków
	var iloscBudynkowGry=1;
	var tBudynkiGry=new Array(iloscBudynkowGry)
	tBudynkiGry[0]=new budynekGry("magazyn ogólny", magazynOgolnyA_trawa, 2, 2); // magazynOgolnyA_trawa
	
	tBudynkiGry[0].kosztDrewno=5;
	tBudynkiGry[0].kosztKamien=10;
	tBudynkiGry[0].czyDostepny=1;
	
	function budynekGry(nazwa, grafika, wielkoscX, wielkoscY)
	{
		this.nazwa=nazwa;
		this.grafika=grafika;
		this.wielkoscX=wielkoscX;
		this.wielkoscY=wielkoscY;
		this.kosztWoda=0;
		this.kosztDrewno=0;
		this.kosztKamien=0;
		this.czyDostepny=0;
	}
	
	var maxBudynkowGracza=50;
	var tBudynkiGracza=new Array(maxBudynkowGracza);
	for(var i=0;i<maxBudynkowGracza;i++)
	{
		tBudynkiGracza[i]=new budynekGracza();
	}
	
	function budynekGracza()
	{
		this.nazwa="";
		this.wytrzymalosc=0;
		this.grafika="";
		this.korX=0;
		this.korY=0;
		this.wielkoscX=0;
		this.wielkoscY=0;
		this.surANazwa="";
		this.surAIlosc=0;
		this.surBNazwa="";
		this.surBIlosc=0;
		this.prodANazwa="";
		this.prodAIlosc=0;
		this.prodBNazwa="";
		this.prodBIlosc=0;
		this.pracownik=0; // ID ludzika przypisanego do pracy w danym budynku
	}
	
	// zmienne operacyjne
	var FPS=20; // 1000 ms / FPS = 50 klatek na sek.
	var obecnieZaznaczone="";
	var czyWczytacMapeZBazy=1;
	var obecnaMapa=0;
	var trybDebugowania=1;
	
	var DebugowanieMapy=0;
	var DebugowanieLudzikow=1;
	var DebugowanieMyszki=1;
	var DebugowanieKlawiatury=0;
	var czySzukacZaznaczenia=0;
	
	var czyZnalezionoLudzika=0;
	var buforIdLudzika="";
	var czySzukacZadania=0;
	var czyUstalacSzerokoscPaskaZasobow=1;
	var czyRysowacMenuPrzyciskow=0;
	
	var czyRysowacMenuBudowy=0;
	var czyTrybBudowania=0;
	var czyTrybWyburzania=0;
	var IDWybranegoBudynku=0;
	var czyMoznaZbudowac=0;
	
	var przymierzonyKoordynatX=0; // koordynaty budynku który jeszcze nie został zbudowany
	var przymierzonyKoordynatY=0;
	var cyklFPS=0;
	var buforCyklFPS=0;
	
	// zmienne animacyjne
	var animWodaKlatka=0;
	var animWodaCzasKlatki=30;
	var animWodaKlatkaBuf=0;
	var buforUniA=0; // bufory uniwersalne
	
	var buforUniB=0;
	var buforUniC=0;
	var buforUniD="";
	var buforUniE="";
	
	// funkcje techniczne
	function dane()
	{
		console.log("==================");
		console.log("tryb debugowania konsolowego (trybDebugowania 0/1): " + trybDebugowania);
		console.log("debugowanie mapy (DebugowanieMapy 0/1): " + DebugowanieMapy);
		console.log("debugowanie ludzików (DebugowanieLudzikow 0/1): " + DebugowanieLudzikow);
		console.log("debugowanie myszki (DebugowanieMyszki 0/1): " + DebugowanieMyszki);
		console.log("debugowanie klawiatury (DebugowanieKlawiatury 0/1): " + DebugowanieKlawiatury);
		console.log("Przy debugowaniu konieczne jest włączenie trybu debugowania oraz co najmniej jednej z dostępnych opcji zakresu debugowania.");
		console.log("canvas szer.: "+canvasGlowny.width);
		console.log("canvas wys.: "+canvasGlowny.height);
		console.log("srodekRysowaniaX: "+srodekRysowaniaX);
		console.log("srodekRysowaniaY: "+srodekRysowaniaY);
		console.log("startRysowaniaMapyX: "+startRysowaniaMapyX);
		console.log("startRysowaniaMapyY: "+startRysowaniaMapyY);
		console.log("funckja daneLudki() zawiera zmienne ludzików");
		console.log("funckja daneSrod() zawiera zmienne srodowiskowe");
		console.log("funckja daneOp() zawiera zmienne operacyjne");
		console.log("==================");
	}
	
	function daneLudki()
	{
		console.log("==================");
		console.log("Limit programowy ludzików (limitLudkow): "+limitLudkow);
		console.log("obecnaIloscLudkow: "+obecnaIloscLudkow);
		console.log("==================");
	}
	
	function daneSrod() // środowiskowe
	{
		console.log("==================");
		console.log("obecnie puste");
		console.log("==================");
	}
	
	function daneOp() // operacyjne
	{
		console.log("==================");
		console.log("FPS: "+FPS);
		console.log("obecnaMapa: "+obecnaMapa);
		console.log("obecnieZaznaczone: "+obecnieZaznaczone);
		console.log("(zmienne debugujące): ");
		console.log("==================");
	}
	
	// klawiatura
	window.addEventListener('keydown',function(e){
		if(trybDebugowania==1 && DebugowanieKlawiatury==1){
			console.log("[Klawiatura] keyDown, keycode: "+e.keyCode);
		}
		if(e.keyCode==68)czyD=1;
		if(e.keyCode==65)czyA=1;
		if(e.keyCode==87)czyW=1;
		if(e.keyCode==83)czyS=1;
		if(e.keyCode==37)czyStrzalkaLewo=1;
		if(e.keyCode==38)czyStrzalkaGora=1;
		if(e.keyCode==39)czyStrzalkaPrawo=1;
		if(e.keyCode==40)czyStrzalkaDol=1;
		//if(e.keyCode==69)czyE=1;
		//if(e.keyCode==75)czyK=1;
		//if(e.keyCode==32)czySpacja=1;
		/*
		87 w
		83 s
		65 a
		68 d
		69 e
		75 k
		32 spacja
		37 strzałka lewo
		38 strzałka góra
		39 strzałka prawo
		40 strzałka dół
		*/
	});
	window.addEventListener('keyup',function(e){
		if(trybDebugowania==1 && DebugowanieKlawiatury==1){
			console.log("[Klawiatura] kyeUp, keycode: "+e.keyCode);
		}
		if(e.keyCode==68)czyD=0;//ostatniRuch="prawo";
		if(e.keyCode==65)czyA=0;//ostatniRuch="lewo";
		if(e.keyCode==87)czyW=0;
		if(e.keyCode==83)czyS=0;
		if(e.keyCode==37)czyStrzalkaLewo=0;
		if(e.keyCode==38)czyStrzalkaGora=0;
		if(e.keyCode==39)czyStrzalkaPrawo=0;
		if(e.keyCode==40)czyStrzalkaDol=0;
		//if(e.keyCode==32)czySpacja=0;
		//if(e.keyCode==69)czyE=0;
		//if(e.keyCode==75)czyK=0;
		//przycisk(e.keyCode);
	});
	
	// obsluga myszki
	//var maslo;
	window.addEventListener('mousedown', function(e){
		//console.log("mouseDown e:" + e);
		//maslo=e;
		if(e.button == 0)
		{
			if(trybDebugowania==1 && DebugowanieMyszki==1){
				console.log("LPM down");
			}
			czyLpmUp=0;
			czyLpmDown=1;
			LpmDonwX=e.clientX;
			LpmDonwY=e.clientY;
		}
		if(e.button == 1)
		{
			//console.log("SPM");
		}
		if(e.button == 2)
		{
			if(trybDebugowania==1 && DebugowanieMyszki==1){
				console.log("PPM down");
			}
			czyPpmDown=1;
			czyPpmUp=0;
		}
	});
	
	window.addEventListener('mouseup', function(e){
		//console.log("mouseUp e:" + e);
		if(e.button == 0)
		{
			if(trybDebugowania==1 && DebugowanieMyszki==1){
				console.log("LPM up");
			}
			czyLpmUp=1;
			czyLpmDown=0;
		}
		if(e.button == 1)
		{
			//console.log("SPM");
		}
		if(e.button == 2)
		{
			if(trybDebugowania==1 && DebugowanieMyszki==1){
				console.log("PPM up");
			}
			czyPpmDown=0;
			czyPpmUp=1;
		}
		myszX=e.clientX;
		myszY=e.clientY;
	});
	window.addEventListener('mousemove', function(e){
		//console.log("mouse move");
		if(cyklFPS==1) // w sumie to nie wiem czy to zmniejsza ilość aktualizacji koordyantów myszki na max 50 aktualizacji / sekunde czy nie 
		{
			myszX=e.clientX;
			myszY=e.clientY;
			buforCyklFPS=0;
		}
	});
	
	// obsluga funkcji przycisków klawiatury
	//
	
	// obsluga funkcji przycisków myszki
	// LPM - zaznaczanie / odznaczenie
	// PPM - wydawanie poleceń
	function myszkaPrzyciski()
	{
		// zaznaczanie / odznaczenie
		// kiedy LPM up oraz czySzukacZaznaczenia==1 to wykonaj poniższe
		// jesli kursor wskazuje na ludzika (obszar ludzika) to go zaznacz jako obecnieZaznaczone
		// jesli nie to sprawdz pole na które wskazuje kursor
		// jesli jest obiekt to go zaznacz jako obecnieZaznaczone jesli nie to ustaw obecnieZaznaczone jako ""
		if(czyLpmDown==1)
		{
			czyKlickLpm=1;
			czySzukacZaznaczenia=0;
			czyZnalezionoLudzika=0;
			czyMyszWObszarzeInterfejsu=0;
		}
		if(czyLpmUp==1 && czyKlickLpm==1 && czySzukacZaznaczenia==0) // najpierw sprawdzamy czy mysz w obszarze interfejsu
		{
			// czy mysz w obszarze paska zasobów
			buforUniA=srodekRysowaniaX-(szerokoscPaskaGornego/2)+30;
			buforUniB=srodekRysowaniaX-(szerokoscPaskaGornego/2)+(110*iloscWidocznychZasobow)+30;
			if(myszX>=buforUniA && myszX<buforUniB && myszY>0 && myszY<=30)
			{
				czyMyszWObszarzeInterfejsu=1;
				czyKlickLpm=0;
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Mysz w obszarze górnego paska zasobów.");
					console.log("czyMyszWObszarzeInterfejsu: "+czyMyszWObszarzeInterfejsu);
				}
			}
			else
			{
				czyMyszWObszarzeInterfejsu=0;
			}
			// czy mysz w obszarze przycisków dolnych
			/*
			// przyciski dolne - szerokość: 170 px wysokość: od 30 do 50 px
		
			c.drawImage(menuSrodek_krawedzLewa, srodekRysowaniaX-85, canvasGlowny.height-30);
			c.drawImage(menuSrodek_buduj, srodekRysowaniaX-55, canvasGlowny.height-30);
			c.drawImage(menuSrodek_menu, srodekRysowaniaX-25, canvasGlowny.height-50);
			c.drawImage(menuSrodek_wyburz, srodekRysowaniaX+25, canvasGlowny.height-30);
			c.drawImage(menuSrodek_krawedzPrawa, srodekRysowaniaX+55, canvasGlowny.height-30);
			var czyRysowacMenuPrzyciskow=0;
	
			var czyRysowacMenuBudowy=0;
			var czyTrybBudowania=0;
			var czyTrybWyburzania=0;
			*/
			// czy mysz w obszarze przycisków dolnych
			
			/*
			// wielkość grafiki budynku 100 x 100 px
			// szerokość paska z budynkami na razie na 1/2 szerokości canvasa + przyciski boczne do przewijania paska z budynkami
			// szerokość prostokąta ramki: szerokosc canvasa / 2 + szerokosc strzalki * 2 = 50 px + kwadraty na strzalki 20 px + ramki kwadratów strzałek + 20 px = szerokosc canvasa / 2 + 90 px
			// wysokosc prostokata ramki: 100 px na budyki + 10 px na odstep buydnek-ramka + 10 px na ramke paska = 120 px
			
			// pasek z budynkami i strzalki
			c.fillStyle="#88919a"; // ramka okna
			c.fillRect(srodekRysowaniaX-(srodekRysowaniaX/2)-45, canvasGlowny.height-190, (canvasGlowny.width/2)+90, 120);
			c.fillStyle="#215381";
			c.fillRect(srodekRysowaniaX-(srodekRysowaniaX/2)-40, canvasGlowny.height-185, 35, 110);
			c.drawImage(strzalkiSzare_lewo, srodekRysowaniaX-(srodekRysowaniaX/2)-35, canvasGlowny.height-140);
			c.fillRect(srodekRysowaniaX-(srodekRysowaniaX/2), canvasGlowny.height-185, canvasGlowny.width/2, 110);
			c.fillRect(srodekRysowaniaX+(srodekRysowaniaX/2)+5, canvasGlowny.height-185, 35, 110);
			c.drawImage(strzalkiSzare_prawo, srodekRysowaniaX+(srodekRysowaniaX/2)+10, canvasGlowny.height-140);
			
			// rysowanie karuzeli z budynkami, nie to nie ma być karuzela spie****enia :)
			buforA=0;
			for(var i=0; i<iloscBudynkowGry; i++)
			{
				if(tBudynkiGry[i].czyDostepny==1)
				{
					c.drawImage(tBudynkiGry[i].grafika, srodekRysowaniaX-(srodekRysowaniaX/2)+(5*buforA)+(100*buforA), canvasGlowny.height-180);
				}
			}*/
			// czy mysz w obszarze otwartej karuzeli bydunków
			if(czyRysowacMenuBudowy==1 && myszX>srodekRysowaniaX-(srodekRysowaniaX/2)-45 && myszX<srodekRysowaniaX+(srodekRysowaniaX/2)+45 && myszY>canvasGlowny.height-190 && myszY<canvasGlowny.height-70)
			{
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Klik w obszarze karuzeli budynków");
				}
				if(myszX>srodekRysowaniaX-(srodekRysowaniaX/2)-40 && myszX<srodekRysowaniaX-(srodekRysowaniaX/2)-5) // przycisk strzałki w lewo
				{
					if(trybDebugowania==1 && DebugowanieMyszki==1)
					{
						console.log("Klik w obszarze strzałki w lewo"); // przesó/uwanie karuzeli nie istnieje
					}
				}
				else if(myszX>srodekRysowaniaX-(srodekRysowaniaX/2) && myszX<srodekRysowaniaX+(srodekRysowaniaX/2)) // karuzela właściwa z budynkami
				{
					if(trybDebugowania==1 && DebugowanieMyszki==1)
					{
						console.log("Klik w obszarze budynków");
					}
					// na razie koordynaty budynków na karuzeli są na sztywno z tutyłu braku jej przewijania
					if(myszX>srodekRysowaniaX-(srodekRysowaniaX/2) && myszX<srodekRysowaniaX-(srodekRysowaniaX/2)+100) // budynek pierwszy - magazyn ogólny <- do późniejszej ewentualnej poprawy
					{
						console.log("myszka na magazynie ogólnym w karuzeli.");
						obecnieZaznaczone="karuzelaBudynek_magazynOgolnyA";
						IDWybranegoBudynku=0;
					}
				}
				else if(myszX>srodekRysowaniaX+(srodekRysowaniaX/2)+5 && myszX<srodekRysowaniaX+(srodekRysowaniaX/2)+40) // przycisk strzałki w prawo
				{
					if(trybDebugowania==1 && DebugowanieMyszki==1)
					{
						console.log("Klik w obszarze strzałki w prawo"); // przesó/uwanie karuzeli nie istnieje
					}
				}
			}
			else if(myszX>srodekRysowaniaX-85 && myszX<srodekRysowaniaX+85 && myszY>canvasGlowny.height-50) // przyciski dolne interfejsu
			{
				czyMyszWObszarzeInterfejsu=1;
				czyKlickLpm=0;
				if(myszX>srodekRysowaniaX-55 && myszX<srodekRysowaniaX-25) // przycisk młotka
				{
					czyRysowacMenuBudowy=1;
					czyRysowacMenuPrzyciskow=0;
					czyTrybWyburzania=0;
				}
				else if(myszX>srodekRysowaniaX-25 && myszX<srodekRysowaniaX+25) // przycisk środkowy
				{
					czyRysowacMenuBudowy=0;
					czyRysowacMenuPrzyciskow=1;
					czyTrybWyburzania=0;
				}
				else if(myszX>srodekRysowaniaX+25 && myszX<srodekRysowaniaX+55) // przycisk burzenia
				{
					czyRysowacMenuBudowy=0;
					czyRysowacMenuPrzyciskow=0;
					czyTrybWyburzania=1;
				}
				else
				{
					czyRysowacMenuBudowy=0;
					czyRysowacMenuPrzyciskow=0;
					czyTrybWyburzania=0;
				}					
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Mysz w obszarze przycisków dolnych.");
					console.log("czyMyszWObszarzeInterfejsu: "+czyMyszWObszarzeInterfejsu);
					console.log("czyRysowacMenuBudowy: "+czyRysowacMenuBudowy);
					console.log("czyRysowacMenuPrzyciskow: "+czyRysowacMenuPrzyciskow);
					console.log("czyTrybWyburzania: "+czyTrybWyburzania);
				}
			}
			else if(!obecnieZaznaczone.includes("karuzelaBudynek"))
			{
				czyMyszWObszarzeInterfejsu=0;
				if(czyRysowacMenuBudowy==1 || czyRysowacMenuPrzyciskow==1)
				{
					czyRysowacMenuBudowy=0;
					czyRysowacMenuPrzyciskow=0;
				}
			}
			
			// mysz z zaznaczonym budynkiem z karuzeli
			if(obecnieZaznaczone.includes("karuzelaBudynek") && czyMoznaZbudowac==1) // można zbudować wybrany budynek we wskazanym miejscu
			{
				// sprawdzanie czy można zbudować we wskazanym miejscu jest w funkcji mozliwoscBudowy() nad funkcją odswierzanie()
				//if(tMapa[przymierzonyKoordynatX+(buforUniA-1)][przymierzonyKoordynatY+(buforUniB-1)].obiekt!="" && tMapa[przymierzonyKoordynatX+(buforUniA-1)][przymierzonyKoordynatY+(buforUniB-1)].blok!="woda")
				//{
					/*console.log("buforUniA: "+buforUniA);
					console.log("buforUniB-1: "+buforUniB);
					console.log("przymierzonyKoordynatX: "+przymierzonyKoordynatX);
					console.log("przymierzonyKoordynatY: "+przymierzonyKoordynatY);
					console.log("przymierzonyKoordynatX+(buforUniA-1): "+przymierzonyKoordynatX+(buforUniA-1));
					console.log("przymierzonyKoordynatY+(buforUniB-1): "+przymierzonyKoordynatY+(buforUniB-1));*/
				//}
				/*function budynekGracza(nazwa, HP, grafika, korX, korY, wielkoscX, wielkoscY, surANazwa, surAIlosc, surBNazwa, surBIlosc, prodANazwa, prodAIlosc, prodBNazwa, prodBIlosc)
				{
					this.nazwa=nazwa;
					this.wytrzymalosc=HP;
					this.korX=korX;
					this.korY=korY;
					this.wielkoscX=wielkoscX;
					this.wielkoscY=wielkoscY;
					this.surANazwa=surANazwa;
					this.surAIlosc=surAIlosc;
					this.surBNazwa=surBNazwa;
					this.surBIlosc=surBIlosc;
					this.prodANazwa=prodANazwa;
					this.prodAIlosc=prodAIlosc;
					this.prodBNazwa=prodBNazwa;
					this.prodBIlosc=prodBIlosc;
					this.pracownik=0; // ID ludzika przypisanego do pracy w danym budynku
				}
				
				function mapaPole()
				{
					this.blok=""; // podłoże (trawa, wodaPlytka)
					this.obiekt=""; // budynek, drzewo, depozyt
					this.depozyt=new depozyt();
					this.czyZyjatko=0; // (człowiek lub zwierze) jeśli tak to 1
				}
				*/
				// dodawanie budynku do tablcy budynków gracza tym samym umieszczając budynek na mapie
				// znajdź pierwsze wolne miejsce w tablicy budynków gracza
				// przypisz tam obecnie budowany budynek
				buforUniD = obecnieZaznaczone.split("_");
				for(var i=0;i<maxBudynkowGracza;i++)
				{
					if(tBudynkiGracza[i].nazwa=="")// jeśli nie ma przypisanego budynku do tego miejsca w tablicy
					{
						przypiszParametryNowegoBudynku(i, buforUniD[1], IDWybranegoBudynku);// dodaj wpis do tablicy budynków gracza
						obecnieZaznaczone="";
						czyMyszWObszarzeInterfejsu=1;// żeby nie szukało zaznaczenia po zbudowaniu budynku
						break;
					}
				}
			}
			
			// mysz w obszarze otwartego okna ludzika
			if(obecnieZaznaczone.includes("Ludzik_") && myszX>=poczatekXOknaLudzika && myszX<poczatekXOknaLudzika+szerokoscOknaLudzika+10 && myszY>=poczatekYOknaLudzika && myszY<poczatekYOknaLudzika+wysokoscOknaLudzika+10) // czy mysz w obszarze otwartego okna ludzika
			{
				czyMyszWObszarzeInterfejsu=1;
				czyKlickLpm=0;
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Mysz w obszarze otwartego okna ludzika.");
					console.log("czyMyszWObszarzeInterfejsu: "+czyMyszWObszarzeInterfejsu);
				}
				buforIdLudzika=obecnieZaznaczone.split("_");// wydobycie ID zaznaczonego ludzika
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Szukanie interakcji przycisku okna dla ludzika o ID: "+buforIdLudzika[1]);
				}
				
				// przyciski stop i drop
				if(tLudziki[buforIdLudzika[1]].zadanie!="" && tLudziki[buforIdLudzika[1]].zadanie!="brak" && myszX>=165 && myszX<190 && myszY>srodekRysowaniaY-30 && myszY<srodekRysowaniaY-5) // mysz na przycisku zatrzymania
				{
					tLudziki[buforIdLudzika[1]].zadanieStop();
				}
				else if(tLudziki[buforIdLudzika[1]].zebranaIlosc!=0 && myszX>=200 && myszX<225 && myszY>=srodekRysowaniaY-30 && myszY<srodekRysowaniaY-5)
				{
					tLudziki[buforIdLudzika[1]].upuscZasob();
				}
			}
			else
			{
				czyMyszWObszarzeInterfejsu=0;
			}
			console.log("obecnieZaznaczone: "+obecnieZaznaczone);
			if(czyMyszWObszarzeInterfejsu==0 && !obecnieZaznaczone.includes("karuzelaBudynek"))
			{
				czySzukacZaznaczenia=1;
			}
			else
			{
				czySzukacZaznaczenia=0;
				czyKlickLpm=0;
			}
		}
		if(czyLpmUp==1 && czyKlickLpm==1 && czySzukacZaznaczenia==1) // mysz nie w obszarze interfejsu więc szukamy zaznaczenia
		{
			if(trybDebugowania==1 && DebugowanieMyszki==1)
			{
				console.log("szukanie zaznaczenia...");
				if(myszX>startRysowaniaMapyX+przesuniecieMapyX)
				{
					console.log("myszX w obszarze mapy od lewej do prawej");
				}
				if(myszX<startRysowaniaMapyX+przesuniecieMapyX+(szerokoscMapyWczytanej*wielkoscPola))
				{
					console.log("myszX w obszarze mapu od prawej w lewo");
				}
				if(myszY>startRysowaniaMapyY+przesuniecieMapyY)
				{
					console.log("myszY w obszarze mapy od góry w dół");
				}
				if(myszY<startRysowaniaMapyY+przesuniecieMapyY+(wysokoscMapyWczytanej*wielkoscPola))
				{
					console.log("myszY w obszarze mapy od dołu w górę");
				}
			}
			for(var i=0; i<obecnaIloscLudkow; i++) // szukanie ludzików
			{
				if(myszX-startRysowaniaMapyX-przesuniecieMapyX>tLudziki[i].x+lewaKrawLudzika && myszX-startRysowaniaMapyX-przesuniecieMapyX<tLudziki[i].x+prawaKrawLudzika) // ludzik w linii x
				{
					if(myszY-startRysowaniaMapyY-przesuniecieMapyY>=tLudziki[i].y && myszY-startRysowaniaMapyY-przesuniecieMapyY<tLudziki[i].y+wielkoscPola) // ludzik również w linii y
					{
						obecnieZaznaczone="Ludzik_"+tLudziki[i].ID.toString();// zaznaczenie ludzika
						czyZnalezionoLudzika=1;
					}
				}
				if(i>obecnaIloscLudkow)// bezpiecznik
				{
					break;
				}
			}
			if(czyZnalezionoLudzika==0 && czyMyszWObszarzeInterfejsu==0)// szukanie zawartosci pola
			{
				if(myszX>startRysowaniaMapyX+przesuniecieMapyX && myszX<startRysowaniaMapyX+przesuniecieMapyX+(szerokoscMapyWczytanej*wielkoscPola))// jeśli kursor w obszarze mapy X
				{
					if(myszY>startRysowaniaMapyY+przesuniecieMapyY && myszY<startRysowaniaMapyY+przesuniecieMapyY+(wysokoscMapyWczytanej*wielkoscPola))// jeśli kursor w obszarze mapy Y
					{
						if(tMapa[(myszX-startRysowaniaMapyX-((myszX-startRysowaniaMapyX)%wielkoscPola))/wielkoscPola][(myszY-startRysowaniaMapyY-((myszY-startRysowaniaMapyY)%wielkoscPola))/wielkoscPola].obiekt!="")// jeśli na polu gdzie jest kursor jest jakiś obiekt
						{
							obecnieZaznaczone=tMapa[(myszX-startRysowaniaMapyX-((myszX-startRysowaniaMapyX)%wielkoscPola))/wielkoscPola][(myszY-startRysowaniaMapyY-((myszY-startRysowaniaMapyY)%wielkoscPola))/wielkoscPola].obiekt; // wówczas go zaznacz
						}
						else
						{
							obecnieZaznaczone="";
						}
					}
				}
			}
			czySzukacZaznaczenia=0;
			if(trybDebugowania==1 && DebugowanieMyszki==1)
			{
				console.log("Zaznaczenie ustawiono jako: "+obecnieZaznaczone);
			}
			czyKlickLpm=0;
		}
		if(czyPpmDown==1)
		{
			czyKlickPpm=1;
		}
		if(czyPpmUp==1 && czyKlickPpm==1)
		{
			// sprawdzenie czy mysz w obszarze interfejsu <- chciałem zrobić w oddzielnej funkcji ale nie działało w dziwny sposób więc będzie powielone :)
			buforUniA=srodekRysowaniaX-(szerokoscPaskaGornego/2)+30;
			buforUniB=srodekRysowaniaX-(szerokoscPaskaGornego/2)+(110*iloscWidocznychZasobow)+30;
			if(myszX>=buforUniA && myszX<buforUniB && myszY>0 && myszY<=30)
			{
				czyMyszWObszarzeInterfejsu=1;
				czyKlickPpm=0;
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Mysz w obszarze górnego paska zasobów.");
					console.log("czyMyszWObszarzeInterfejsu: "+czyMyszWObszarzeInterfejsu);
				}
			}
			else
			{
				czyMyszWObszarzeInterfejsu=0;
			}
			// czy mysz w obszarze przycisków dolnych
			if(myszX>srodekRysowaniaX-85 && myszX<srodekRysowaniaX+85 && myszY>canvasGlowny.height-50)
			{
				czyMyszWObszarzeInterfejsu=1;
				czyKlickPpm=0;
				/*if(myszX>srodekRysowaniaX-55 && myszX<srodekRysowaniaX-25) // przycisk młotka
				{
					//czyRysowacMenuBudowy=1;
				}
				else if(myszX>srodekRysowaniaX-25 && myszX<srodekRysowaniaX+25) // przycisk młotka
				{
					czyRysowacMenuPrzyciskow=1;
				}
				else if(myszX>srodekRysowaniaX+25 && myszX<srodekRysowaniaX+55) // przycisk młotka
				{
					czyTrybWyburzania=1;
				}*/
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Mysz w obszarze przycisków dolnych.");
					console.log("czyMyszWObszarzeInterfejsu: "+czyMyszWObszarzeInterfejsu);
					console.log("czyRysowacMenuBudowy: "+czyRysowacMenuBudowy);
					console.log("czyRysowacMenuPrzyciskow: "+czyRysowacMenuPrzyciskow);
					console.log("czyTrybWyburzania: "+czyTrybWyburzania);
				}
			}
			else
			{
				czyMyszWObszarzeInterfejsu=0;
				if(czyRysowacMenuBudowy==1 || czyRysowacMenuPrzyciskow==1 || czyTrybWyburzania==1)
				{
					czyRysowacMenuBudowy=0;
					czyRysowacMenuPrzyciskow=0;
					czyTrybWyburzania=0;
				}
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Mysz poza obszarem przycisków dolnych.");
					console.log("czyMyszWObszarzeInterfejsu: "+czyMyszWObszarzeInterfejsu);
					console.log("czyRysowacMenuBudowy: "+czyRysowacMenuBudowy);
					console.log("czyRysowacMenuPrzyciskow: "+czyRysowacMenuPrzyciskow);
					console.log("czyTrybWyburzania: "+czyTrybWyburzania);
				}
			}
			
			// mysz w obszarze otwartego okna ludzika
			if(obecnieZaznaczone.includes("Ludzik_") && myszX>=poczatekXOknaLudzika && myszX<poczatekXOknaLudzika+szerokoscOknaLudzika+10 && myszY>=poczatekYOknaLudzika && myszY<poczatekYOknaLudzika+wysokoscOknaLudzika+10) // czy mysz w obszarze otwartego okna ludzika
			{
				czyMyszWObszarzeInterfejsu=1;
				czyKlickPpm=0;
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Mysz w obszarze otwartego okna ludzika.");
					console.log("czyMyszWObszarzeInterfejsu: "+czyMyszWObszarzeInterfejsu);
				}
				/*buforIdLudzika=obecnieZaznaczone.split("_");// wydobycie ID zaznaczonego ludzika
				if(trybDebugowania==1 && DebugowanieMyszki==1)
				{
					console.log("Szukanie interakcji przycisku okna dla ludzika o ID: "+buforIdLudzika[1]);
				}
				
				// przyciski stop i drop
				if(tLudziki[buforIdLudzika[1]].zadanie!="" && myszX>=165 && myszX<190 && myszY>srodekRysowaniaY-30 && myszY<srodekRysowaniaY-5) // mysz na przycisku zatrzymania
				{
					tLudziki[buforIdLudzika[1]].zadanieStop();
				}
				else if(tLudziki[buforIdLudzika[1]].zebranaIlosc!=0 && myszX>=200 && myszX<225 && myszY>=srodekRysowaniaY-30 && myszY<srodekRysowaniaY-5)
				{
					tLudziki[buforIdLudzika[1]].upuscZasob();
				}*/
			}
			else
			{
				czyMyszWObszarzeInterfejsu=0;
			}
			if(czyMyszWObszarzeInterfejsu==0 && !obecnieZaznaczone.includes("karuzelaBudynek"))
			{
				czySzukacZadania=1;
			}
			else if(czyMyszWObszarzeInterfejsu==0 && obecnieZaznaczone.includes("karuzelaBudynek"))
			{
				obecnieZaznaczone="";
				IDWybranegoBudynku=0;
				czyMoznaZbudowac=0;
			}
			else
			{
				czyMyszOknoInfo=1;
			}
			// polecenia
			if(czySzukacZadania==1)
			{
				// jeśli obecnieZaznaczone!=""
				// jeśli obecnieZaznaczone.includes("Ludzik_") to podziel obecnieZaznaczone na dwie części gdzie dzielnik to "_"
				// weź drugą część i wyznacz zadanie ludzikowi na podstawie tego na co wskazuje kursor
				if(obecnieZaznaczone!="")
				{
					if(obecnieZaznaczone.includes("Ludzik_"))
					{
						buforIdLudzika=obecnieZaznaczone.split("_");// wydobycie ID zaznaczonego ludzika
						buforUniA=(myszX-startRysowaniaMapyX-przesuniecieMapyX-((myszX-startRysowaniaMapyX-przesuniecieMapyX)%wielkoscPola))/wielkoscPola; // wydobycie koordynatów pola mapy na podstawie koordynatów myszki
						buforUniB=(myszY-startRysowaniaMapyY-przesuniecieMapyY-((myszY-startRysowaniaMapyY-przesuniecieMapyY)%wielkoscPola))/wielkoscPola; // koordyantów startowych mapy, jej przesunięcia i wielkości pola
						if(trybDebugowania==1 && DebugowanieMyszki==1)
						{
							console.log("Szukanie zadania dla ludzika o ID: "+buforIdLudzika[1]);
							console.log("buforUniA ( tMapa X ): "+buforUniA);
							console.log("buforUniB ( tMapa Y ): "+buforUniB);
						}
						if(tMapa[buforUniA][buforUniB].obiekt!="") // zlecenie zadania zależnie od obiektu na polu mapy
						{
							if(tMapa[buforUniA][buforUniB].depozyt.nazwaPola!="") // zadanie zbierania depozytu z pola
							{
								tLudziki[parseInt(buforIdLudzika[1])].noweZadanie("Zbiera_"+tMapa[buforUniA][buforUniB].depozyt.nazwaPola+"_"+buforUniA.toString()+"_"+buforUniB.toString());
							}
						}
						else // zadanie przemieszczenia się ludzika
						{
							if(tMapa[buforUniA][buforUniB].czyZyjatko==0) // jeśli pole jest wolne
							{
								tLudziki[parseInt(buforIdLudzika[1])].noweZadanie("ruch do_"+(buforUniA*wielkoscPola).toString()+"_"+(buforUniB*wielkoscPola).toString());// ruch do koordynatów rzeczywistych
							}
							else if(trybDebugowania==1 && DebugowanieMyszki==1)
							{
								console.log("Niemożność przemieszczenia, pole już zajęte.");
							}
						}
						czySzukacZadania=0;
						if(trybDebugowania==1 && DebugowanieMyszki==1)
						{
							console.log("Zakończono szukanie zadania.");
						}
					}
				}
				czyKlickPpm=0;
			}
			// okienka informacyjne dla interfejsu i nie tylko
			if(czyMyszOknoInfo==1)
			{
				// wyświeltanie inforjacji o zasobach na górnym pasku
				
			}
		}
	}

	function klawiaturaPrzyciski()
	{
		{
			if(czyStrzalkaLewo==1 || czyA==1)
			{
				if(przesuniecieMapyX+startRysowaniaMapyX+predkoscPrzesuwaniaMapy<canvasGlowny.width/3) // jeśli mapa po przesunięciu na pozycji mniejszej niż 1/3 canvasa od lewej
				{
					przesuniecieMapyX+=predkoscPrzesuwaniaMapy;
				}
			}
			if(czyStrzalkaPrawo==1 || czyD==1)
			{
				if(przesuniecieMapyX+startRysowaniaMapyX+(szerokoscMapyWczytanej*wielkoscPola)-predkoscPrzesuwaniaMapy>canvasGlowny.width-(canvasGlowny.width/3)) // jesli mapa po przesunieciu na pozycji większej niż 2/3 canvasa od prawej
				{
					przesuniecieMapyX-=predkoscPrzesuwaniaMapy;
				}
			}
		}
		if(czyMoznaPrzesuwacY==1) // przesuwanie mapy os Y
		{
			if(czyStrzalkaGora==1 || czyW==1)
			{
				if(przesuniecieMapyY+startRysowaniaMapyY+predkoscPrzesuwaniaMapy<canvasGlowny.height/3) // jeśli mapa po przesunięciu na pozycji mniejszej niż 1/3 canvasa od lewej
				{
					przesuniecieMapyY+=predkoscPrzesuwaniaMapy;
				}
			}
			if(czyStrzalkaDol==1 || czyS==1)
			{
				if(przesuniecieMapyY+startRysowaniaMapyY+(wysokoscMapyWczytanej*wielkoscPola)-predkoscPrzesuwaniaMapy>canvasGlowny.height-(canvasGlowny.height/3)) // jesli mapa po przesunieciu na pozycji większej niż 2/3 canvasa od prawej
				{
					przesuniecieMapyY-=predkoscPrzesuwaniaMapy;
				}
			}
		}
	}
	
	// funkcja rysująca
	function rysuj()
	{
		c.clearRect(0,0,innerWidth,innerHeight);
		
		// rysowanie bloków i obiektów mapy
		for(var i=0; i<tMapa.length; i++)
		{
			for(var j=0;j<tMapa[i].length;j++)
			{
				// rysowanie blokow
				if(tMapa[i][j].blok=="trawa")
				{
					c.drawImage(trawa, (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
				}
				else if(tMapa[i][j].blok=="woda")
				{
					c.drawImage(tWodaPlytka[animWodaKlatka], (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
				}
				
				// rysowanie obiektow
				/*if(tMapa[i][j].obiekt=="kamienieMale")
				{
					c.drawImage(kamienieMale, (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
				}
				else if(tMapa[i][j].obiekt=="stosKamieni")
				{
					c.drawImage(stosKamieni, (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
				}
				else if(tMapa[i][j].obiekt=="maleDrzewko")
				{
					c.drawImage(maleDrzewko, (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
				}*/
				
				if(tMapa[i][j].depozyt.nazwa!="")
				{
					c.drawImage(tMapa[i][j].depozyt.grafika, (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
				}
				else if(tMapa[i][j].obiekt!="") // jeśli coś jest obiektem ale nie w formie programowej
				{
					/*if(tMapa[i][j].obiekt=="kamienieMale")
					{
						c.drawImage(kamienieMale, (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
					else if(tMapa[i][j].obiekt=="stosKamieni")
					{
						c.drawImage(stosKamieni, (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
					else if(tMapa[i][j].obiekt=="maleDrzewko")
					{
						c.drawImage(maleDrzewko, (i*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (j*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}*/
				}
			}
		}
		
		// rysowanie ramki mapy
		for(var i=0;i<szerokoscMapyWczytanej+2;i++)
		{
			for(var j=0;j<wysokoscMapyWczytanej+2;j++)
			{
				if(i==0)// lewa krawędz
				{
					if(j==0)
					{
						c.drawImage(tRamkaMapy[7],((i-1)*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX,((j-1)*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
					else if(j==wysokoscMapyWczytanej+1)
					{
						c.drawImage(tRamkaMapy[2],((i-1)*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX,((j-1)*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
					else
					{
						c.drawImage(tRamkaMapy[4],((i-1)*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX,((j-1)*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
				}
				else if(i==szerokoscMapyWczytanej+1)// prawa krawedz
				{
					if(j==0)
					{
						c.drawImage(tRamkaMapy[6],((i-1)*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX,((j-1)*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
					else if(j==wysokoscMapyWczytanej+1)
					{
						c.drawImage(tRamkaMapy[1],((i-1)*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX,((j-1)*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
					else
					{
						c.drawImage(tRamkaMapy[3],((i-1)*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX,((j-1)*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
				}
				else// krawedzie górna i dolna
				{
					if(j==0)// krawędz górna
					{
						c.drawImage(tRamkaMapy[5],((i-1)*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX,((j-1)*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
					else if(j==wysokoscMapyWczytanej+1)//- krawedz dolna
					{
						c.drawImage(tRamkaMapy[0],((i-1)*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX,((j-1)*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
					}
				}
			}
		}
		
		// rysowanie wybranego budynku z karuzeli i czerwonego / zielonego tła pod nim
		if(obecnieZaznaczone.includes("karuzelaBudynek"))
		{
			if(czyMoznaZbudowac==0)
			{
				c.fillStyle="#FF0000";
				c.globalAlpha=0.5;
				c.fillRect(startRysowaniaMapyX+(przymierzonyKoordynatX*wielkoscPola), startRysowaniaMapyY+(przymierzonyKoordynatY*wielkoscPola), 100, 100);
				c.globalAlpha=1.0;
			}
			else if(czyMoznaZbudowac==1)
			{
				c.fillStyle="#00FF00";
				c.globalAlpha=0.5;
				c.fillRect(startRysowaniaMapyX+(przymierzonyKoordynatX*wielkoscPola), startRysowaniaMapyY+(przymierzonyKoordynatY*wielkoscPola), 100, 100);
				c.globalAlpha=1.0;
			}
			c.globalAlpha=0.7;
			c.drawImage(tBudynkiGry[IDWybranegoBudynku].grafika, startRysowaniaMapyX+(przymierzonyKoordynatX*wielkoscPola), startRysowaniaMapyY+(przymierzonyKoordynatY*wielkoscPola));
			c.globalAlpha=1.0;
		}
		
		// rysowanie budynków na mapie
		for(var i=0; i<maxBudynkowGracza; i++)
		{
			if(tBudynkiGracza[i].nazwa!="")
			{
				c.drawImage(tBudynkiGracza[i].grafika, (tBudynkiGracza[i].korX*wielkoscPola)+startRysowaniaMapyX+przesuniecieMapyX, (tBudynkiGracza[i].korY*wielkoscPola)+startRysowaniaMapyY+przesuniecieMapyY);
			}
			if(i>=maxBudynkowGracza) // bezpiecznik pętli
			{
				console.log("bezpiecznik pętli rysowania budynków na mapie.");
				break;
			}
		}
		
		// rysowanie komunikatow (obecnie nie istnieje)
		/*if(akcjaDoWykonania=="tabliczka1")
		{
			c.fillStyle= "white";
			c.fillRect(mapaStartX+150,450,180,100);
			c.fillStyle= "black";
			c.fillText("Skoro to czytasz to znaczy",mapaStartX+ 160, 470);
			c.fillText("że w końcu się obudziłeś.",mapaStartX+ 160, 490);
			c.fillText("Po prawej masz coś do",mapaStartX+ 160, 510);
			c.fillText("cięcia, przyda ci się.",mapaStartX+ 160, 530);
		}*/
		
		animacje();
		rysujLudki();
		rysujInterfejs();
	}
	
	function animacje()
	{
		// rysowanie animacji terenu
		// tutaj tylko zmiana obecnej klatki w czasie
		// rysowanie animacji w funkcji rysuj() przy rysowaniu grafiki bloku
		
		// animacja wody
		animWodaKlatkaBuf++
		if(animWodaKlatkaBuf>=animWodaCzasKlatki)
		{
			animWodaKlatka++;
			animWodaKlatkaBuf=0;
			if(animWodaKlatka>=animWodaKlatki)
			{
				animWodaKlatka=0;
			}
		}
	}
	
	function rysujLudki()
	{
		if(obecnaIloscLudkow>0)
		{
			for(var i=0; i<limitLudkow;i++)
			{
				if(typeof tLudziki[i]!="undefined")
				{
					if(tLudziki[i].ID!=-1)
					{
						tLudziki[i].rysuj();
					}
				}
			}
		}
	}
	
	function rysujInterfejs()
	{
		// pasek z zasobami na środku u góry
		if(czyUstalacSzerokoscPaskaZasobow==1)// jeśli ilość widocznych zasobów uległa zmianie sprawdz jakie zasoby są widoczne i je zsumuj
		{
			iloscWidocznychZasobow=0;
			for(var i=0; i<tZasoby.length; i++)
			{
				if(tZasoby[i].czyWidoczny==1)
				{
					iloscWidocznychZasobow++;
				}
				if(i>tZasoby.length) //bezpiecznik pętli
				{
					break;
				}
			}
			czyUstalacSzerokoscPaskaZasobow=0;
			szerokoscPaskaGornego+=(110*iloscWidocznychZasobow); // wielkość pola na grafikę oraz opis ilości zasobu to 110 x 30 pix
		}
		c.drawImage(pasekZasobowPoczatek,srodekRysowaniaX-(szerokoscPaskaGornego/2),0);
		for(var i=0; i<tZasoby.length; i++)
		{
			if(tZasoby[i].czyWidoczny==1)
			{
				c.drawImage(pasekZasobowZasob, srodekRysowaniaX-(szerokoscPaskaGornego/2)+(110*i)+30,0);
				c.drawImage(tZasoby[i].grafika, srodekRysowaniaX-(szerokoscPaskaGornego/2)+(110*i)+30,0);
				c.fillStyle="white";
				c.font="14px Arial";
				c.fillText(tZasoby[i].obecnaIlosc+" / "+tZasoby[i].maxIlosc, srodekRysowaniaX-(szerokoscPaskaGornego/2)+(110*i)+56, 18);
			}
		}
		c.drawImage(pasekZasobowKoniec,srodekRysowaniaX+((szerokoscPaskaGornego/2)-30),0);
		// przyciski dolne - szerokość: 170 px wysokość: od 30 do 50 px
		/*
		czyRysowacMenuBudowy=1;
		czyRysowacMenuPrzyciskow=0;
		czyTrybWyburzania=0;
		*/
		
		c.drawImage(menuSrodek_krawedzLewa, srodekRysowaniaX-85, canvasGlowny.height-30);
		c.drawImage(menuSrodek_buduj, srodekRysowaniaX-55, canvasGlowny.height-30);
		c.drawImage(menuSrodek_menu, srodekRysowaniaX-25, canvasGlowny.height-50);
		c.drawImage(menuSrodek_wyburz, srodekRysowaniaX+25, canvasGlowny.height-30);
		c.drawImage(menuSrodek_krawedzPrawa, srodekRysowaniaX+55, canvasGlowny.height-30);
		if(czyRysowacMenuBudowy==1) // rysowanie paska z budynkami na liście do budowy
		{
			// wielkość grafiki budynku 100 x 100 px
			// szerokość paska z budynkami na razie na 1/2 szerokości canvasa + przyciski boczne do przewijania paska z budynkami
			// szerokość prostokąta ramki: szerokosc canvasa / 2 + szerokosc strzalki * 2 = 50 px + kwadraty na strzalki 20 px + ramki kwadratów strzałek + 20 px = szerokosc canvasa / 2 + 90 px
			// wysokosc prostokata ramki: 100 px na budyki + 10 px na odstep buydnek-ramka + 10 px na ramke paska = 120 px
			
			// pasek z budynkami i strzalki
			c.fillStyle="#88919a"; // ramka okna
			c.fillRect(srodekRysowaniaX-(srodekRysowaniaX/2)-45, canvasGlowny.height-190, (canvasGlowny.width/2)+90, 120);
			c.fillStyle="#215381";
			c.fillRect(srodekRysowaniaX-(srodekRysowaniaX/2)-40, canvasGlowny.height-185, 35, 110);
			c.drawImage(strzalkiSzare_lewo, srodekRysowaniaX-(srodekRysowaniaX/2)-35, canvasGlowny.height-140);
			c.fillRect(srodekRysowaniaX-(srodekRysowaniaX/2), canvasGlowny.height-185, canvasGlowny.width/2, 110);
			c.fillRect(srodekRysowaniaX+(srodekRysowaniaX/2)+5, canvasGlowny.height-185, 35, 110);
			c.drawImage(strzalkiSzare_prawo, srodekRysowaniaX+(srodekRysowaniaX/2)+10, canvasGlowny.height-140);
			
			// rysowanie karuzeli z budynkami, nie to nie ma być karuzela spie****enia :)
			buforA=0;
			for(var i=0; i<iloscBudynkowGry; i++)
			{
				if(tBudynkiGry[i].czyDostepny==1)
				{
					c.drawImage(tBudynkiGry[i].grafika, srodekRysowaniaX-(srodekRysowaniaX/2)+(5*buforA)+(100*buforA), canvasGlowny.height-180);
				}
			}
		}
		else if(czyRysowacMenuPrzyciskow==1) // menu z przyciskami na środku ( ludziki zbieranie oraz możliwość zapisu lub wczytania gry )
		{
			//
		}
		
		// okno zaznaczonej rzeczy
		if(obecnieZaznaczone!="")
		{
			if(obecnieZaznaczone.includes("Ludzik_"))// okno zaznaczonego ludzika
			{
				// okno 250 x 300
				buforIdLudzika=obecnieZaznaczone.split("_");// wydobycie ID zaznaczonego ludzika
				c.fillStyle="#88919a"; // ramka okna
				c.fillRect(poczatekXOknaLudzika, srodekRysowaniaY-155,szerokoscOknaLudzika+10,wysokoscOknaLudzika+10);
				c.fillStyle="#215381";
				c.fillRect(25, srodekRysowaniaY-150,szerokoscOknaLudzika,wysokoscOknaLudzika);
				c.fillStyle="#88919a"; // ramka pod grafikę
				c.fillRect(28, srodekRysowaniaY-147,104,104);
				c.fillStyle="#215381";
				c.fillRect(30, srodekRysowaniaY-145,100,100);
				c.drawImage(tLudziki[buforIdLudzika[1]].grafika, 30, srodekRysowaniaY-145, 100, 100);
				c.fillStyle="white";
				c.font="18px Arial";
				
				// imie ludzika
				c.fillText(tLudziki[buforIdLudzika[1]].ID, 140, srodekRysowaniaY-125); // nazwa ludzika, na razie ID; dodać centrowanie kiedy będzie imie
				
				// płeć ludzika
				if(tLudziki[buforIdLudzika[1]].plec=="kob")
				{
					c.drawImage(plecPostaci_kobieta, 135, srodekRysowaniaY-115);
				}
				else
				{
					c.drawImage(plecPostaci_menzczyzna, 135, srodekRysowaniaY-115);
				}
				
				// zadanie ludzika
				c.fillText("Zadanie:", 165, srodekRysowaniaY-98);
				if(tLudziki[buforIdLudzika[1]].zadanie=="brak")
				{
					// c.fillText(tLudziki[buforIdLudzika[1]].zadanie, 140, srodekRysowaniaY-70); // (tLudziki[buforIdLudzika[1]].zadanie.length/2) - nie działa jak należy
					c.fillText("brak", 185, srodekRysowaniaY-75);
				}
				else if(tLudziki[buforIdLudzika[1]].zadanie=="Przemieszcza sie")
				{
					c.fillText("Idzie", 185, srodekRysowaniaY-75);
				}
				else if(tLudziki[buforIdLudzika[1]].zadanie=="Zbiera")
				{
					c.fillText("Zbiera", 140, srodekRysowaniaY-75);
					if(tLudziki[buforIdLudzika[1]].zebranyPrzedmiot=="male kamienie")
					{
						c.drawImage(surowiecKamien, 200, srodekRysowaniaY-90);
					}
					else if(tLudziki[buforIdLudzika[1]].zebranyPrzedmiot=="drewno")
					{
						c.drawImage(surowiecDrewno, 200, srodekRysowaniaY-90);
					}
				}
				
				// jeśli ludzik coś zebrał
				if(tLudziki[buforIdLudzika[1]].zebranaIlosc!=0)
				{
					c.fillText("Zebrano:       "+tLudziki[buforIdLudzika[1]].zebranaIlosc.toString(), 140, srodekRysowaniaY-50);
					if(tLudziki[buforIdLudzika[1]].zebranyPrzedmiot=="male kamienie")
					{
						c.drawImage(surowiecKamien, 215, srodekRysowaniaY-65);
					}
					else if(tLudziki[buforIdLudzika[1]].zebranyPrzedmiot=="drewno")
					{
						c.drawImage(surowiecDrewno, 215, srodekRysowaniaY-65);
					}
				}
				
				// przyciski stop i drop ; wym 25x25 px
				if(tLudziki[buforIdLudzika[1]].zadanie!="brak")
				{
					c.drawImage(stopButton, 165, srodekRysowaniaY-30);
				}
				if(tLudziki[buforIdLudzika[1]].zebranaIlosc!=0)
				{
					c.drawImage(dropButton, 200, srodekRysowaniaY-30);
				}
				
				// pasek głodu
				c.drawImage(surowiecJedzenie, 27, srodekRysowaniaY-30);
				c.fillStyle="#88919a"; // ramka pod pasek
				c.fillRect(52, srodekRysowaniaY-25,104,15);
				c.fillStyle="#215381"; // puste pole paska
				c.fillRect(54, srodekRysowaniaY-23,100,11);
				if(tLudziki[buforIdLudzika[1]].najedzenie>=50)
				{
					c.fillStyle="Green";
				}
				else if(tLudziki[buforIdLudzika[1]].najedzenie<50 && tLudziki[buforIdLudzika[1]].najedzenie>20)
				{
					c.fillStyle="Yellow";
				}
				else
				{
					c.fillStyle="Red";
				}
				c.fillRect(54, srodekRysowaniaY-23,tLudziki[buforIdLudzika[1]].najedzenie,11);
				
				// pasek pragnienia
				c.drawImage(surowiecWoda, 27, srodekRysowaniaY);
				c.fillStyle="#88919a"; // ramka pod pasek
				c.fillRect(52, srodekRysowaniaY+5,104,15);
				c.fillStyle="#215381"; // puste pole paska
				c.fillRect(54, srodekRysowaniaY+7,100,11);
				if(tLudziki[buforIdLudzika[1]].napojenie>=50)
				{
					c.fillStyle="Green";
				}
				else if(tLudziki[buforIdLudzika[1]].napojenie<50 && tLudziki[buforIdLudzika[1]].napojenie>20)
				{
					c.fillStyle="Yellow";
				}
				else
				{
					c.fillStyle="Red";
				}
				c.fillRect(54, srodekRysowaniaY+7,tLudziki[buforIdLudzika[1]].napojenie,11);
				
				// strój oraz narzędzie
				c.fillStyle="#88919a"; // ramka pod grafike stroju
				c.fillRect(164, srodekRysowaniaY+5,54,54);
				c.drawImage(slotNaStroj, 166, srodekRysowaniaY+7);
				c.fillStyle="#88919a"; // ramka pod grafike narzędzia
				c.fillRect(216, srodekRysowaniaY+5,54,54);
				c.drawImage(slotNaNarzedzie, 218, srodekRysowaniaY+7);
				
				// kreska oraz statystyki
				c.fillStyle="#88919a";
				c.fillRect(70, srodekRysowaniaY+65,150,2); // nie pamiętam polecenia na rysowanie linii więc narysuje bardzo płaski prostokąt :)
				c.fillStyle="White";
				c.fillText("Szybkość: "+tLudziki[buforIdLudzika[1]].predkosc, 30, srodekRysowaniaY+85);
				c.fillText("Zbieranie: "+tLudziki[buforIdLudzika[1]].predkoscZbierania, 30, srodekRysowaniaY+110);
			}
		}
	}
	
	function dodajLudzika(pozX, pozY) // dodać niemożność spawnowania ludzików na zajętych juz polach oraz zajmowanie pola w momencie spawnu
	{
		if(obecnaIloscLudkow<limitLudkow)
		{
			for(var i=0;i<limitLudkow;i++)
			{
				if(typeof tLudziki[i]!="undefined")
				{
					if(tLudziki[i].ID==-1) // recykling slotu po martwym ludziku
					{
						tLudziki[i]=new ludzik(i, pozX, pozY);
						obecnaIloscLudkow++;
						tZasoby[0].obecnaIlosc++;
						console.log("%c "+" %c Pojawił się nowy ludzik - ID: "+i+ " - recykling slotu" , 'background-color: green;', "background-color: white;");
						break;
					}
				}
				else // tworzenie nowego ludzika
				{
					tLudziki[i]=new ludzik(i, pozX, pozY);
					obecnaIloscLudkow++;
					tZasoby[0].obecnaIlosc++;
					console.log("%c "+" %c Pojawił się nowy ludzik - ID: "+i+ " - nowy slot" , 'background-color: green;', "background-color: white;");
					break;
				}
			}
		}
		else
		{
			console.log("%c "+" %c brak miejsca na nowych ludzików" , 'background-color: yellow;', "background-color: white;");
		}		
	}
	
	function ludzikiZadania()
	{
		if(obecnaIloscLudkow>0)
		{
			for(var i=0;i<obecnaIloscLudkow;i++)
			{
				if(trybDebugowania==1 && DebugowanieLudzikow==1){
					//console.log("zadanie dla: "+i);
				}
				tLudziki[i].wykonajZadanie();
			}
		}
	}
	
	function mozliwoscBudowy()
	{
		if(obecnieZaznaczone.includes("karuzelaBudynek"))
		{
			if(myszX-startRysowaniaMapyX>0 && myszX-startRysowaniaMapyX<(szerokoscMapyWczytanej*wielkoscPola)) // czy mysz w obszarze mapy w osi X
			{
				if(myszY-startRysowaniaMapyY>0 && myszY-startRysowaniaMapyY<(wysokoscMapyWczytanej*wielkoscPola)) // czy mysz w obszarze mapy w osi Y
				{
					// przypisanie koordynatów rysowania grafiki budynku do przymierzanego pola
					przymierzonyKoordynatX=parseInt(((myszX-startRysowaniaMapyX)-(myszX%wielkoscPola))/wielkoscPola);
					przymierzonyKoordynatY=parseInt(((myszY-startRysowaniaMapyY)-(myszY%wielkoscPola))/wielkoscPola);
					
					buforUniA=1;
					buforUniB=1;
					buforUniC=1;
					buforUniA=parseInt(buforUniA);
					buforUniB=parseInt(buforUniB);
					buforUniC=parseInt(buforUniC);
					// sprawdzanie kolizji przymierzanego budynku z obiektami i wodą na mapie
					for(var i=0;;i++)
					{
						for(var j=0;;j++)
						{
							if(buforUniB>tBudynkiGry[IDWybranegoBudynku].wielkoscY)
							{
								buforUniB=1;
								break;
							}
							else
							{
								if(tMapa[parseInt(przymierzonyKoordynatX+(buforUniA-1))][parseInt(przymierzonyKoordynatY+(buforUniB-1))].obiekt!="" || tMapa[przymierzonyKoordynatX+(buforUniA-1)][przymierzonyKoordynatY+(buforUniB-1)].blok=="woda")
								{
									buforUniC=0;
									czyMoznaZbudowac=0;
									break;
								}
							}
							buforUniB++;
							if(j==100) // bezpiecznik
							{
								console.log("bezpiecznik 'j' sprawdzanie kolizji przymierzanego budynku z obiektami na mapie")
								break;
							}
						}
						
						if(buforUniA>=tBudynkiGry[IDWybranegoBudynku].wielkoscX || buforUniC==0)
						{
							buforUniA=1;
							break;
						}
						buforUniA++;
						if(i==100) // bezpiecznik
						{
							console.log("bezpiecznik 'i' sprawdzanie kolizji przymierzanego budynku z obiektami na mapie")
							break;
						}
					}
					if(buforUniC==1) // jesli nie ma kolizji to ustalamy mozliwoscBudowy na 1
					{
						czyMoznaZbudowac=1;
					}
				}
			}
		}
	}
	
	function przypiszParametryNowegoBudynku(IDPozycjiTablicy, buforNazwy, IDbudowanegoBudynku)
	{
		/*function budynekGracza()
		{
			this.nazwa="";
			this.wytrzymalosc=0;
			this.grafika="";
			this.korX=0;
			this.korY=0;
			this.wielkoscX=0;
			this.wielkoscY=0;
			this.surANazwa="";
			this.surAIlosc=0;
			this.surBNazwa="";
			this.surBIlosc=0;
			this.prodANazwa="";
			this.prodAIlosc=0;
			this.prodBNazwa="";
			this.prodBIlosc=0;
			this.pracownik=0; // ID ludzika przypisanego do pracy w danym budynku
			
			// zmienne zasobów - tych na pasku górnym
			tZasoby=new Array(5); // spis wszystkich zasobów w grze
			tZasoby[0]=new zasob("Ludzie", surowiecLudziki, 1);
			tZasoby[1]=new zasob("Woda", surowiecWoda, 1);
			tZasoby[2]=new zasob("Jedzenie", surowiecJedzenie, 1);
			tZasoby[3]=new zasob("Drewno", surowiecDrewno, 1);
			tZasoby[4]=new zasob("Kamień", surowiecKamien, 1);
			
			function zasob(nazwa, grafika, czyWidoczny)
			{
				this.nazwa=nazwa;
				this.grafika=grafika;
				this.obecnaIlosc=0;
				this.maxIlosc=0;
				this.opis="<tutaj krótki opis zasobu>";
				this.czyWidoczny=czyWidoczny; // 1 / 0
			}
		}*/
		if(IDbudowanegoBudynku==0)// magazyn ogólny
		{
			tBudynkiGracza[IDPozycjiTablicy].nazwa=buforNazwy;
			tBudynkiGracza[IDPozycjiTablicy].wytrzmalosc=100;
			tBudynkiGracza[IDPozycjiTablicy].grafika=magazynOgolnyA_trawa;
			tBudynkiGracza[IDPozycjiTablicy].korX=przymierzonyKoordynatX;
			tBudynkiGracza[IDPozycjiTablicy].korY=przymierzonyKoordynatY;
			tBudynkiGracza[IDPozycjiTablicy].wielkoscX=2;
			tBudynkiGracza[IDPozycjiTablicy].wielkoscY=2;
			for(var i=2;i<5;i++)
			{
				tZasoby[i].maxIlosc+=100;
			}
			for(var i=0;i<2;i++) // wstawienie obiektu na pola mapy
			{
				for(var j=0;j<2;j++)
				{
					tMapa[przymierzonyKoordynatX+i][przymierzonyKoordynatY+j].obiekt=buforNazwy;
				}
			}
		}
	}
	
	function odswierzanie()
	{
		//console.log("frame");
		myszkaPrzyciski();
		klawiaturaPrzyciski();
		ludzikiZadania();
		mozliwoscBudowy();
		rysuj();
		if(czyWczytacMapeZBazy==1)
		{
			wczytajMapeZBazy();
			PoczatkoweTworzenieLudzikow();
			czyWczytacMapeZBazy=0;
		}
		if(buforCyklFPS==0)
		{
			buforCyklFPS=1;
			cyklFPS=1;
		}
	}
	
	setInterval(odswierzanie, FPS);
	console.log("Gotowe. Funkcja dane() zawiera informacje o niektórych zmiennych gry.");
	console.log("Ver.: Pre-alpha 0.2.0 - made by bartek751.");
	console.log("========================================");
	
	// ką/oncik pomocniczy :)
	/*
		kolor tła komunikatu na szary i tekstu na biały:
			console.log("%cLudzik "+this.ID+", raportowanie aktywne.", 'background-color: grey; color: white');
		c.fillStyle="white";
		c.fillRect(graczX,graczY,4,4);
		c.drawImage(PostacA, mapaStartX+graczX, mapaStartY+graczY);
		A = zmiennaTekstowa.split("_"); - dzieli zmiennaTekstowa na fragmenty gdzie dzielnikiem jest _ i zapisuje w formie tablicy w zmiennej A
		obecnieZaznaczone.includes("Ludzik_") - jeśli obecnieZaznaczone zaiera "Ludzik" to zwróć prawdę
	*/
</script>
</html>