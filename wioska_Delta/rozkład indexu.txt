1. Opis html/css
2. osadzenie canvasa, dodanie grafik do dokumentu oraz początek skryptu
3. przystosowanie canvasa oraz tablicy mapy
4. funkcja wczytania mapy z bazy
5. dodanie grafik do skryptu
6. zmienne oraz klasa ludzików
7. zmienne klawiszowe, środowiskowe, zasobowe, budynków, operacyjne oraz funkcje dane() w kolejności takiej jaka podana
8. przechwytywanie klawiatury oraz myszki
9. funkcje rysuj(), animacje(), rysujLudki() oraz rysujInterfejs()
10. funkcje dodajLudzika() oraz wykonywania zadań ludzików - ludzikiZadania()
11. funkcje mozliwoscBudowy() oraz przypisania parametrów startowych dla nowych budynków
12. pętla główna i koniec skryptu