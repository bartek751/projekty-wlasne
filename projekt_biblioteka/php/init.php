<?php
$nazwa = "localhost";
$uzytkownik = "root";
$haslo = "";
$nazwa_db = "biblioteka";
$conn = new mysqli($nazwa, $uzytkownik, $haslo, $nazwa_db);
if ($conn->connect_error) {
    die("Połączenie z bazą nie zostało nawiązane: " . $conn->connect_error);
} 

// tworzenie sesji ( w projekcie biblioteka jest domyślnie tworzona w pliku połączenia ) 
session_start();

// tworzenie zmiennej "x"
//$_SESSION["x"] = "wartosc";

// wypisanie wartości zmiennej
// echo "zmienna x = ".$_SESSION["x"];

// wypisanie wszystkich zmiennych sesyjnych
//print_r($_SESSION);

// usuniecie zmiennej "x" ze zmiennych sesyjnych
//unset($_SESSION["x"]);

// jesli nie ma tablicy "tab" to ja tworzymy
/*
if(!isset($_SESSION["tab"]))
{
$_SESSION["tab"]=array();
}
*/

// tworzenie tablicy 2D z tablicy "tab" wraz z przypisaniem jej wartosci
//$_SESSION["koszyk"][]=array($a, $b, $c);

// usunięcie wszystkich zmiennych sesyjnych
//session_unset();

// zakończenie sesji
// session_destroy();

// jedno ze źródeł:
// https://www.w3schools.com/php/php_sessions.asp
?>