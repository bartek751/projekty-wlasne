<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Lasy</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%">
					<b>Lasy</b> w grze technopolis są zbudowane z drzew iglastych. Za pomocą przyległych do nich 
					<a href="tartak.php" id="odnosnik"><img src="../grafiki/tartak.png" class="ikonaOdnosnika">tartaków</a> można pozyskiwać z nich zarówno
					<img src="../grafiki/ikona_drewno.png"><a href="drewno.php" id="odnosnik">drewno</a> jak i 
					<img src="../grafiki/ikona_zywica.png"><a href="zywica.php" id="odnosnik">żywicę</a>.
					W trakcie pozyskiwania <img src="../grafiki/ikona_drewno.png"><a href="drewno.php" id="odnosnik">drewna</a> drzewa są powoli usuwane z przyległego 
					<a href="tartak.php" id="odnosnik"><img src="../grafiki/tartak.png" class="ikonaOdnosnika">tartaku</a>
					jednak zaprzestanie jego pozyskiwana spowoduje, że drzewa po pewnym czasie odrosną w miejsce tych wyciętych, gdyż lasy dążą do powolnego rozrostu.
					</p>
					<p>
					Na początku każdej rozgrywki generowane są od 1 do 4 grup drzew, o różnej wielkości. 
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid mt-1" src="grafiki/fragmentLasu.png">
					<div class="card-body">
						<p class="card-text text-center">fragment lasu</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>