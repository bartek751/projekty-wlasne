<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Kopalnia głębinowa</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
					<b>Kopalnia głębinowa</b> to jeden z późniejszych budynków dostępnych w grze. Pozwala ona na prowadzenie wydobycia i budowanie szybów pod powierzchnią, 
					gdzie można znaleźć takie surowce jak <a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapienie</a>, 
					<a href="piryt.php"><img src="../grafiki/ikona_piryt.png">piryt</a>, <a href="malachit.php"><img src="../grafiki/ikona_malachit.png">malachit</a> 
					czy <a href="halit.php"><img src="../grafiki/ikona_halit.png">sól</a>.
					Jej utrzymanie wiąże się z kosztami wyższymi, niż w przypadku innych budynków, gdyż do działania potrzebuje minimum trzech 
					ludzi, z czego jeden zawsze jest na powierzchni i nie prowadzi wydobycia. 
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/kopalniaGlebinowa.png">
					<div class="card-body">
						<p class="card-text text-center">Kopalnia głębinowa</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<p class="fs-5">
						Koszt budowy to:
						<ul>
							<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">3000 monet</a></li>
							<li><a href="deski.php"><img src="../grafiki/ikona_deski.png">400 desek</a></li>
							<li><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">50 cegły</a></li>
							<li><a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">40 Żelazo</a></li>
						</ul>
					</p>
					<p class="fs-5">
						Dodatkowo do budowy kopalni głębinowej potrzeba 4 wolnych ludzi.<br>
						Obecnie kopalnia głębinowa nie podlega ulepszeniom.
					</p>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<P class="fs-5">
						<b>Eksploracja podziemi:</b>
					</p>
					<p style="text-indent: 3%;" class="fs-5">
					Schodząc pod ziemię widzimy trzy rodzaje pól. Wydobyte, dostępne do wydobycia oraz czarne, jeszcze nie widoczne. Wydobycie można rozpocząć klikając LPM na pole dostępne 
					do wydobycia. Z kolei jeśli chcemy zaprzestać kopania, należy kliknąć na dane pole PPM. Miejsca, gdzie prowadzone są prace oznaczone są kilofem oraz liczbą pracujących na 
					miejscu górników. 
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>