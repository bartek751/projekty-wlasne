<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<div class="col-sm-12 col-md-4 mt-1">
				<div class="d-flex justify-content-center align-items-center h-100">
					<a href="zasoby.php" class="btn btn-outline-success">
						<i>Powrót do zasobów</i>
					</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-4 text-center mt-1">
				Kwarc
			</div>
			<div class="d-none d-md-block col-md-4 mt-1">
				
			</div>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
						<b>Kwarc</b> w grze technopolis może być użyty do ulepszeń w <a href="warsztat.php"><img src="../grafiki/warsztat.png" class="ikonaOdnosnika">warsztacie</a>, 
						w automatyzacji w budynkach oraz jako przedmiot <a href="rynek.php"><img src="../grafiki/DDCoin.png">handlu</a>. Pozyskuje się go za pomocą
						<a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnosnika">żwirowni</a>, 
						<a href="kopalniaGlebinowa.php"><img src="../grafiki/kopalniaGlebinowa.png" class="ikonaOdnosnika">kopalni głębinowej</a>
						lub poprzez <a href="rynek.php"><img src="../grafiki/DDCoin.png">rynek (giełda towarów)</a>.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaMalegoObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="card-img-top mx-auto d-block img-fluid" src="../grafiki/ikona_kwarc.png">
					<div class="card-body">
						<p class="card-text text-center">ikona kwarcu</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Kwarc w rzeczywistości</b>
					</p>
					<p style="text-indent: 3%;">
						W rzeczywistości jest on minerałem zbudowanym głównie z dwutlenku krzemu. Występują różne rodzaje kwarcu a jego barwa również jest różna np. biały, 
						pomarańczowy lub też bezbarwny. Jest stosowany między innymi w przemyśle ceramicznym, w sprzęcie medycznym i naukowym, w elektronice i paru innych miejscach.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>