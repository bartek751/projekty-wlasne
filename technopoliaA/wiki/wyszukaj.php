<html>
<head>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="mt-1 text-center">
				Wyniki wyszukiwania: 
				<?php
					/*
					Pozyskać wartość string z pola tekstowego _/ ( $szukane )
					Wczytać tablicę z nazwami plików _/ ( $tPlikiSzukania )
					Sprawdzać zawartość plików pod kątem zbieżności z szukanym stringiem _/
					Pliki ze znalezionym stringiem oznaczyć _/
					Dodać komunikat o braku znalezionych wyników _/
					Dodać zabezpieczenia _/
					Stworzyć listę z odnośnikami do oznaczonych plików
						- problem zmienionej czcionki _/
						- problem zapisu zbyt dużej ilości tekstu _/
						- problem wczytania grafiki _/
						- problem zagnieżdżania się tekstu przy odnośnikach do grafik _/
						- problem nie zamkniętego odnosnika _/
						- problem nie zamkniętego drugiego odnosnika _/
						- problem znaku specjalnego w artykuje o hematycie X ( zignorowany, przesunięto próg skracania i problem znikną )
						- problem z wystąpieniem żelaza w artykule o piasku _/
						- problem z brakiem artykułu o magazynie _/ ( poprawiono sam artykuł dodając znacznik <p> )
					Sprzątnąć kod _/
					*/
					if(isset($_POST["szukaj"]))
					{
						$szukane=$_POST["szukaj"];
						if($szukane!="")
						{
							$szukane[0]=strtoupper($szukane[0]);
							echo $szukane."<br>";
							include 'listaPlikowSzukania.php';
							
							$czyKropkaKoncaZdania=0;
							$czyZnalezionoCokolwiek=0;
							$czyZnalezionoZdjecie=0;
							$czyZnalezionoOdnosnik=0;
							$czyDwaOdnosniki=0;
							$pierwszaKropka=0;
							
							$bufor="";
							$pierwszyWynik=0; // pierwszy wyswietlony wynik
							$nonNumericValueTransformedIntoInt=0;
							$dlugoscTresci=155; // limit dlugosci tresci pod odnosnikiem - wymagane minimum 3
							$tZnalezioneIndexy = array_fill( 0, $iloscPlikowDoSprawdzenia, 0); // tablica z oznaczonymi wynikami znalezionymi
							$trybWczytywania=0; // 0 - linia po linii, 1 - znak po znaku dla tresci wlasciwej
							$wyraz=""; // kontrola wczytywanej tresci
							$trescWlasciwa="";
							for($i=0;$i<$iloscPlikowDoSprawdzenia;$i++)
							{
								$plik=fopen($tPlikiSzukania[$i][0],"r") or die("Niemożność otworzenia pliku".$tPlikiSzukania[$i][0]);
								$pierwszaKropka=0;
								$czyKropkaKoncaZdania=0;
								$czyZnalezionoZdjecie=0;
								$czyZnalezionoOdnosnik=0;
								$czyDwaOdnosniki=0;
								$trescWlasciwa="";
								for(;!feof($plik);)
								{
									if($trybWczytywania==0)
									{
										$liniaTekstu=fgets($plik);
									}
									else if($trybWczytywania==1)
									{
										$znakTekstu=fgetc($plik);
										if($znakTekstu!=" " && $znakTekstu!="\n")
										{
											$wyraz.=$znakTekstu;
										}
										else
										{
											$wyraz="";
										}
										$trescWlasciwa.=$znakTekstu;
										if(strpos($wyraz, "<img")!==false)
										{
											$czyZnalezionoZdjecie=1;
										}
										if(strpos($wyraz, "href=")!==false)
										{
											$czyZnalezionoOdnosnik=1;
										}
										if($czyZnalezionoZdjecie==1 && $znakTekstu==">")
										{
											$czyZnalezionoZdjecie=0;
											$pierwszaKropka=0;
										}
										if($czyZnalezionoOdnosnik==1 && $znakTekstu==">")
										{
											$czyZnalezionoOdnosnik=0;
											$pierwszaKropka=0;
										}
										if($znakTekstu=="." && $pierwszaKropka==0 && $czyKropkaKoncaZdania==0)
										{
											$pierwszaKropka=1;
										}
										else if($znakTekstu!="." && $czyZnalezionoZdjecie==0 && $czyZnalezionoOdnosnik==0 && $pierwszaKropka==1)
										{
											$czyKropkaKoncaZdania=1;
										}
										if($czyKropkaKoncaZdania==1 && $pierwszaKropka==1) // stwierdzono tresc do zapisu | kontrola zawartosci do zapisu
										{
											$pierwszaKropka=0;
											if(strpos($trescWlasciwa, "<font")!==false) // sprawdzenie i poprawka znacznika <font>
											{
												if(strpos($trescWlasciwa, "size=\"4\"")!==false)
												{
													$bufor="</font><font size=\"5\"";
													$bufor.=$trescWlasciwa;
													$trescWlasciwa=$bufor;
												}
											}
											if(strpos($trescWlasciwa, "<p style=\"text-indent: 3%;\">")!==false)// sprawdzenie i poprawka marginesu
											{
												$nonNumericValueTransformedIntoInt=strlen("<p style=\"text-indent: 3%;\">");
												$nonNumericValueTransformedIntoInt=intval($nonNumericValueTransformedIntoInt);
												$bufor=substr($trescWlasciwa, $nonNumericValueTransformedIntoInt+5);
												$trescWlasciwa=$bufor;
											}
											if(strlen($trescWlasciwa)>$dlugoscTresci)// sprawdzenie i poprawka na długosc tresci
											{
												$bufor="";
												for($k=0;$k<$dlugoscTresci-3;$k++)
												{
													$bufor.=$trescWlasciwa[$k];
												}
												$bufor.=". . .";
												$trescWlasciwa=$bufor;
											}
											if(strpos($trescWlasciwa, "<img")!==false && strpos($trescWlasciwa, "png\">")===false)// sprawdzenie i poprawka znacznika <img>
											{
												$nonNumericValueTransformedIntoInt=strrpos($trescWlasciwa,"<img");
												$nonNumericValueTransformedIntoInt=intval($nonNumericValueTransformedIntoInt);
												$bufor=substr($trescWlasciwa, 0,$nonNumericValueTransformedIntoInt-1);
												$trescWlasciwa=$bufor;
												if(strpos($trescWlasciwa, ". . .")===false)
												{
													$trescWlasciwa.=". . .";
												}
											}
											if(strpos($trescWlasciwa, "<a")!=strrpos($trescWlasciwa,"<a")) // wykrycie dwoch odnosnikow
											{
												$czyDwaOdnosniki=1;
											}
											if($czyDwaOdnosniki==0 && strpos($trescWlasciwa, "<a")!==false && strpos($trescWlasciwa, "</a>")===false)// sprawdzenie i poprawka znacznika <a href>
											{
												if($czyDwaOdnosniki==0 && strpos($trescWlasciwa, "href=")!==false && strpos($trescWlasciwa, "id=\"odnosnik\">")===false)
												{
													$nonNumericValueTransformedIntoInt=strrpos($trescWlasciwa,"<a");
													$nonNumericValueTransformedIntoInt=intval($nonNumericValueTransformedIntoInt);
													$bufor=substr($trescWlasciwa, 0,$nonNumericValueTransformedIntoInt-1);
													$trescWlasciwa=$bufor;
												}
												$trescWlasciwa.="</a></a>";
												if(strpos($trescWlasciwa, ". . .")===false)
												{
													$trescWlasciwa.=". . .";
												}
											}
											else if($czyDwaOdnosniki==1 && (strrpos($trescWlasciwa,"<a") > strrpos($trescWlasciwa,"</a>")))
											{
												$nonNumericValueTransformedIntoInt=strrpos($trescWlasciwa,"<a");
												$nonNumericValueTransformedIntoInt=intval($nonNumericValueTransformedIntoInt);
												$bufor=substr($trescWlasciwa, 0,$nonNumericValueTransformedIntoInt-1);
												$trescWlasciwa=$bufor;
												$trescWlasciwa.="</a></a>";
												if(strpos($trescWlasciwa, ". . .")===false)
												{
													$trescWlasciwa.=". . .";
												}
											}
											$tPlikiSzukania[$i][2]=$trescWlasciwa; // zapis tresci
										}
									}
									// kontrona trybu wczytywania
									if($trybWczytywania==0 && (strpos($liniaTekstu, "<p>")!==false || strpos($liniaTekstu, "<p style=\"text-indent: 3%;\">")!==false || strpos($liniaTekstu, "<p style=\"text-indent: 5%;\">")!==false))
									{
										$trybWczytywania=1;
									}
									else if($trybWczytywania==1 && strpos($wyraz, "</p>")!==false)
									{
										$trybWczytywania=0;
										for($j=0;$j<2;$j++)
										{
											if($j==0)
											{
												$szukane[0]=strtoupper($szukane[0]);
											}
											else
											{
												$szukane[0]=strtolower($szukane[0]);
											}
											if(strpos($trescWlasciwa, $szukane)!==false)
											{
												$tZnalezioneIndexy[$i]=1;
												if($czyZnalezionoCokolwiek==0)
												{
													$czyZnalezionoCokolwiek=1;
												}
												if($szukane==$tPlikiSzukania[$i][1])// ustalenie pierwszego wyniku
												{
													$pierwszyWynik=$i;
												}
											}
										}
										$trescWlasciwa="";
										$wyraz="";
									}
								}
								fclose($plik);
							}
						}
						else
						{
							echo "Pole szukania puste";
						}
					}
				?>
			</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row mt-1 fs-5">
				
				<?php
				if(isset($czyZnalezionoCokolwiek))
				{
					if($czyZnalezionoCokolwiek==1) // wypisanie wynikow
					{
						echo "
							<div class='col-12 m-1' id='centrumGrupa'>
								<a href='".$tPlikiSzukania[$pierwszyWynik][0]."'>".$tPlikiSzukania[$pierwszyWynik][1]."</a><br>
								<p class='mt-1 mb-2'>
									".$tPlikiSzukania[$pierwszyWynik][2]."<br>
								</p>
							</div>
							";
						for($i=0;$i<$iloscPlikowDoSprawdzenia;$i++)
						{
							if($tZnalezioneIndexy[$i]==1 && $i!=$pierwszyWynik)
							{
								echo "
									<div class='col-12 m-1' id='centrumGrupa'>
										<a href='".$tPlikiSzukania[$i][0]."'>".$tPlikiSzukania[$i][1]."</a><br>
										<p class='mt-1 mb-2'>
											".$tPlikiSzukania[$i][2]."<br>
										</p>
									</div>
									";
							}
						}
					}
					else
					{
						echo "Nie znaleziono szukanego wyrazu.";
					}
				}
				else
				{
					echo "";
				}
				?>
				
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>