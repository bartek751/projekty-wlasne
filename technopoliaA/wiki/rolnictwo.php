<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Rolnictwo</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
						<b>Rolnictwo</b> to jeden z systemów w grze. Umożliwia on hodowlę oraz pozyskiwanie roślin uprawnych.
					</p>
					Obecnie dostępne są następujące rośliny uprawne:
					<ul>
						<li><a href="zboze.php"><img src="../grafiki/ikona_zboze.png">Zboże</a></li>
						<li><a href="marchew.php"><img src="../grafiki/ikona_marchew.png">Marchew</a></li>
						<li><a href="ziemniaki.php"><img src="../grafiki/ikona_ziemniaki.png">Ziemniaki</a></li>
					</ul>
					<p style="text-indent: 3%;">
						Aby móc uprawiać te rośliny należy w pierwszej kolejności przeprowadzić odpowiednie dla nich 
						<a href="badania.php"><img src="../grafiki/przycisk_badania.png" class="ikonaOdnosnika">badania</a>, dzięki którym gracz odblokowuje również pola uprawne. 
						Pola uprawne mogą być umieszczone na dowonych polach mapy, które nie są zajęte przez drzewa, piasek, kamienie oraz inne budynki.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid mt-1" src="../grafiki/poleUprawne-chwasty.png">
					<div class="card-body">
						<p class="card-text text-center">Pole zarośnięte chwastami</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p style="text-indent: 3%;">
						Budowa pola uprawnego kosztuje <a href="zasoby.php"><img src="../grafiki/DDCoin.png"> 100 monet</a> oraz 
						<a href="kamien.php"><img src="../grafiki/ikona_kamien.png"> 10 kamienia</a>. Dodatkowo wymagany jest jeden wolny człowiek
						zarówno do budowy pola jak i wykonywania przy nim prac.
					</p>
					<p style="text-indent: 3%;">
						Kiedy gracz ma już przeprowadzone odpowiednie <a href="badania.php"><img src="../grafiki/przycisk_badania.png" class="ikonaOdnosnika">badania </a>
						oraz ma zbudowane pole może sie przymieżyć do zasadzenia roślin. Każda roślina ma swój przedział czasowy, w którym może ona
						zostać zasadzona na polu, są to odpowiednio:
					<ul>
						<li><a href="zboze.php"><img src="../grafiki/ikona_zboze.png">Zboże</a> - od Lutego do Kwietnia</li>
						<li><a href="marchew.php"><img src="../grafiki/ikona_marchew.png">Marchew</a> - od Kwietnia do Czerwca</li>
						<li><a href="ziemniaki.php"><img src="../grafiki/ikona_ziemniaki.png">Ziemniaki</a> - od Marca do Maja</li>
					</ul>
					</p>
					<p style="text-indent: 3%;">
						Każda z roślin rośnie około 4 miesiące w grze, czyli od 360 do 480 sekund czasu rzeczywistego. Aby zasadzić daną roślinę gracz musi znajdować się w 
						trakcie, któregoś z miesięcy umożliwiających jej uprawę. Dodatkowo musi on posiadać 20 sztuk danej rośliny, 
						<a href="zasoby.php"><img src="../grafiki/DDCoin.png"> 50 - 70 monet</a> oraz wspomnianego już wcześniej jednego wolnego człowieka. Gdy rośliny osiągną
						ostatnie stadium wzrostu gracz musi zlecić zebranie plonów z pola. Niezebrane uprawy po pewnym czasie zamienią się w chwasty.
					</p>
					<p style="text-indent: 3%;">
						Jeżeli w trakcie trwania ciepłych miesięcy gracz nie będzie korzystał z pola uprawnego to zarośnie ono chwastami. Gracz może wówczas zlecić usunięcie 
						chwastów z pola jednak wymaga to<a href="zasoby.php"><img src="../grafiki/DDCoin.png"> 30 monet</a> 
						oraz jednego wolnego człowieka.<br>
						Jeśli pole będzie użytkowane co roku, wydajność plonów może znacząco spaść.
					</p>
					<p>
						Pole uprawne na chwilę obecną nie podlega ulepszeniom.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>