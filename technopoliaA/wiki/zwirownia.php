<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row  tytulArtykulu" id="centrumRowA">
			<p class="text-center">Żwirownia</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
						<b>Żwirownia</b> to jeden z podstawowych budynków produkcyjnych dostępnych w grze. Wytwarza ona <a href="zwir.php"><img src="../grafiki/ikona_zwir.png">żwir</a>, 
						<a href="piasek.php"><img src="../grafiki/ikona_piasek.png">piasek</a>, <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamienie</a> oraz inne 
						zasoby pozyskiwane z powierzchni mapy.
					</p>
					<p style="text-indent: 3%;">
						Istotną informacją jest to że podłoże ma wpływ na ilość oraz typ wydobywanych surowców, czyli np. żwirownia umieszczona na czterech polach piasku nie będzie 
						w stanie wydobyć znacznych ilości <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamieni</a>. Część surowców, takich jak 
						<a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapienie</a> czy <a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">ruda darniowa</a>,
						nie jest widoczna na mapie, i żeby stwierdzić jej obecność, trzeba wykonać badanie powierzchni, odblokowywane dzięki 
						<a href="badania.php" id="odnosnik"><img src="../grafiki/przycisk_badania.png"  class="ikonaOdnosnika">badaniom</a>.
					</p>
					<p>
						Zasoby jakie może wydobyć żwirownia:
						<ul>
							<li><a href="piasek.php"><img src="../grafiki/ikona_piasek.png">Piasek</a></li>
							<li><a href="zwir.php"><img src="../grafiki/ikona_zwir.png">Żwir</a></li>
							<li><a href="kamien.php"><img src="../grafiki/ikona_kamien.png">Kamień</a></li>
							<li><a href="glina.php"><img src="../grafiki/ikona_glina.png">Glina</a></li>
							<li><a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">Ruda darni</a></li>
							<li><a href="wapien.php"><img src="../grafiki/ikona_wapien.png">Wapień</a></li>
							<li><a href="wegiel.php"><img src="../grafiki/ikona_wegiel.png">Węgiel</a></li>
						</ul>
						Żwirownia może pomieścić do 250 sztuk zasobów.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/zwirownia.png">
					<div class="card-body">
						<p class="card-text text-center">Żwirownia</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<div class="overflow-auto">
						<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm">
							<thead>
								<tr>
									<th class="align-middle">Zasób</th>
									<th>Poziom 1</th>
									<th>Poziom 2</th>
									<th>Poziom 3</th>
									<th>Poziom 4</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-start"><a href="zasoby.php"><img src="../grafiki/DDCoin.png">Monety</a></td>
									<td>50</td>
									<td>300</td>
									<td>500</td>
									<td>950</td>
								</tr>
								<tr>
									<td class="text-start"><a href="deski.php"><img src="../grafiki/ikona_deski.png">Deski</a></td>
									<td>-</td>
									<td>120</td>
									<td>180</td>
									<td>250</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zywica.php"><img src="../grafiki/ikona_zywica.png">Żywica</a></td>
									<td>-</td>
									<td>-</td>
									<td>40</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">Żelazo</a></td>
									<td>-</td>
									<td>-</td>
									<td>30</td>
									<td>80</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">Cegły</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>120</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row  mt-1 ps-3 g-2">
				<p class="fs-5">
				Dodatkowo do budowy żwirowni potrzeba 1 wolnego człowieka.<br>
					Wraz ze wzrostem poziomu żwirowni zwiększa się jej ilość miejsc pracy.<br>
					Wzrost zatrudnienia przekłada się liniowo na wzrost wydobycia.
				</p>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>