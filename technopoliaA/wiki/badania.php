<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
	<style>
		/*.tabelaBadan th, .tabelaBadan td, .tabelaBadan li{
			vertical-align: middle;
		}*/
	</style>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Badania</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
						<b>Badania</b> prowadzone są w <a href="warsztat.php"><img src="../grafiki/warsztat.png" class="ikonaOdnosnika">warsztacie</a> i pozwalają na 
						odblokowywanie nowych technologii, co pozwala wydobywać oraz tworzyć nowe surowce, budować nowe budynki oraz wprowadzać ulepszenia do już istniejących.
						Badania pozwalają także na korzystanie z kolejnych mechanik przewidzianych przez grę.
					</p>
					<p>
						Badania można prowadzić w siedmiu określonych dziedzinach:
						<ul>
							<li>Automatyce</li>
							<li>Budownictwie</li>
							<li>Geologii</li>
							<li>Rolnictwie</li>
							<li>Chemii</li>
							<li>Procesach</li>
							<li>Materiałach</li>
						</ul>
					</p>
					<p style="text-indent: 3%;">
						By w danej dziedzinie badania trwały, muszą być zapewnione konkretne surowce oraz przydzieleni badacze. Zatrudnienie kolejnych badaczy w danej dziedzinie 
						zwiększa szybkość badań liniowo. 
					</p>
					<p style="text-indent: 3%;">
						Badania odblokowywane są losowo, jeśli tylko spełnione są odpowiednie warunki. Dodatkowo, konkretne zdarzenia w grze (w tym badania) odblokowują 
						kolejne materiały, które badaniom możesz poddać.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid mt-1" src="../grafiki/przycisk_badania.png">
					<div class="card-body">
						<p class="card-text text-center">Grafika przycisku badań</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p style="text-indent: 3%;">
					Każda dziedzina odznacza się swoimi unikalnymi badaniami. Charakterystyczne jest także pole działań każdej z dziedzin. Przykładowo większość nowych budynków 
					odblokowuje się w badaniach budownictwa, a mechaniki związane z uprawą ziemi odblokowywane są w badaniach nad rolnictwem.
					</p>
				</div>
				<div class="col mt-1">
					<div class="row m-1 shadow-sm" id="centrumGrupa">
						<div id="naglowekKategorii" class="rounded-top d-flex flex-md-row flex-column justify-content-between align-items-center">
							<div class="text-center">
								Badania nad automatyką
							</div>
							<div>
								<button type="button" class="btn btn-success m-2" data-bs-toggle="collapse" data-bs-target="#badania_automatyka" 
									onclick="btnZmienStan(this)">Rozwiń</button>
							</div>
						</div>
						<p style="text-indent: 3%;">
							Badania nad automatyką odznaczają się wysokim progiem wejścia. Mają wpływ przede wszystkim na zwiększenie wydajności budynków.
						</p>
						<span class="mx-auto rounded-pill" id="belkaB"></span>
						<div class="m-1">
							<div id="badania_automatyka" class="collapse mx-auto overflow-x-auto">
								<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm tabelaBadan">
									<thead>
										<tr>
											<th>Badanie</th>
											<th>Wymagania</th>
											<th>Minimalne koszty</th>
											<th>Odkryte możliwości</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Powozy</td>
											<td class="w-25">
												<ul>
													<li>Zbudowanie <a href="miejsceSpotkan.php"><img src="../grafiki/animacje/miejsceSpotkan-1.png" class="ikonaOdnosnika">
													miejsca spotkań</a></li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">200 monet</a></li>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">50 drewna</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na zorganizowanie pierwszej ekspedycji, która w przypadku webowej wersji gry jest jednocześnie warunkiem wygranej.
											</td>
										</tr>
										<tr>
											<td>Piła tarczowa</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie wytopu <a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">żelaza</a> z 
														<a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">rudy darniowej</a></li>
													<li>Zbadanie gromadzenia <a href="zywica.php"><img src="../grafiki/ikona_zywica.png">żywicy</a></li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">80 monet</a></li>
													<li><a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">10 żelaza</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na kupienie w <a href="tartak.php"><img src="../grafiki/tartak.png" class="ikonaOdnosnika">tartakach</a> 
												piły tarczowej i przyspiesza produkcję <a href="deski.php"><img src="../grafiki/ikona_deski.png">desek</a>.
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row m-1 shadow-sm" id="centrumGrupa">
						<div id="naglowekKategorii" class="rounded-top d-flex flex-md-row flex-column justify-content-between align-items-center">
							<div class="text-center">
								Badania budowlane
							</div>
							<div>
								<button type="button" class="btn btn-success m-2" data-bs-toggle="collapse" data-bs-target="#badania_budowle" 
									onclick="btnZmienStan(this)">Rozwiń</button>
							</div>
						</div>
						<p style="text-indent: 3%;">
							Badania budowlane pozwalają na stawianie nowych budynków.
						</p>
						<span class="mx-auto rounded-pill" id="belkaB"></span>
						<div class="m-1">
							<div id="badania_budowle" class="collapse mx-auto overflow-x-auto">
								<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm tabelaBadan">
									<thead>
										<tr>
											<th>Badanie</th>
											<th>Wymagania</th>
											<th>Minimalne koszty</th>
											<th>Odkryte możliwości</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Piece</td>
											<td class="w-25">
												-
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">180 monet</a></li>
													<li><a href="piasek.php"><img src="../grafiki/ikona_piasek.png">4 piasku</a></li>
													<li><a href="glina.php"><img src="../grafiki/ikona_glina.png">10 gliny</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na budowanie <a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">pieców</a>, koniecznych do 
												przetwórstwa surowców.
											</td>
										</tr>
										<tr>
											<td>Studnia</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie badań hydrologicznych</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby"><img src="../grafiki/DDCoin.png">90 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wybudowanie <a href="studnia.php"><img src="../grafiki/studnia.png" class="ikonaOdnosnika">studni</a>, będącej źródłem 
												wody.
											</td>
										</tr>
										<tr>
											<td>Kopalnie</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie <a href="studnia.php"><img src="../grafiki/studnia.png" class="ikonaOdnosnika">Studni</a></li>
													<li>Opracowanie teoretycznych podstaw kopalni w dziale badań geologicznych</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">1000 monet</a></li>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">50 drewna</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na zbudowanie <a href="kopalniaGlebinowa.php"><img src="../grafiki/kopalniaGlebinowa.png" class="ikonaOdnosnika">
												kopalni głębinowej</a>, dzięki której wydobywać można zalegające pod ziemią surowce.
											</td>
										</tr>
										<tr>
											<td>Kamienice</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie Studni</li>
													<li>Zbadanie Pieców</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">1000 monet</a></li>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">50 drewna</a></li>
													<li><a href="wapien.php"><img src="../grafiki/ikona_wapien.png">30 wapienia</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wybudowanie wygodniejszych, większych mieszkań, które mogą być zaadaptowane także do innych celów.
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row m-1 shadow-sm" id="centrumGrupa">
						<div id="naglowekKategorii" class="rounded-top d-flex flex-md-row flex-column justify-content-between align-items-center">
							<div class="text-center">
								Badania geologiczne
							</div>
							<div>
								<button type="button" class="btn btn-success m-2" data-bs-toggle="collapse" data-bs-target="#badania_geologia" 
									onclick="btnZmienStan(this)">Rozwiń</button>
							</div>
						</div>
						<p style="text-indent: 3%;">
							Badania w dziedzinie geologii pozwalają na poznawanie nowych minerałów oraz ich wydobycie, a także na badanie podłoża.
						</p>
						<span class="mx-auto rounded-pill" id="belkaB"></span>
						<div class="m-1">
							<div id="badania_geologia" class="collapse mx-auto overflow-x-auto">
								<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm tabelaBadan">
									<thead>
										<tr>
											<th>Badanie</th>
											<th>Wymagania</th>
											<th>Minimalne koszty</th>
											<th>Odkryte możliwości</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Badanie żwiru</td>
											<td class="w-25">
												-
											</td>
											<td>
												<ul>
													<li><a href="zwir.php"><img src="../grafiki/ikona_zwir.png">2 żwir</a></li>
												</ul>
											</td>
											<td class="w-50">
												Odblokowuje dodanie do badań <a href="hematyt.php"><img src="../grafiki/ikona_hematyt.png">hematytu</a> i 
												<a href="piryt.php"><img src="../grafiki/ikona_piryt.png">pirytu</a>
											</td>
										</tr>
										<tr>
											<td>Badania Hydrologiczne</td>
											<td class="w-25">
												-
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">100 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na prowadzenie poszukiwań wody, a także na odkrycie złoża ropy naftowej. Dodatkowo odblokowuje 
												<a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">rudę darniową</a> jako materiał do badań
											</td>
										</tr>
										<tr>
											<td>Badanie gruntu</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie Hydrologii</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">200 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na prowadzenie badania powierzchni, które pozwala określić, co można wydobyć w 
												<a href="zwirownia.php"><img src="../grafiki/zwirownia.png">żwirowni</a>.
											</td>
										</tr>
										<tr>
											<td>Badania głębinowe</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie badań gruntu</li>
													<li>Zbadanie <a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">pieców</a></li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">300 monet</a></li>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">100 drewna</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala zbadać zasoby na dużych głębokościach, co pozwala określić, czy opłaca się wybudować kopalnię. 
											</td>
										</tr>
										<tr>
											<td>Wapień</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie badań gruntowych lub zdobycie <a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapienia</a> 
													przez zakup na rynku</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">50 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Odblokowanie surowca <a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapienia</a> do badań. 
											</td>
										</tr>
										<tr>
											<td>Koncepcja kopalni</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie badań głębinowych</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">10 monet</li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na rozpoczęcie badania kopalni przez budowniczych
											</td>
										</tr>
										<tr>
											<td>Malachit</td>
											<td class="w-25">
												<ul>
													<li>Wybudowanie kopalni lub zakup <a href="malachit.php"><img src="../grafiki/ikona_malachit.png">malachitu</a> na rynku</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">30 monet</li>
												</ul>
											</td>
											<td class="w-50">
												Odblokowuje surowiec <a href="malachit.php"><img src="../grafiki/ikona_malachit.png">malachit</a> dla badań.
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row m-1 shadow-sm" id="centrumGrupa">
						<div id="naglowekKategorii" class="rounded-top d-flex flex-md-row flex-column justify-content-between align-items-center">
							<div class="text-center">
								Badania rolincze
							</div>
							<div>
								<button type="button" class="btn btn-success m-2" data-bs-toggle="collapse" data-bs-target="#badania_rolnicze" 
									onclick="btnZmienStan(this)">Rozwiń</button>
							</div>
						</div>
						<p style="text-indent: 3%;">
							Badania rolnicze odblokowują rolnictwo.
						</p>
						<span class="mx-auto rounded-pill" id="belkaB"></span>
						<div class="m-1">
							<div id="badania_rolnicze" class="collapse mx-auto overflow-x-auto">
								<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm tabelaBadan">
									<thead>
										<tr>
											<th>Badanie</th>
											<th>Wymagania</th>
											<th>Minimalne koszty</th>
											<th>Odkryte możliwości</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Uprawa</td>
											<td class="w-25">
												-
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">20 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na postawienie pierwszych pól uprawnych.
											</td>
										</tr>
										<tr>
											<td>Zboże</td>
											<td class="w-25">
												<ul>
													<li>Pola uprawne</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">20 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na uprawę <a href="zboze.php"><img src="../grafiki/ikona_zboze.png">zboża</a>.
											</td>
										</tr>
										<tr>
											<td>Marchew</td>
											<td class="w-25">
												<ul>
													<li>Pola uprawne</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">20 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na uprawę <a href="marchew.php"><img src="../grafiki/ikona_marchew.png">marchwi</a>.
											</td>
										</tr>
										<tr>
											<td>Ziemniaki</td>
											<td class="w-25">
												<ul>
													<li>Pola uprawne</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">20 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na uprawę <a href="ziemniaki.php"><img src="../grafiki/ikona_ziemniaki.png">ziemniaków</a>.
											</td>
										</tr>
										</tr>
										<tr>
											<td>Nawożenie wapnem</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie Zboża</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">200 monet</li>
													<li><a href="wapno.php"><img src="../grafiki/ikona_wapno.png">20 wapno</li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala zwiększyć żyzność gleby przez nawożenie.
											</td>
										</tr>
										<tr>
											<td>Nasadzanie drzew</td>
											<td class="w-25">
												<ul>
													<li>ziemniaki</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">100 monet</li>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">10 drewno</li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala Nasadzać drzewa, w celu odtworzenia lasu.
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row m-1 shadow-sm" id="centrumGrupa">
						<div id="naglowekKategorii" class="rounded-top d-flex flex-md-row flex-column justify-content-between align-items-center">
							<div class="text-center">
								Badania chemiczne
							</div>
							<div>
								<button type="button" class="btn btn-success m-2" data-bs-toggle="collapse" data-bs-target="#badania_chemiczne" 
									onclick="btnZmienStan(this)">Rozwiń</button>
							</div>
						</div>
						<p style="text-indent: 3%;">
							Badania chemiczne pozwalają poznać nowe możliwości przetwarzania matariałów.
						</p>
						<span class="mx-auto rounded-pill" id="belkaB"></span>
						<div class="m-1">
							<div id="badania_chemiczne" class="collapse mx-auto overflow-x-auto">
								<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm tabelaBadan">
									<thead>
										<tr>
											<th>Badanie</th>
											<th>Wymagania</th>
											<th>Minimalne koszty</th>
											<th>Odkryte możliwości</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Węgiel drzewny</td>
											<td class="w-25">
												-
											</td>
											<td>
												<ul>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">3 drewno</li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wytwarzanie w <a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">piecach</a> 
												<a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">węgla drzewnego</a>, odblokowuje go jako surowiec badań.
											</td>
										</tr>
										<tr>
											<td>Żelazo z darni</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie węgla drzewnego</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">20 ruda darni</a></li>
													<li><a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">10 węgiel drzewny</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wytop <a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">żelaza</a> z 
												<a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">rudy darniowej</a> w 
												<a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">piecach</a> oraz odblokowuje 
												<a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">żelazo</a> do badań.
											</td>
										</tr>
										<tr>
											<td>Żelazo z pirytu</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie żelaza z darni</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="piryt.php"><img src="../grafiki/ikona_piryt.png">20 piryt</a></li>
													<li><a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">10 węgiel drzewny</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wytop <a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">żelaza</a> z 
												<a href="piryt.php"><img src="../grafiki/ikona_piryt.png">pirytu</a> w 
												<a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">piecach</a>.
											</td>
										</tr>
										<tr>
											<td>Żelazo z hematytu</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie żelaza z darni</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="hematyt.php"><img src="../grafiki/ikona_hematyt.png">20 hematyt</a></li>
													<li><a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">10 węgiel drzewny</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wytop <a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">żelaza</a> z 
												<a href="hematyt.php"><img src="../grafiki/ikona_hematyt.png">hematytu</a> w 
												<a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">piecach</a>.
											</td>
										</tr>
										<tr>
											<td>Palenie wapieni</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie węgla drzewnego</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="wapien.php"><img src="../grafiki/ikona_wapien.png">20 wapień</a></li>
													<li><a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">20 węgiel drzewny</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wypalanie <a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapieni</a> do 
												<a href="wapno.php"><img src="../grafiki/ikona_wapno.png">wapna</a>, a tym samym umożliwia jego produkcję w 
												<a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">piecach</a> i ulepszenie wielu budynków.
											</td>
										</tr>
										<tr>
											<td>Wytop miedzi</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie żelaza z darni</li>
													<li>Zbadanie malachitu</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="malachit.php"><img src="../grafiki/ikona_malachit.png">10 malachit</a></li>
													<li><a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">10 węgiel drzewny</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wytop <a href="miedz.php"><img src="../grafiki/ikona_miedz.png">miedzi</a> z 
												<a href="malachit.php"><img src="../grafiki/ikona_malachit.png">malachitu</a> w 
												<a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">piecach</a> oraz odblokowuje 
												<a href="miedz.php"><img src="../grafiki/ikona_miedz.png">miedź</a> do badań.
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row m-1 shadow-sm" id="centrumGrupa">
						<div id="naglowekKategorii" class="rounded-top d-flex flex-md-row flex-column justify-content-between align-items-center">
							<div class="text-center">
								Badania procesów
							</div>
							<div>
								<button type="button" class="btn btn-success m-2" data-bs-toggle="collapse" data-bs-target="#badania_procesy" 
									onclick="btnZmienStan(this)">Rozwiń</button>
							</div>
						</div>
						<p style="text-indent: 3%;">
							Badania procesów pozwalają poznać nowe możliwości pozyskiwania zasobów.
						</p>
						<span class="mx-auto rounded-pill" id="belkaB"></span>
						<div class="m-1">
							<div id="badania_procesy" class="collapse mx-auto overflow-x-auto">
								<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm tabelaBadan">
									<thead>
										<tr>
											<th>Badanie</th>
											<th>Wymagania</th>
											<th>Minimalne koszty</th>
											<th>Odkryte możliwości</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Pozyskiwanie żywicy</td>
											<td class="w-25">
												-
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">10 monet</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na pozyskiwanie <a href="zywica.php"><img src="../grafiki/ikona_zywica.png">żywicy</a> w 
												<a href="tartak.php"><img src="../grafiki/tartak.png" class="ikonaOdnosnika">tartakach</a>.
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row m-1 shadow-sm" id="centrumGrupa">
						<div id="naglowekKategorii" class="rounded-top d-flex flex-md-row flex-column justify-content-between align-items-center">
							<div class="text-center">
								Badania nowych materiałów
							</div>
							<div>
								<button type="button" class="btn btn-success m-2" data-bs-toggle="collapse" data-bs-target="#badania_materialy" 
									onclick="btnZmienStan(this)">Rozwiń</button>
							</div>
						</div>
						<p style="text-indent: 3%;">
							Badania nowych materiałów pozwalają pozyskać nowe materiały na drodze testów ich właściwości i reakcji.
						</p>
						<span class="mx-auto rounded-pill" id="belkaB"></span>
						<div class="m-1">
							<div id="badania_materialy" class="collapse mx-auto overflow-x-auto">
								<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm tabelaBadan">
									<thead>
										<tr>
											<th>Badanie</th>
											<th>Wymagania</th>
											<th>Minimalne koszty</th>
											<th>Odkryte możliwości</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Cegła</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie pieców</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">80 monet</a></li>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">20 drewno</a></li>
													<li><a href="glina.php"><img src="../grafiki/ikona_glina.png">20 gliny</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na wypalanie <a href="glina.php"><img src="../grafiki/ikona_glina.png">gliny</a> i produkcję 
												<a href="cegly.php"><img src="../grafiki/ikona_cegla.png">cegieł</a>.
											</td>
										</tr>
										<tr>
											<td>Cement</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie Cegły</li>
													<li>Zbadanie wypalania wapna</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">60 monet</a></li>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">20 drewno</a></li>
													<li><a href="glina.php"><img src="../grafiki/ikona_glina.png">20 gliny</a></li>
													<li><a href="wapien.php"><img src="../grafiki/ikona_wapien.png">20 wapień</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na uzyskiwanie <a href="cement.php"><img src="../grafiki/ikona_cement.png">cementu</a> poprzez wypalenie 
												<a href="glina.php"><img src="../grafiki/ikona_glina.png">gliny</a> z 
												<a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapieniem</a>.
											</td>
										</tr>
										<tr>
											<td>Szkło</td>
											<td class="w-25">
												<ul>
													<li>Zbadanie Cegły</li>
													<li>Zbadanie wypalania wapna</li>
												</ul>
											</td>
											<td>
												<ul>
													<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">60 monet</a></li>
													<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">20 drewna</a></li>
													<li><a href="piasek.php"><img src="../grafiki/ikona_piasek.png">20 piasek</a></li>
													<li><a href="wapien.php"><img src="../grafiki/ikona_wapien.png">20 wapień</a></li>
												</ul>
											</td>
											<td class="w-50">
												Pozwala na uzyskiwanie <a href="szklo.php"><img src="../grafiki/ikona_szklo.png">szkła</a> przez stopienie 
												<a href="wapno.php"><img src="../grafiki/ikona_wapno.png">wapna</a> z 
												<a href="piasek.php"><img src="../grafiki/ikona_piasek.png">piaskiem</a>.
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnZmienStan(btn){
		var stan=btn.innerHTML;
		if(btn.innerHTML=="Rozwiń"){
			btn.innerHTML="Zwiń";
		}
		else{
			btn.innerHTML="Rozwiń";
		}
	}

	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>