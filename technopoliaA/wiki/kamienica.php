<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Kamienica</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
				<p style="text-indent: 3%;">
					<b>Kamienica</b> to jeden z późniejszych budynków dostępnych w grze. Podstawową rolą kamienic jest zapewnienie nowych miejsc dla ludzi. Są one najbardziej komfortowym budynkiem, 
					który może pomieścić 10 nowych osadników. 
					</p>
					<p style="text-indent: 3%;">
					Interfejs pozwala na podobne operacje jak w przypadku <a href="szalasy.php" id="odnosnik"><img src="../grafiki/szalasy.png"  class="ikonaOdnosnika">szałasów</a>, 
					dodatkowo można także zmienić funkcję z mieszkalnej na usługową (zmienić na pasaż handlowy), o ile istnieje dostatecznie duża liczba wolnych miejsc mieszkalnych. 
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/malaKamienica.png">
					<div class="card-body">
						<p class="card-text text-center">Kamienica</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<p class="fs-5">
						Koszt budowy to:
						<ul>
							<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">4000 monet</a></li>
							<li><a href="deski.php"><img src="../grafiki/ikona_deski.png">90 deski</a></li>
							<li><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">120 cegły</a></li>
							<li><a href="cement.php"><img src="../grafiki/ikona_cement.png">100 cement</a></li>
							<li><a href="szklo.php"><img src="../grafiki/ikona_szklo.png">10 szkło</a></li>
						</ul>
					</p>
					<p class="fs-5">
						Dodatkowo do budowy kamienicy potrzeba 3 wolnych ludzi.<br>
						Obecnie kamienica nie podlega ulepszeniom.
					</p>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-3">
					<p class="fs-5">
						<b>Pasaż handlowy:</b>
					</p>
					<p class="fs-5" style="text-indent: 3%;">
					Miejsce to powstaje przez przekształcenie kamienicy. Pozwala on po stosownych decyzjach społecznych na wystawienie na sprzedaż odpowiednich materiałów oraz badanie 
					aktualnego zapotrzebowania na nie. Możliwe staje się także decydowanie o ich cenach.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>