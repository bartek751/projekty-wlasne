<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Zasoby</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p>
						<b>O zasobach w grze</b>
					</p>
					<p>
						<b>Zasoby</b> w grze technopolis są nieodłącznym elementem rozgrywki. W grze istnieje ich 26 plus 4 zasoby specjalne.<br>
						W skład zasobów specjalnych wchodzą:
						<ul>
							<li><img src="../grafiki/ludziki.png">Zasoby ludzkie (ludzie)</li>
							<li><img src="../grafiki/woda.png">Woda </li>
							<li><img src="../grafiki/piorunek.png">Energia elektryczna</li>
							<li><img src="../grafiki/DDCoin.png">Monety</li>
						</ul>
					</p>
					<p style="text-indent: 3%;">
						Pozostałe zasoby są pozyskiwane za pomocą budynków produkcyjnych takich jak <a href="tartak.php"><img src="../grafiki/tartak.png" class="ikonaOdnosnika">
						tartak</a> czy <a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnosnika">żwirownia</a>,
						<a href="rolnictwo.php"><img src="../grafiki/ikona_zboze.png" class="ikonaOdnosnika">rolnictwa</a>, 
						<a href="piece.php"><img src="../grafiki/piece.png" class="ikonaOdnosnika">pieców</a> lub za pośrednictwem 
						<a href="rynek.php"><img src="../grafiki/DDCoin.png">rynku (giełdy towarów)</a>.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaMalegoObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/ikona_deski.png">
					<div class="card-body">
						<p class="card-text text-center">ikona desek</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Lista zasobów</b>
					</p>
					<p>
						Poniżej znajduje się pełna lista zasobów wraz z odnośnikami do nich:
						<ul>
							<li><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">Cegły</a></li>
							<li><a href="cement.php"><img src="../grafiki/ikona_cement.png">Cement</a></li>
							<li><a href="deski.php"><img src="../grafiki/ikona_deski.png">Deski</a></li>
							<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">Drewno</a></li>
							<li><a href="glina.php"><img src="../grafiki/ikona_glina.png">Glina</a></li>
							<li><a href="halit.php"><img src="../grafiki/ikona_halit.png">Halit (sól)</a></li>
							<li><a href="hematyt.php"><img src="../grafiki/ikona_hematyt.png">Hematyt</a></li>
							<li><a href="kamien.php"><img src="../grafiki/ikona_kamien.png">Kamień</a></li>
							<li><a href="kwarc.php"><img src="../grafiki/ikona_kwarc.png">Kwarc</a></li>
							<li><a href="malachit.php"><img src="../grafiki/ikona_malachit.png">Malachit</a></li>
							<li><a href="marchew.php"><img src="../grafiki/ikona_marchew.png">Marchew</a></li>
							<li><a href="miedz.php"><img src="../grafiki/ikona_miedz.png">Miedź</a></li>
							<li><a href="piasek.php"><img src="../grafiki/ikona_piasek.png">Piasek</a></li>
							<li><a href="piryt.php"><img src="../grafiki/ikona_piryt.png">Piryt</a></li>
							<li><a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">Ruda darni</a></li>
							<li><a href="smola.php"><img src="../grafiki/ikona_smola.png">Smoła</a></li>
							<li><a href="szklo.php"><img src="../grafiki/ikona_szklo.png">Szkło</a></li>
							<li><a href="wapien.php"><img src="../grafiki/ikona_wapien.png">Wapień</a></li>
							<li><a href="wapno.php"><img src="../grafiki/ikona_wapno.png">Wapno</a></li>
							<li><a href="wegiel.php"><img src="../grafiki/ikona_wegiel.png">Węgiel</a></li>
							<li><a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">Węgiel drzewny</a></li>
							<li><a href="zboze.php"><img src="../grafiki/ikona_zboze.png">Zboże</a></li>
							<li><a href="ziemniaki.php"><img src="../grafiki/ikona_ziemniaki.png">Ziemniaki</a></li>
							<li><a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">Żelazo</a></li>
							<li><a href="zwir.php"><img src="../grafiki/ikona_zwir.png">Żwir</a></li>
							<li><a href="zywica.php"><img src="../grafiki/ikona_zywica.png">Żywica</a></li>
						</ul>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>