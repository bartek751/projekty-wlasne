<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--<link rel="stylesheet" href="https://www.markuptag.com/bootstrap/5/css/bootstrap.min.css"/> old link for bootstrap-->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
	<div class="row" id="pasekGorny">
			<?php include "naglowek.php"; ?>
	</div>

	<div class="row" id="srodekStrony">
		<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2 container">
		</div>
		
		<div class="col- col-lg-8 container-fluid" id="kolCentrum">

			<div class="row d-flex flex-row g-1 mt-1" id="centrumRowA">
				<div class="col-xs-12 shadow-sm" id="centrumGrupa">
					<p id="tytul" class="m-1 mx-auto text-center">Witaj na technopolis - wiki</p>
					<input type="button" class="btn btn-success mx-auto m-2 " id="przycisk" value="Powrót do technopolis" onclick="btnDoIndexu()">
					<p id="nowyGracz" class="m-1 text-center">
						Jeśli jesteś nowy/wa w tej grze to możesz się zapoznać z <a href="pierwszeKroki.php"> pierwszymi krokami</a>, które pokazują jak zacząć grać.
					</p>
				</div>
			</div>

			<div class="row column-gap-2 mt-1 g-1" id="centrumRowB">
				
				<div class="col-12 col-lg-7 shadow-sm p-0" id="centrumGrupa">
					<div id="naglowekKategorii" class="clearfix rounded-top container-fluid">
						<div class="float-start ms-4">
							Budynki:
						</div>
						<div class="float-end me-4">
							<img src="../grafiki/malaKamienica.png"></img>
						</div>
					</div>
					<div class="m-1">
						<div class="row">
							<div class="col-12 col-sm-6 col-md col-lg-4">
								<ul>
									<li> <a href="szalasy.php"><img src="../grafiki/szalasy.png">Szałasy</a></li>
									<li> <a href="miejsceSpotkan.php"><img src="../grafiki/animacje/miejsceSpotkan-1.png">Miejsce spotkań</a></li>
									<li> <a href="studnia.php"><img src="../grafiki/studnia.png" style="margin-left: 10px; margin-right: 10px;">Studnia</a></li>
									<li> <a href="kamienica.php"><img src="../grafiki/malaKamienica.png">Kamienica</a></li>
								</ul>
							</div>
							<div class="col-12 col-sm-6 col-md col-lg-4">
								<ul>
									<li> <a href="zwirownia.php"><img src="../grafiki/zwirownia.png">Żwirownia</a></li>
									<li> <a href="tartak.php"><img src="../grafiki/tartak.png">Tartak</a></li>
									<li> <a href="kopalniaGlebinowa.php"><img src="../grafiki/kopalniaGlebinowa.png">Kopalnia głębinowa</a></li>
								</ul>
							</div>
							<div class="col-12 col-sm-6 col-md col-lg-4">
								<ul>
									<li> <a href="magazyn.php"><img src="../grafiki/magazyn.png">Magazyn</a></li>
									<li> <a href="warsztat.php"><img src="../grafiki/warsztat.png">Warsztat</a></li>
									<li> <a href="piece.php"><img src="../grafiki/piece.png">Piece</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-12 col-md col-lg shadow-sm p-0" id="centrumGrupa">
					<div id="naglowekKategorii" class="clearfix rounded-top">
						<div class="float-start ms-4">
							Mechaniki:
						</div>
						<div class="float-end me-4">
							<img src="../grafiki/zebatka.png" style="width: 40px; height: 40px;"></img>
						</div>
					</div>
					<div class="m-1">
						<div class="row">
							<div class="col">
								<ul>
									<li> <a href="badania.php"><img src="../grafiki/przycisk_badania.png" class="ikonaListy">Badania</a></li>
									<li> <a href="rynek.php"><img src="../grafiki/DDCoin.png" class="ikonaListy">Rynek</a></li>
									<li> <a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png" class="ikonaListy">Zadowolenie</a></li>
									<li> <a href="mapa.php"><img src="../grafiki/kamienie.png" class="ikonaListy">Mapa</a></li>
								</ul>
							</div>
							<div class="col">
								<ul>
									<li> <a href="lasy.php"><img src="../grafiki/drzewo2.png" class="ikonaListy">Lasy</a></li>
									<li> <a href="rolnictwo.php"><img src="../grafiki/ikona_zboze.png" class="ikonaListy">Rolnictwo</a></li>
									<li> <a href="przychodyWydatki.php"><img src="../grafiki/DDCoin.png" class="ikonaListy">Przychody i wydatki</a></li>
									<li> <a href="zasoby.php"><img src="../grafiki/ikona_wegiel.png" class="ikonaListy">Zasoby</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mt-2">
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
			<p style="font-size: 1.5em;" class="text-center">
				Zmiany w grze i na stronie mogą się pojawić w dowolnej chwili bez zapowiedzi.<br>
			</p>
			</div>
		</div>

		<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		</div>
	</div>
</div>
</body>
<!--<script src="https://www.markuptag.com/bootstrap/5/js/bootstrap.bundle.min.js"></script> old link for bootstrap-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();

	// technopolis wiki - ver.: 1.1
</script>
</html>