<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Warsztat</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
						<b>Warsztat</b> to jeden z podstawowych budynków dostępnych w grze. Jest to miejsce, w którym twoi ludzie mogą przeprowadzać 
						<a href="badania.php"><img src="../grafiki/przycisk_badania.png" class="ikonaOdnosnika">badania </a>i dokonywać nowych odkryć.
					</p>
					<p style="text-indent: 3%;">
						Aby można było prawadzić <a href="badania.php"><img src="../grafiki/przycisk_badania.png" class="ikonaOdnosnika">badania</a> należy wejść w okno warsztatu 
						następnie przycisk badań i przekazać na nie <a href="zasoby.php">zasoby</a> oraz przypisać pracownika warsztatu do określonego działu badań.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/warsztat.png">
					<div class="card-body">
						<p class="card-text text-center">Warsztat poziom 1</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<div class="overflow-auto">
						<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm">
							<thead>
								<tr>
									<th class="align-middle">Zasób</th>
									<th>Poziom 1</th>
									<th>Poziom 2</th>
									<th>Poziom 3</th>
									<th>Poziom 4</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-start"><a href="zasoby.php"><img src="../grafiki/DDCoin.png">Monety</a></td>
									<td>800</td>
									<td>700</td>
									<td>3600</td>
									<td>9000</td>
								</tr>
								<tr>
									<td class="text-start"><a href="kamien.php"><img src="../grafiki/ikona_kamien.png">Kamień</a></td>
									<td>250</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zwir.php"><img src="../grafiki/ikona_zwir.png">Żwir</a></td>
									<td>250</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="deski.php"><img src="../grafiki/ikona_deski.png">Deski</a></td>
									<td>140</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">Cegły</a></td>
									<td>-</td>
									<td>200</td>
									<td>600</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="wapno.php"><img src="../grafiki/ikona_wapno.png">Wapno</a></td>
									<td>-</td>
									<td>100</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zywica.php"><img src="../grafiki/ikona_zywica.png">Żywica</a></td>
									<td>-</td>
									<td>50</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cement.php"><img src="../grafiki/ikona_cement.png">Cement</a></td>
									<td>-</td>
									<td>-</td>
									<td>90</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="szklo.php"><img src="../grafiki/ikona_szklo.png">Szkło</a></td>
									<td>-</td>
									<td>-</td>
									<td>80</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="miedz.php"><img src="../grafiki/ikona_miedz.png">Miedź</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>90</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">Żelazo</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>170</td>
								</tr>
								<tr>
									<td class="text-start"><a href="smola.php"><img src="../grafiki/ikona_smola.png">Smoła</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>200</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<p class="fs-5">
					Dodatkowo do budowy warsztatu potrzeba 2 wolnych ludzi.<br>
					Wraz ze wzrostem poziomu warsztatu zwiększa się ilość dostępnych badaczy.
				</p>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>