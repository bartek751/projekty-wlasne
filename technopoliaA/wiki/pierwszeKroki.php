<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Pierwsze kroki</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row mt-1 ps-3 g-2 fs-5">
				<div class="col m-1">
					<p style="text-indent: 3%;">
					A więc jesteś nowy/wa w grze i nie wiesz od czego zacząć? Ten artykuł jest przygotowany właśnie myślą o osobach takich jak ty. Postaram sie aby był on możliwie 
					krótki i treściwy. Zacznijmy więc od podstawy czyli wytłumaczenia co gdzie się znajduje na ekranie po uruchomieniu gry ("co ja paczam").
					</p>
				</div>
			</div>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
			<div class="row mt-1 ps-3 fs-2 text-center">
				<p>Interfejs</p>
			</div>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
			<div class="row mt-1 ps-3 fs-5">
				<p class="text-center mt-1">
					<img src="grafiki/oknoGry.png" class="mx-auto mw-100 mh-100"><br>
					Ekran główny gry.
				</p>
				<p style="text-indent: 3%;">
					Powyższa grafika przedstawia ekran główny gry, czyli to co się wam ukaże zaraz po jej uruchomieniu. Zaczynając jej opis od góry pierwsze co widzimy to pasek z 
					grafikami i cyferkami. Jest to pasek z kilkoma wyszczególnionymi zasobami specjalnymi oraz 
					<a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png">zadowoleniem</a>. 
				</p>
				<p style="text-indent: 3%;">
					Patrząc na 
					ten pasek od lewej piersze co jest nam ukazane to <img src="../grafiki/ludziki.png"><b>ludzie</b> do naszej dyspozycji (zasoby ludzkie). Konkretniej to są tam 
					trzy cyfry oddzielone "/". Cyfry te to odpowienio: 0 - Ludzie pracujący / 5 - ludzie bezrobotni / 5 - miejsca na ludzi. A więc wygląda to na użyteczne
					informacje w przeciwieństwie do dwóch kolejnych czyli <img src="../grafiki/woda.png"><b>wody</b> i <img src="../grafiki/piorunek.png"><b>prądu</b> (energii 
					elektrycznej). Te dwa wskaźniki są obecnie nieistotne więc możesz je śmiało zignorować. :)
				</p>
				<p style="text-indent: 3%;">
					Dalej mamy wskaźnik <a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png">zadowolenia</a>. Czerwona strzałka obok niego oznacza że zadowolenie 
					spada i warto by się tym zająć bo jeśli buźka przedstawiająca jego stan będzie czerwona to nasi ludzie będą chcieli opuścić naszą kolonię i będziemy 
					zmierzać ku porażce. Zaraz opowiem co z tym zrobić. Przejdźmy jednak dalej czyli do <img src="../grafiki/DDCoin.png"><b>monet</b> (w sumie to na razie tyle w 
					temacie monet) oraz <b>obecnego miesiąca</b> widniejącego na prawo od górnego paska. Pojedyńczy miesiąc trwa około 2 minuty czasu rzeczywistego i jego zmiana 
					wpływa na <b>rolnictwo</b> (tak mamy rolnictwo w grze) oraz na potrzebę posiadania opału przez mieszkańców w czasie chłodniejszych miesięcy.
				</p>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="row mt-1 ps-3 fs-2 text-center">
					<p>Zadowolenie</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<p class="text-center mt-1">
					<img src="grafiki/spadekZadowolenia.png" class="mx-auto mw-100 mh-100"><br>
					Zadowolenie spada.<br>
					To niedobrze
				</p>
				<p style="text-indent: 3%;">
					Tak więc przebrnijmy szybko przez temat <a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png">zadowolenia</a>. Jego stan jest ściśle zwiazany z tym co chcą nasi ludzie. 
					Jak to sprawdzić? Otóż nie da się. :) Trzeba z nimi postępować jak z małym dzieckiem. Trzeba się domyśleć czego mogą chcieć i im to dać lub też przeczytać na 
					wiki artykuł o <a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png">zadowoleniu</a> i już wszysko będzie jasne.
				</p>
				<p style="text-indent: 3%;">
					Tak więc do rzeczy. Ludzie potrzebują między innymi <b>miejsc pracy</b> żeby utrzymać niski wskaźnik bezrobocia oraz <img src="../grafiki/DDCoin.png">monet na 
					<b>wypłaty</b> dla nich. Trzeba więc zbudować miejsca pracy.
				</p>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="row mt-1 ps-3 fs-2 text-center">
					<p>Menu budowy</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<p style="text-indent: 3%;">
					Proponował bym zacząć od 
					<a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnosnika">Żwirowni</a> zbudowanej na kamieniach oraz 
					<a href="tartak.php"><img src="../grafiki/tartak.png" class="ikonaOdnosnika">Tartaku</a>, który można zbudować tylko jako przyległy do drzew. Dlaczego 
					<a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnosnika">żwirownia</a> na kamieniach?
					Ponieważ jeśli ją tam zbudujemy to będziemy wydobywać <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamienie</a>, które zdecydowanie nam się 
					przydadzą.
				</p>
				<p style="text-indent: 3%;">
					Klikamy LPM (Lewy Przycisk Myszy dla niewtajemniczonych) na miejscu <b>przycisku budowy</b> w lewym dolnym rogu okna gry (tak, to ten domek). Następnie 
					wybieramy kursorem (wskaźnikiem myszki) budynek, który chcemy zbudować, klikamy LPM na jego nazwę i... nie działa. Dlaczego? Kiedy najedziemy na nazwę 
					jakiegoś budynku kursorem to da sie zauważyć, że na dolnym pasku pojawiły się jakieś informacje. Są to koszty budowy wybranego budynku oraz informacja w 
					nawiasie ile danego zasobu obecnie posiadamy.
				</p>
				<p style="text-indent: 3%;">
					Ale zaraz, zaraz, panie twórco, mnie na to nie stać. Owszem, jest to zamierzony zabieg. Gracz ma często korzystać z 
					<a href="rynek.php"><img src="../grafiki/DDCoin.png">Rynku (giełdy towarów)</a> i to właśnie z niego pozyskamy pierwsze materiały do budowy tych budynków. Przejdźmy 
					zatem do okna rynku, które można otworzyć klikając jego przycisk w lewym dolnym rogu okna gry (tak, to ten wózek sklepowy).
				</p>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="row mt-1 ps-3 fs-2 text-center">
					<p>Rynek (giełda towarów)</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<p class="text-center mt-1">
					<img src="grafiki/oknoHandel.png" class="mx-auto mw-100 mh-100"><br>
					Okno handlu.
				</p>
				<p style="text-indent: 3%;">
					Za długi ten artykuł, ale wytrzymaj jeszcze pare minut albowiem powoli zbliżamy się do jego końca. Powyżej widać okno handlu. Po lewej są towary, które możemy 
					kupić (za nasze <img src="../grafiki/DDCoin.png">monety oczywiście) a po prawej to co mamy i możemy sprzedać. Polecam zakupić trochę 
					<a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> oraz <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamienia</a> 
					przy czym warto kupić więcej <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> niż 
					<a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamienia</a>.
				</p>
				<p style="text-indent: 3%;">
					<b>Cenę</b> danego towaru widzimy zaraz obok jego grafiki, czyli w tym przypadku <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamień</a> 
					kosztuje 3G a <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewno</a> 2G. <b>G</b> to skrót od "Gold" czyli złoto ale w tematyce growej chodzi 
					o <img src="../grafiki/DDCoin.png"><b>monety</b>. Po prawej od ceny są przyciski dzięki którym możemy zakupić dany towar w danej ilości czyli te z cyferki 1, 
					10 i 100. Łatwo przeliczyć że kupno 100 <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamienia</a> i 
					200 <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> będzie nas kosztować 700 <img src="../grafiki/DDCoin.png">monet.
					Polecał bym zakupić właśnie te towary we właśnie tej ilości. Warto tutaj również dodać iż ceny towarów <b>mogą się u was różnić</b> a nawet pewne jest, 
					że będą się one <b>zmieniać w czasie gry</b>, wiadomo, różnice rynkowe. Teraz zamykamy okno klikając LPM (Lewy Przycisk Myszy dla niewtajemniczonych) poza nie 
					i próbujemy budować.
				</p>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="row mt-1 ps-3 fs-2 text-center">
					<p>Ostatnie kroki początku</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<p style="text-indent: 3%;">
					Ponawiamy procedurę budowy budynku. LPM na menu budowy, następnie wybieramy budynek i tym razem mapa przybiera odcienie zieleni i czerwieni a do naszego kursora
					przyczepiony jest budynek, który wybraliśmy. Zielony obszar to miejsce gdzie można zbudować dany budynek - czerwony obszar analogicznie jest miejscem gdzie budować
					danego budynku nie możemy.
				</p>
				<p style="text-indent: 3%;">
					Proponuję zatem zbudować dwie <a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnosnika">żwirownie</a> i jeden 
					<a href="tartak.php"><img src="../grafiki/tartak.png" class="ikonaOdnosnika">tartak</a>. Teraz możemy sprawdzić czy działają wchodząc do okna rynku i tam 
					możemy zobaczyć że nagle mamy więcej zasobów niż to co kupiliśmy. Kolejną rzeczą jaką proponuje zbudować są 
					<a href="szalasy.php"><img src="../grafiki/szalasy.png" class="ikonaOdnosnika">szałasy</a>. Dadzą nam 2 miejsca na <img src="../grafiki/ludziki.png">ludzi. 
					Jeśli są miejsca na nowych <img src="../grafiki/ludziki.png">ludzi i <a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png">zadowolenie</a> jest na 
					odpowiednim poziomie to możemy się spodziewać, że po pewnym czasie przybędą do nas nowi bezrobotni. Przypominam tylko żeby mieć trochę 
					<img src="../grafiki/DDCoin.png">monet na zapas żeby móc zapewnić wypłaty dla pracowników. Domyślnie jeden budynek produkcyjny płaci 
					10 <img src="../grafiki/DDCoin.png">monet dla każdego swojego pracownika.
				</p>
				<p style="text-indent: 3%;">
					Jeszcze jeden, ostatni bloczek tekstu. <b>Do budowy budynków</b> potrzeba <b>zazwyczaj</b> co najmniej <b>jednego bezrobotnego</b> człowieka, w końcu ktoś to musi 
					zbudować. Jeśli przekroczymy 10 posiadanych ludzi to nie będą oni się już w stanie sami wyżywić i należy im <b>zakupić coś do jedzenia</b> z 
					<a href="rynek.php"><img src="../grafiki/DDCoin.png">rynku</a>. Możesz poszerzyć swoje możliwości produkcyjne (i nie tylko) przeprowadzając 
					<a href="badania.php"><img src="../grafiki/przycisk_badania.png" class="ikonaOdnosnika">badania</a> w 
					<a href="warsztat.php"><img src="../grafiki/warsztat.png" class="ikonaOdnosnika">warsztacie</a>. 
					<a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png">Zadowolenie</a> możesz też podnieść programami socjalnymi w oknie zarządzania 
					<a href="szalasy.php"><img src="../grafiki/szalasy.png" class="ikonaOdnosnika">szałasami</a> oraz w
					<a href="miejsceSpotkan.php"><img src="../grafiki/animacje/miejsceSpotkan-1.png" class="ikonaOdnosnika">miejscu spotkań</a> jednak aby móc z nich skorzystać potrzebujesz albo 
					<img src="../grafiki/DDCoin.png">monet albo <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> albo czegoś innego w zależności od programu 
					socjalnego.
				</p>
				<p>
					Tak wiec, powodzenia. Klikaj, odkrywaj i sprawdzaj co jak działa, chyba właśnie na tym polega granie. :)
				</p>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>