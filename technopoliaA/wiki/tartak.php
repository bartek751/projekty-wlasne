<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Tartak</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
						<b>Tartak</b> to jeden z podstawowych budynków produkcyjnych dostępnych w grze. Wytwarza on <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewno</a>
						z przyległych do niego drzew oraz może je przetwarzać na <a href="deski.php"><img src="../grafiki/ikona_deski.png">deski</a>. Po odpowiednich 
						<a href="badania.php"><img src="../grafiki/przycisk_badania.png" class="ikonaOdnosnika">badaniach</a> może on również pozyskiwać 
						<a href="zywica.php"><img src="../grafiki/ikona_zywica.png">żywicę</a>. To co w danym momencie wytwarza tartak moża ustawić w jego oknie. Warto dodać iż tartak 
						w trakcie pozyskiwania drewna przeprowadza wycinkę przyległych drzew. Z tego powodu po pewnym czasie może on służyć tylko do produkcji 
						<a href="deski.php"><img src="../grafiki/ikona_deski.png">desek</a>.
					</p>
					<p>
						Do wydobycia <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> oraz <a href="zywica.php"><img src="../grafiki/ikona_zywica.png">żywicy</a>
						tartak musi być przyległy do drzew.<br>
						Prędkość wydobycia <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> jest równa ilości pracowników na sekundę.<br>
						Wydobycie_drewna = ilość_pracowników * 1_sekunda
					</p>
					<p>
						Jeśli chodzi o <a href="zywica.php"><img src="../grafiki/ikona_zywica.png">żywicę</a> to im więcej przyległych drzew tym wydajniej ją pozyskuje.<br>
						Może on przechować 100 sztuk zasobów.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/tartak.png">
					<div class="card-body">
						<p class="card-text text-center">Tartak poziom 1</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-3">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<div class="overflow-auto">
						<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm">
							<thead>
								<tr>
									<th class="align-middle">Zasób</th>
									<th>Poziom 1</th>
									<th>Poziom 2</th>
									<th>Poziom 3</th>
									<th>Poziom 4</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-start"><a href="zasoby.php"><img src="../grafiki/DDCoin.png">Monety</a></td>
									<td>80</td>
									<td>250</td>
									<td>600</td>
									<td>900</td>
								</tr>
								<tr>
									<td class="text-start"><a href="kamien.php"><img src="../grafiki/ikona_kamien.png">Kamień</a></td>
									<td>40</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">Drewno</a></td>
									<td>30</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="deski.php"><img src="../grafiki/ikona_deski.png">Deski</a></td>
									<td>-</td>
									<td>80</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zwir.php"><img src="../grafiki/ikona_zwir.png">Żwir</a></td>
									<td>-</td>
									<td>50</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">Żelazo</a></td>
									<td>-</td>
									<td>-</td>
									<td>20</td>
									<td>100</td>
								</tr>
								<tr>
									<td class="text-start"><a href="wapno.php"><img src="../grafiki/ikona_wapno.png">Wapno</a></td>
									<td>-</td>
									<td>-</td>
									<td>30</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">Cegły</a></td>
									<td>-</td>
									<td>-</td>
									<td>100</td>
									<td>250</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cement.php"><img src="../grafiki/ikona_cement.png">Cement</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>120</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<p class="fs-5">
				Dodatkowo do budowy tartaku potrzeba 1 wolnego człowieka.<br>
					Wraz ze wzrostem poziomu tartaku zwiększa się jego ilość miejsc pracy.
				</p>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>