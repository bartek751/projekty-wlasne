<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Zadowolenie</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p>
						<b>Opis ogólny zadowolenia</b>
					</p>
					<p style="text-indent: 3%;">
						<b>Zadowolenie</b> społeczeństwa wacha się w granicach od 1 do 100 i zależy od wielu czynników i wpływa bezpośrednio na przyrost populacji naszego miasta. 
						By technopolia mogła się rozwijać, potrzebni są ludzie. By Ci chcieli dołączać do naszej społeczności, potrzebne jest odpowiednio wysokie szczęście 
						(35 lub więcej) oraz wolne miejsca. Prawdopodobieństwo pojawienia się nowych ludzi rośnie wraz ze zwrostem zadowolenia oraz ilości wolnych miejsc. Okres, 
						między którym może dołączyć do nas nowy osadnik, wynosi jedną sekundę. Należy jednak pamiętać, że ludzie mogą także odejść z powodu niskiego poziomu 
						zadowolenia.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid mt-1" src="grafiki/spadekZadowolenia.png">
					<div class="card-body">
						<p class="card-text text-center">Wskaźnik zadowolenia w grze</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Wzrost zadowolenia</b>
					</p>
					<p style="text-indent: 3%;">
						Za <b>wzrost zadowolenia</b> odpowiadają zarówno działania gracza jak i zmiany cykliczne gry. Jeden cykl gry to jeden miesiąc i trwa w rzeczywistości 
						120 sekund.
					</p>
					<p>
						Do działań gracza zalicza się:
						<ul>
							<li>Organizację imprez (różnych, w zależności od miesiąca), podnosi to poziom szczęścia o wartość od 4 do 6. Należy pamiętać, że imprezy można 
								organizować co dwa cykle.</li>
							<li>Programy socjalne, w <a href="szalasy.php"><img src="../grafiki/szalasy.png" class="ikonaOdnosnika">budynkach mieszkalnych</a> 
								możliwe jest przeznaczenie środków na wsparcie społeczeństwa, co skutkuje wzrostem morale zależnego od konkretnego programu.</li>
							<li>Zmiany społeczne dokonywane w <a href="miejsceSpotkan.php"><img src="../grafiki/animacje/miejsceSpotkan-1.png" class="ikonaOdnosnika">miesjcu 
								spotkań</a>. Należy do nich między innymi podniesienie pensji. Należy jednak pamiętać, że jest to zmiana trwała i przyczynia się do inflacji.</li>
						</ul>
						Do zmian cyklicznych zalicza się:<br><br>
						<ul>
							<li>Wzrost zadowolenia wynikający z dostępności <a href="zasoby.php">zasobów</a> takich jak żywność (przez cały rok) czy też 
								<a href="wegiel.php"><img src="../grafiki/ikona_wegiel.png">węgiel</a> i <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewno</a> 
								(zimą).</li>
							<li>Wzrost zadowolenia wynikający z niskiego bezrobocia (zatrudnione więcej niż 7/8 społeczeństwa).</li>
							<li>Zadowolenie wynikające z budynków określonej jakości (im więcej tym lepiej, do 20 cyklu zadowolenie podnoszą lepianki, do 40 domy z drewna, 
								do 60 domy z kamienia, potem tylko i wyłącznie <a href="kamienica.php"><img src="../grafiki/malaKamienica.png" class="ikonaOdnosnika">
								kamienice</a>).</li>
						</ul>
					</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Spadek zadowolenia</b>
					</p>
					<p style="text-indent: 3%;">
						Za <b>spadek zadowolenia</b> odpowiadają zmiany cykliczne gry oraz zmiany zachodzące w "interwale właściwym". Jeden cykl gry to jeden miesiąc i trwa 
						w rzeczywistości 120 sekund, natomiast "interwał właściwy" trwa 1 sekundę czasu rzeczywistego.
					</p>
					<p>
						Do czynników powodujących pogorszenie się nastrojów społecznych należą:
						<ul>
							<li>Niedostatek odpowiednich materiałów ((żywności, jeśli ludności powyżej 10) oraz 
								<a href="wegiel.php"><img src="../grafiki/ikona_wegiel.png">węgla</a> i <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a>
								(jeśli miesiąc listopad, grudzień, styczeń, luty, marzec)).</li>
							<li>Wysokie bezrobocie, zatrudnienie na poziomie niższym niż 5/8.</li>
							<li>Szybki spadek zadowolenia, jeśli zasoby pieniężne nie starczają na wypłaty (uruchamia się jednocześnie mechanizm bankructwa).</li>
							<li>Zła jakość budynków (korzystanie z szałasów po 80 cylku i lepianek po 140 cyklu).</li>
							<li>Nieobecność niektórych budynków w późniejszych etapach gry (<a href="warsztat.php"><img src="../grafiki/warsztat.png" class="ikonaOdnosnika">
								warsztatu</a> i <a href="miejsceSpotkan.php"><img src="../grafiki/animacje/miejsceSpotkan-1.png" class="ikonaOdnosnika">miejsca spotkań</a>
								po 40 cyklu, <a href="studnia.php"><img src="../grafiki/studnia.png" class="ikonaOdnosnika">studni</a> po 100 cyklu).</li>
						</ul>
					</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Reprezentacja graficzna zadowolenia</b>
					</p>
					<p style="text-indent: 3%;">
						Do reprezentacji obecnego zakresu wartości zadowolenia używane są poniższe grafiki:
					</p>
					<div class="row container-fluid column-gap-5 mt-1 mx-auto px-3 g-2">
						<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
							<img class="card-img-top mx-auto d-block img-fluid mt-1" src="../grafiki/zadowolenie-1.png">
							<div class="card-body">
								<p class="card-text text-center">100 - 76</p>
							</div>
						</div>
						<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
							<img class="card-img-top mx-auto d-block img-fluid mt-1" src="../grafiki/zadowolenie-2.png">
							<div class="card-body">
								<p class="card-text text-center">75 - 55</p>
							</div>
						</div>
						<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
							<img class="card-img-top mx-auto d-block img-fluid mt-1" src="../grafiki/zadowolenie-3.png">
							<div class="card-body">
								<p class="card-text text-center">54 - 41</p>
							</div>
						</div>
						<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
							<img class="card-img-top mx-auto d-block img-fluid mt-1" src="../grafiki/zadowolenie-4.png">
							<div class="card-body">
								<p class="card-text text-center">40 - 30</p>
							</div>
						</div>
						<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
							<img class="card-img-top mx-auto d-block img-fluid mt-1" src="../grafiki/zadowolenie-5.png">
							<div class="card-body">
								<p class="card-text text-center">29 - 11</p>
							</div>
						</div>
						<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
							<img class="card-img-top mx-auto d-block img-fluid mt-1" src="../grafiki/zadowolenie-6.png">
							<div class="card-body">
								<p class="card-text text-center">10 - 1</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>