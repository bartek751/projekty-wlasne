<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Szałasy</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
					<b>Szałasy</b> to jeden z podstawowych budynków dostępnych w grze. Jest to miejsce, w którym żyją twoi ludzie. Szałasy i ich późniejsze ulepszenia 
					tworzą wolne miejsa mieszkalne dzięki czemu przyczyniają się do wzrostu dostępnych ludzi.
					</p>
					<p style="text-indent: 3%;">
					Nowi ludzie będą się pojawiać jeśli będą mieli wolne miejsca mieszkalne oraz <a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png">
					poziom zadowolenia </a> będzie co najmniej średni. Po spełnieniu tych warunków nowi przybysze mogą się pojawić w dowolnym momencie.
					Przy większych populacjach warto zaopatrzeć się w jedzenie oraz <a href="miejsceSpotkan.php"><img src="../grafiki/animacje/miejsceSpotkan-1.png" class="ikonaOdnosnika">
					miesce spotkań </a> aby zadbać o <a href="zadowolenie.php"><img src="../grafiki/zadowolenie-1.png"> zadowolenie</a>.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/animacje/szalas-1.png">
					<div class="card-body">
						<p class="card-text text-center">Szałasy poziom 1</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<div class="overflow-auto">
						<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm">
							<thead>
								<tr>
									<th class="align-middle">Zasób</th>
									<th>Poziom 1</th>
									<th>Poziom 2</th>
									<th>Poziom 3</th>
									<th>Poziom 4</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-start"><a href="zasoby.php"><img src="../grafiki/DDCoin.png" class="ikonaOdnosnika">Monety</a></td>
									<td>20</td>
									<td>100</td>
									<td>400</td>
									<td>800</td>
								</tr>
								<tr>
									<td class="text-start"><a href="kamien.php"><img src="../grafiki/ikona_kamien.png">Kamień</a></td>
									<td>20</td>
									<td>-</td>
									<td>120</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">Drewno</a></td>
									<td>40</td>
									<td>80</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="glina.php"><img src="../grafiki/ikona_glina.png">Glina</a></td>
									<td>-</td>
									<td>100</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zwir.php"><img src="../grafiki/ikona_zwir.png">Żwir</a></td>
									<td>-</td>
									<td>30</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="deski.php"><img src="../grafiki/ikona_deski.png">Deski</a></td>
									<td>-</td>
									<td>-</td>
									<td>90</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">Cegły</a></td>
									<td>-</td>
									<td>-</td>
									<td>30</td>
									<td>190</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cement.php"><img src="../grafiki/ikona_cement.png">Cement</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>90</td>
								</tr>
								<tr>
									<td class="text-start"><a href="szklo.php"><img src="../grafiki/ikona_szklo.png">Szkło</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>30</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<p class="fs-5">
					Dodatkowo do budowy szałasów potrzeba 1 wolnego człowieka.<br>
					Wraz ze wzrostem poziomu szałasów zwiększa się ilość miejsc mieszkalnych.
				</p>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>