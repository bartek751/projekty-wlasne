<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
		<p class="text-center">Miejsce spotkań</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
					<b>Miejsce spotkań</b> pozwala na rozwój społeczny. Wchodząc w okno miejsca spotkań gracz ma możliwość wpływania na swoje społeczeństwo. Dodatkowo mając 
					więcej niż 10 ludzi warto się zaopatrzeć w miejsce spotań celem poprawy <img src="../grafiki/zadowolenie-1.png"><a href="zadowolenie.php">poziomu zadowolenia</a>.<br>
					Akcje jakie może podjąć gracz w miejscu spotań:
					<ul>
						<li>Podniesienie pensji wszystkim pracownikom (podnosi wynagrodzenia w miejsach pracy)</li>
						<li>Organizacja festynów (podnosi zadowolenie ; dostępne raz na 2 miesiące)</li>
						<li>Organizacja wyprawy (pozwala na organizację wyprawy, która w wersji webowej gry jest jednoznaczna z jej ukończemiem)</li>
					</ul>
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/animacje/miejsceSpotkan-1.png">
					<div class="card-body">
						<p class="card-text text-center">Miejsce spotkań</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<p class="fs-5">
						Koszt budowy to:
						<ul>
							<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">400 monet</a></li>
							<li><a href="deski.php"><img src="../grafiki/ikona_deski.png">50 desek</a></li>
							<li><a href="glina.php"><img src="../grafiki/ikona_glina.png">120 gliny</a></li>
						</ul>
					</p>
					<p class="fs-5">
						Dodatkowo do budowy miejsca spotkań potrzeba 1 wolnego człowieka.<br>
						Miejsce spotkań nie podlega ulepszeniom.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>