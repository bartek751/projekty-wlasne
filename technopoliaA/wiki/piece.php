<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Piece</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
						<b>Piece</b> to jeden z kluczowych budynków przetwórczych. Do działania wymaga paliwa (<a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewno</a>, 
						<a href="deski.php"><img src="../grafiki/ikona_deski.png">deski</a>, <a href="wegiel.php"><img src="../grafiki/ikona_wegiel.png">węgiel</a> lub 
						<a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">węgiel drzewny</a>). Użycie węgli skraca czas wytwarzania, potrzeba 
						ich także znacznie mniej niż <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> czy 
						<a href="deski.php"><img src="../grafiki/ikona_deski.png">desek</a>. Dodatkowo niektóre surowce mogą wymagać użycia węgla do przetwarzania. 
						Każda technologia przetwarzania w piecu wymaga stosownych <a href="badania.php"><img src="../grafiki/przycisk_badania.png" class="ikonaOdnosnika">badań</a>.
					</p>
					<p>
						Można prowadzić tutaj także najliczniejszą ilość procesów wytwórczych. Są to odpowiednio:
						<ul>
							<li>Wypalanie <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> na 
								<a href="wegielDrzewny.php"><img src="../grafiki/ikona_wegielDrzewny.png">węgiel drzewny</a></li>
							<li>Wypalanie <a href="cegly.php"><img src="../grafiki/ikona_cegla.png">cegieł</a> z 
								<a href="glina.php"><img src="../grafiki/ikona_glina.png">gliny</a></li>
							<li>Palenie <a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapieni</a> na 
								<a href="wapno.php"><img src="../grafiki/ikona_wapno.png">wapno</a> (wymaga węgla)</li>
							<li>Wytop <a href="szklo.php"><img src="../grafiki/ikona_szklo.png">szkła</a></li>
							<li>Otrzymywanie <a href="cement.php"><img src="../grafiki/ikona_cement.png">cementu</a> (wymaga węgla)</li>
							<li>Wytop <a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">żelaza</a> z 
								<a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">rudy darniowej</a>, 
								<a href="piryt.php"><img src="../grafiki/ikona_piryt.png">pirytu</a> lub 
								<a href="hematyt.php"><img src="../grafiki/ikona_hematyt.png">hematytu</a> (wymaga węgla)</li>
							<li>Wytop <a href="miedz.php"><img src="../grafiki/ikona_miedz.png">miedzi</a> z 
								<a href="malachit.php"><img src="../grafiki/ikona_malachit.png">malachitu</a></li>
						</ul>
						Piece mogą pomieścić do 50 sztuk zasobów.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/animacje/piece-1.png">
					<div class="card-body">
						<p class="card-text text-center">Piece</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<div class="overflow-auto">
						<table class="table table-info table-bordered table-striped w-auto fs-6 text-center shadow-sm">
							<thead>
								<tr>
									<th class="align-middle">Zasób</th>
									<th>Poziom 1</th>
									<th>Poziom 2</th>
									<th>Poziom 3</th>
									<th>Poziom 4</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-start"><a href="zasoby.php"><img src="../grafiki/DDCoin.png">Monety</a></td>
									<td>200</td>
									<td>1000</td>
									<td>2000</td>
									<td>4200</td>
								</tr>
								<tr>
									<td class="text-start"><a href="kamien.php"><img src="../grafiki/ikona_kamien.png">Kamień</a></td>
									<td>320</td>
									<td>120</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="piasek.php"><img src="../grafiki/ikona_piasek.png">Piasek</a></td>
									<td>80</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="glina.php"><img src="../grafiki/ikona_glina.png">Glina</a></td>
									<td>600</td>
									<td>100</td>
									<td>-</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">Cegły</a></td>
									<td>-</td>
									<td>250</td>
									<td>400</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="cement.php"><img src="../grafiki/ikona_cement.png">Cemen</a>t</td>
									<td>-</td>
									<td>-</td>
									<td>200</td>
									<td>-</td>
								</tr>
								<tr>
									<td class="text-start"><a href="zelazo.php"><img src="../grafiki/ikona_zelazo.png">Żelazo</a></td>
									<td>-</td>
									<td>-</td>
									<td>20</td>
									<td>200</td>
								</tr>
								<tr>
									<td class="text-start"><a href="miedz.php"><img src="../grafiki/ikona_miedz.png">Miedź</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>120</td>
								</tr>
								<tr>
									<td class="text-start"><a href="szklo.php"><img src="../grafiki/ikona_szklo.png">Szkło</a></td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>80</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<p class="fs-5">
					Dodatkowo do budowy pieców potrzeba 2 wolnych ludzi.<br>
					Wraz ze wzrostem poziomu pieców zwiększa się ich ilość miejsc pracy.<br>
					Ilość zatrudnionych ludzi wpływa liniowo na wydajność wytwarzania.
				</p>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>