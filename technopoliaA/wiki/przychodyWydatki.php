<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Przychody i wydatki</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
						<b>Monety</b> w grze technopolis są jednym z podstawowych i jednocześnie istotnych zasobów w grze. Na ich bilans wpływają 2 źródła dochodów oraz 7 źródeł 
						rozchodów. Do źródeł przychodów zalicza się <a href="rynek.php" id="odnosnik"><img src="../grafiki/DDCoin.png" class="ikonaOdnosnika">rynek (giełda towarów)</a> 
						oraz sprzedaż żywności mieszkańcom. Cena żywności sprzedanej mieszkańcom może być zmieniona przez gracza gdy ten zbuduje 
						<a href="kamienica.php" id="odnosnik"><img src="../grafiki/malaKamienica.png" class="ikonaOdnosnika">kamienicę</a> i przekształci ją na pasaż handlowy.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid mt-1" src="../grafiki/DDCoin.png">
					<div class="card-body">
						<p class="card-text text-center">ikona monety</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Wydatki</b>
					</p>
					<p>
					Na wydatki w grze ma wpływ siedem elementów:
					<ul>
						<li>Kupno zasobów na <a href="rynek.php" id="odnosnik"><img src="../grafiki/DDCoin.png" class="ikonaOdnosnika">rynku (giełda towarów)</a></li>
						<li>Opłacanie wynagrodzeń dla pracujących mieszkańców</li>
						<li>Budowanie oraz ulepszanie budynków</li>
						<li>Przeznaczanie monet na prowadzenie <a href="badania.php" id="odnosnik"><img src="../grafiki/przycisk_badania.png" class="ikonaOdnosnika">badań</a></li>
						<li>Organizacja festiwali w <a href="miejsceSpotkan.php" id="odnosnik"><img src="../grafiki/animacje/miejsceSpotkan-1.png" class="ikonaOdnosnika">miejscu spotkań</a></li>
						<li>Niektóre programy socjalne polepszające <a href="zadowolenie.php" id="odnosnik"><img src="../grafiki/zadowolenie-1.png">zadowolenie</a></li>
						<li>Oczyszczanie <a href="rolnictwo.php"><img src="../grafiki/poleUprawne-chwasty.png" class="ikonaOdnosnika">pola uprawnego</a> z chwastów</li>
					</ul>
					</p>
					<p style="text-indent: 3%;">
						Jeśli ilość posiadanych przez gracza monet spadnie poniżej 0, wówczas gracz ma naliczany debet. Jego wielkość nie jest jednak nigdzie wyświetlana jednak 
						jest on spłacany na bierząco gdy tylko gracz pozyska monety.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>