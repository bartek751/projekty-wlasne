<html>
<body>
	<nav class="navbar navbar-expand-md nav-justified navbar-dark bg-success">
		<div class="container-fluid d-flex justify-content-around align-items-center navbar-nav p-0">
			<div class="nav-item">
				<button class="btn btn-info" type="submit" onclick="btnGlownaWiki()">Strona główna wiki</button>
			</div>
			<div class="nav-item">
				<a class="navbar-brand text-black"><h2>Technopolis Wiki</h2></a>
			</div>
			<div class="nav-item">
				<form class="d-flex m-0 justify-content-center" action="wyszukaj.php" method="POST">
				<input class="form-control me-2 w-auto" name="szukaj" type="search" placeholder="Wyszukaj..." aria-label="Search">
				<button class="btn btn-info" type="submit">Szukaj</button>
				</form>
			</div>
		</div>
    </nav>
</body>
<script>
	function btnGlownaWiki()
	{
		location.href="index.php";
	}
</script>
</html>