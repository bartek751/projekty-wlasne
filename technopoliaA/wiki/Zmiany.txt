============
   V 1.1
============
Dodano:
- znacznik <header> w nagłówku
- znacznik <meta> do skalowania strony
- skalowanie dla różnych urządzeń
- brakujące opisy dla kategorii badań
- skrypt w artykule o badaniach zmieniający tekst przycisków rozwijających i zwijających kategorie

Zmieniono:
- Nagłówki głównych kategorii na stronie głównej
- Zgrupowano nagłówek, przycisk i pierwsze kroki w jedną grupę
- Wiekość tekstu ustalana jest teraz poprez css
- wielkość ikon w grupie mechanik w indexie
- wygląd ramek grup w indexie
- kolor belki oddzielającej segmenty strony
- przeniesiono styl css do osobnego pliku "styl.css"
- kolumny boczne (te z grafiką kamieni) nie są widoczne na małych urządzeniach

Naprawiono:
- przy pustym polu wyszukiwania pojawiał się komunikat błędu
- poprawiono pozycjonowanie pola i przycisku szukania w nagłówku

Poprawiono:
- Odniesienie do formatowania tekstu w css dla odnośników
- Literówka w artykule o kamieniach
- kilka błędów ortograficznych
- centrowanie od teraz odbywa się za pomocą css
- dodano braukjące ikony przy odnośnikach (głównie budynków i badań)
- artykuły o wszystkich budynkach
- artykuły o wszystkich mechanikach
- artykuły o wszystkich zasobach
- stronę z wynikami wyszukiwania
- artykuł o pierwszych krokach
- stronę główną gry

Usunięto:
- kropki w grupach na stronie głównej
- informację o obecnej wersji gry na stronie głównej wiki