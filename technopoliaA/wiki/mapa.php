<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Mapa gry</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p>
						<b>Opis ogólny mapy</b>
					</p>
					<p style="text-indent: 3%;">
						<b>Mapa</b> w grze Technopolis ma wymiary 30 na 20 kratek, oraz pięć poziomów głębi. Każda warstwa zawiera charakterystyczne dla siebie 
						<a href="zasoby.php">zasoby</a>, 
						które można pozyskać dzięki konkretnym budynkom. Niektóre z zasobów podlegają regeneracji (np. wody gruntowe, 
						<a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewno</a>), inne są nieodnawialne 
						(np. <a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">ruda darniowa</a>, 
						<a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapienie</a>). 
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid mt-1" src="grafiki/fragmentMapy.png">
					<div class="card-body">
						<p class="card-text text-center">Fragment mapy gry</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Opis zewnętrznej warstwy mapy</b>
					</p>
					<p style="text-indent: 3%;">
						Zewnętrzna warstwa ukazuje się nam zaraz po wczytaniu gry. Jej powierzchnia składa się z jednego z czterech rodzajów podłoża: piasku, gołoborza, łąki oraz 
						lasu. Ich ułożenie jest losowe, nie mniej charakteryzuje się pewnymi zasadami. Mapa generuje po jednym źródle kamieni i piasku, przy czym piasek zajmuje od 
						5 do 18 kratek, z kolei gołoborze z reguły ma między 4 a 15 kratek wielkości. Dodatkowo te dwie formacje charakteryzują się znacznym rozciągnięciem. Z kolei 
						las generowany jest w dwóch, trzech lub czterech skupiskach po 12-60 pól. Może on zajmować znaczną część mapy. Wszystkie wymienione typy podłoża mogą ulec 
						wyczerpaniu, co sprawi, że zamienią się one w pozbawioną zasobów łąkę. 
					</p>
					<p style="text-indent: 3%;">
						Na łące można prowadzić wydobycie <a href="piasek.php"><img src="../grafiki/ikona_piasek.png">piasku</a>, 
						<a href="glina.php"><img src="../grafiki/ikona_glina.png">gliny</a>, 
						<a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamieni</a> oraz 
						<a href="zwir.php"><img src="../grafiki/ikona_zwir.png">żwiru</a> dzięki 
						<a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnsnika">żwirowni</a>, odznacza się to jednak bardzo małą wydajnością. 
						Dużo większe ilości <a href="zwir.php"><img src="../grafiki/ikona_zwir.png">żwiru</a> i <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamieni</a> 
						można uzyskać, stawiając <a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnsnika">żwirownię</a> na gołoborzu, z kolei by uzyskać 
						<a href="glina.php"><img src="../grafiki/ikona_glina.png">glinę</a> i <a href="piasek.php"><img src="../grafiki/ikona_piasek.png">piasek</a> 
						potrzebne do budowy i przetwórstwa, należy postawić <a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnsnika">żwirownię</a> na terenie 
						piaszczystym. <a href="lasy.php"><img src="../grafiki/drzewo2.png" class="ikonaListy">Las</a> może z kolei być źródłem 
						<a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewna</a> (wyczerpuje zasoby) oraz 
						<a href="zywica.php"><img src="../grafiki/ikona_zywica.png">żywicy</a> (nie wyczerpuje zasobów), jeśli w bezpośrednim sąsiedztwie znajduje się 
						<a href="tartak.php"><img src="../grafiki/tartak.png" class="ikonaOdnsnika">tartak</a>. 
						<a href="lasy.php"><img src="../grafiki/drzewo2.png" class="ikonaListy">Las</a> jako jedyny może zostać odtworzony poprzez zasadzenie drzew na łące. 
						Także, jeśli przez dłuższy czas nie będzie on eksploatowany, odnowi swoje zasoby (odrośnie). Z tego właśnie powodu intensywna gospodarka leśna
						w późniejszym etapie gry nie jest zalecana.
					</p>
					<p class="text-center">
						<img class="mx-auto mw-100 mh-100" src="grafiki/mapaGry.png"><br>
						Cztery rodzaje podłoża występujące w grze
					</p>
					<p style="text-indent: 3%;">
						Warstwa zewnętrzna jako jedyna pozwala na ustawianie na niej budynków. Wybierając budowanie z menu budowy, zaznacza się nam obszar, na którym możliwe 
						jest budowanie. Każdy budynek ma własne wymagania co do miejsca umieszczenia i rodzaju podłoża.
					</p>
					<p class="text-center">
						<img class="mx-auto mw-100 mh-100" src="grafiki/mapaBudowaTartak.png"><br>
						Budowanie tartaku na mapie
					</p>
					<p style="text-indent: 3%;">
						Budynek jest kolejnym typem podłoża, zabronionym do wykorzystania przez jakiekolwiek inne twory czy operacje. Większość budynków zajmuje cztery kratki 
						wielkości. Dodatkowym, niewidocznym zasobem mapy jest jej żyzność, co ma znaczenie w rolnictwie. Jeśli 
						pole używane jest co roku, to zbiory z niego słabną, aż do wykonania rocznej przerwy.
					</p>
					<p class="text-center">
						<img class="mx-auto mw-100 mh-100" src="grafiki/mapaWarstwaZewnetrzna.png" class="w-75"><br>
						Wygląd mapy zewnętrznej w czasie gry
					</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Warstwa gruntowa</b>
					</p>
					<p style="text-indent: 3%;">
						Warstwa gruntowa mapy jako jedyna nie ma swojej reprezentacji graficznej. Jest ona jedynie szeregiem płytko występujących <a href="zasoby.php">zasobów</a>. 
						By poznać jej zawartość, można wykonać badanie gruntu lub poszukiwanie wód podziemnych. Typowymi <a href="zasoby.php">zasobami</a>, które można stąd wydobyć 
						to <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamień</a>, 
						<a href="rudaDarn.php"><img src="../grafiki/ikona_rudaDarn.png">ruda darniowa</a>, 
						<a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapień</a> i niewielkie ilości innych minerałów (
						<a href="hematyt.php"><img src="../grafiki/ikona_hematyt.png">hematyt</a>, 
						<a href="kwarc.php"><img src="../grafiki/ikona_kwarc.png">kwarc</a>, 
						<a href="wegiel.php"><img src="../grafiki/ikona_wegiel.png">węgiel</a>).
						Ich wydobycie prowadzi się, budując w odpowiednim miejscu <a href="zwirownia.php"><img src="../grafiki/zwirownia.png" class="ikonaOdnsnika">żwirownię</a>. 
						Zasoby te nie odnawiają się. Warstwa ta zawiera także zasoby wody, do których dostęp można uzyskać dzięki 
						<a href="studnia.php"><img src="../grafiki/studnia.png" class="ikonaOdnsnika">studniom</a>. Te z kolei odnawiają się cyklicznie, choć ich granice występowania 
						mogą się przesunąć.
					</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Warstwy podziemne, głębiny</b>
					</p>
					<p style="text-indent: 3%;">
						Dostęp do pozostałych trzech warstw umożliwiają 
						<a href="kopalniaGlebinowa.php"><img src="../grafiki/kopalniaGlebinowa.png" class="ikonaOdnsnika">kopalnie głębinowe</a>. W ich interfejsie możemy wybrać 
						opcję zejścia pod ziemię. Wybierając ją, zyskujemy dostęp do podziemnej części mapy. Nie jest ona jednak wyświetlana w sposób kompletny, widoczne są dla nas 
						jedynie wykopane przez nas korytarze oraz miejsca, gdzie aktualnie prowadzi się wydobycie. Podstawową akcją jest tutaj ustawienie miejsc, gdzie prowadzone 
						ma być wydobycie, poprzez przydzielenie górników do pola klikiem LPM lub ich zwolnienie z pozycji dzięki kliknięciu PPM. 
					</p>
					<p style="text-indent: 3%;">
						Warstwy te mogą zawierać takie <a href="zasoby.php">zasoby</a> jak pokłady 
						<a href="wapien.php"><img src="../grafiki/ikona_wapien.png">wapieni</a>, 
						<a href="piryt.php"><img src="../grafiki/ikona_piryt.png">pirytu</a>, 
						<a href="hematyt.php"><img src="../grafiki/ikona_hematyt.png">hematytu</a>, 
						<a href="wegiel.php"><img src="../grafiki/ikona_wegiel.png">węgla</a>, 
						<a href="halit.php"><img src="../grafiki/ikona_halit.png">halitu</a> czy 
						<a href="malachit.php"><img src="../grafiki/ikona_malachit.png">malachitu</a>. Jednak głównym efektem wydobycia będzie skała płonna, czyli w tym 
						wypadku zwykłe <a href="kamien.php"><img src="../grafiki/ikona_kamien.png">kamienie</a>. Jeśli nad pierwszą z warstw głębinowych znajdują się wody podziemne, 
						może prowadzić to do zalewania tuneli. Analogiczna sytuacja na niższych poziomach może wydarzyć się niezależnie od uwarunkowań warstwy gruntowej. 
						(Będzie można temu przeciwdziałać wyposażając kopalnię w stację pomp).
					</p>
					<p class="text-center">
					<img class="mx-auto mw-100 mh-100" src="grafiki/mapaGlebinyA.png"><br>
					Wycinek mapy z pierwszej warstwy głębinowej, wraz z pozycjami górników
					</p>
					By dostać się na niższe piętra, konieczne jest podnoszenie poziomu kopalń.
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>