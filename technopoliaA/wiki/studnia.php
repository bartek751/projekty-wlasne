<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Studnia</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p style="text-indent: 3%;">
					<b>Studnia</b> jest budynkiem pozwalającym na pozyskiwanie wody. Zasadniczo, nie można zrobić zapasów tego surowca, więc ważne jest, aby jej napływ uzupełniał bieżące zapotrzebowanie.
					Ilość czerpanej wody zależy od miejsca, na którym została zbudowana, dlatego najlepiej przed jej postawieniem wykonać dowolne badanie terenu. Woda w studniach odnawia się w sposób ciągły. 
					Zużycie wody bliskie 100% wydajności może powodować szybsze osuszanie się terenów, gdzie studnie się znajdują. W menu tego budynku możliwe jest wyłączenie pojedynczej studni z użycia.  
					</p>
				</div>
				<div class="col-xs-12 col-md kartaMalegoObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid" src="../grafiki/studnia.png">
					<div class="card-body">
						<p class="card-text text-center">Studnia</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1">
					<p class="fs-5">
						<b>Budowa i ulepszenia:</b>
					</p>
					<p class="fs-5">
						Koszt budowy to:
						<ul>
							<li><a href="zasoby.php"><img src="../grafiki/DDCoin.png">200 monet</a></li>
							<li><a href="drewno.php"><img src="../grafiki/ikona_drewno.png">10 drewno</a></li>
							<li><a href="cegly.php"><img src="../grafiki/ikona_cegla.png">10 cegły</a></li>
						</ul>
					</p>
					<p class="fs-5">
						Dodatkowo do budowy studni potrzeba 1 wolnego człowieka.<br>
						Studnia nie podlega ulepszeniom.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>