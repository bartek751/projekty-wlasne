<html>
<head>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8952847261671941" crossorigin="anonymous"></script>
	<title>Technopolis - wiki</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="styl.css"/>
</head>
<body>
<div class="container-fluid">
<div class="row" id="pasekGorny">
	<?php include "naglowek.php"; ?>
</div>
<div class="row" id="srodekStrony">
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
	
	<div class="col-lg-8" id="kolCentrum">
		<div class="row tytulArtykulu" id="centrumRowA">
			<p class="text-center">Rynek</p>
			<span class="w-75 mx-auto rounded-pill" id="belka"></span>
		</div>
		<div class="row" id="centrumRowB">
			<div class="row column-gap-2 mt-1 ps-3 g-2">
				<div class="col-xs-12 col-md-10 fs-5">
					<p>
						<b>Opis ogólny rynku</b>
					</p>
					<p style="text-indent: 3%;">
						<b>Rynek</b> w grze technopolis umożliwia kupno oraz sprzedaż zasobów takich jak materiały lub żywność. W grze widnieje jako "Giełda towarów" i można ją 
						otworzyć klikając przycisk wózka sklepowego.
					</p>
					<p>
						Rynek podlega zmianom zarówno wynikających z działań gracza (kupno i sprzedaż) jak i ze względu na zmiany cykliczne gry. Ceny towarów mogą się różnić 
						w każdej z rozpoczętych rozgrywek i będą ulegać zmianom wraz z jej trwaniem.
					</p>
				</div>
				<div class="col-xs-12 col-md kartaObrazku align-self-start shadow-sm" id="centrumGrupa">
					<img class="mx-auto d-block img-fluid mt-1" src="../grafiki/przycisk_handel.png">
					<div class="card-body">
						<p class="card-text text-center">Przycisk okna handlu</p>
					</div>
				</div>
			</div>
			<div class="row mt-1 ps-3 g-2">
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Ceny i ich zmiany</b>
					</p>
					<p style="text-indent: 3%;">
						Każdego miesiąca następuje losowa rewaloryzacja cen towarów na rynku. Zależy ona jednak częściowo od nabywanych i sprzedawanych przez nas towarów. 
						Wysoka liczba zakupionego surowca sprzyja wzrostowi cen, z kolei jego wyprzedawanie wiąże się ze wzrostem prawdopodobieństwa na to, że jego ceny będą maleć.
					</p>
					<p style="text-indent: 3%;">
						Zależności są oddzielne dla każdego towaru i są utajnione dla gracza. Różnią się także w zależności od rozgrywki. Zmiany następują wraz ze zmianą miesiąca. 
						Wtedy także na rynku mogą pojawić się nowe surowce. Po upłynięciu określonej ilości miesięcy, na giełdzie pojawiają się nowe towary, takie jak 
						<a href="wapien.php"><img src="../grafiki/ikona_wapien.png">Wapień</a>, <a href="malachit.php"><img src="../grafiki/ikona_malachit.png">Malachit</a> czy 
						<a href="halit.php"><img src="../grafiki/ikona_halit.png">sól</a>. 
					</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>System płacowy</b>
					</p>
					<p style="text-indent: 3%;">
						Na początku każdego miesiąca zostają wypłacone pensje, pomniejszając ilość <a href="zasoby.php"><img src="../grafiki/DDCoin.png">monet</a>. Jeśli nie 
						posiadamy wystarczającej jej ilości, braki przypisane są jako bankructwo. To z kolei jest natychmiast spłacane, gdy tylko sprzedamy dowolny towar. 
						Na początku miesiąca następują też samożutne zwolnienia z powodu zbyt niskich pensji oraz zatrudnienia w zakładzie, jeśli proponowana pensja jest 
						odpowiednio wysoka. 
					</p>
				</div>
				<span class="w-75 mx-auto rounded-pill" id="belka"></span>
				<div class="mt-1 fs-5">
					<p>
						<b>Pobieranie surowców przez społeczeństwo</b>
					</p>
					<p style="text-indent: 3%;">
						Społeczeństwo co jakiś losowy czas (w obrębie interwału właściwego) pobiera dane surowce, zostawiając w budżecie gracza cenę rynkową surowca powiększoną 
						o marżę (z regółu 10-50% ceny). Przez cały rok pobierana jest żywność, w okresie zimowym od listopada do marca dodatkowo pobierany jest 
						<a href="wegiel.php"><img src="../grafiki/ikona_wegiel.png">węgiel</a> i <a href="drewno.php"><img src="../grafiki/ikona_drewno.png">drewno</a>. 
						Ilość pobieranych surowców jest wprost proporcjonalna do liczności społeczeństwa.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="kolumnaBoczna" class="d-none d-lg-block col-lg-2">
		
	</div>
</div>
</div>
</body>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
	function btnDoIndexu(){
		location.href="../index.html";
	}
	
	function spasowanieWysokosciStrony()
	{
		var a=document.getElementById("pasekGorny").clientHeight;
		var b=document.getElementById("centrumRowA").clientHeight;
		var c=document.getElementById("centrumRowB").clientHeight;
		if(a+b+c<window.innerHeight)
		{
			document.getElementById("srodekStrony").style.height=window.innerHeight-a;
		}
	}
	spasowanieWysokosciStrony();
</script>
</html>